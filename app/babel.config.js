module.exports = {
  presets: ['module:@react-native/babel-preset'],
  plugins: [
    'react-native-reanimated/plugin',
    [
      'module-resolver',
      {
        root: ['.'],
        extensions: ['.ts', '.tsx', '.json'],
        alias: {
          'src/': './src',
        },
      },
    ],
  ],
  env: {
    // development: {
    //   plugins: [['@babel/plugin-transform-react-jsx', {runtime: 'classic'}]],
    // },
    production: {
      plugins: ['transform-remove-console'],
    },
  },
};
