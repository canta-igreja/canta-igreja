# Organização

- `src/view/base`: Componentes base (Botão, Texto, View)
- `src/view/components`: Componentes mais elaborados, que geralmente utilizam componentes base (Modal)
- `src/view/fragment`: Fragmentos de tela
- `src/view/Screen`: Telas que são exibidas em um router

## Gerar bundle

```bash
# Configure asset (including database)
npx @callstack/react-native-asset@latest

# Generate .apk for flashlight tests
npm run android -- --mode="release"
npx react-native run-ios --mode=Release

# Generate .adb for release
npx react-native build-android --mode=release
```

## Gerar ícone

```
# Cuidado, isso vai sobrescrever os ícones do android
# Para Android, o ideal é utilizar o Android Studio para substituir a imagem
#npx icon-set-creator create --adaptive-icon-background "#6B8BC8" ../media/icone-square.png
```
