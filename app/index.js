// import './src/arch/util/wdyr';

import 'react-native-gesture-handler';

import {AppRegistry} from 'react-native';
import {App} from './src/App';
import {name as appName} from './app.json';

// https://github.com/software-mansion/react-native-screens?tab=readme-ov-file#experimental-support-for-react-freeze
import {enableFreeze} from 'react-native-screens';
enableFreeze(true);

AppRegistry.registerComponent(appName, () => App);
