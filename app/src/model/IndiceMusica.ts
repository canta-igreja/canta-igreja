export interface IndiceMusica {
  id_musica: number;
  id_edicao_livro: number;

  indice: string;
  titulo_livro: string;
  edicao: string;
  editora: string;
}
