
export interface Pagina<T> {
  /**
   * Página corrente, onde 0 corresponde a primeira página
   */
  current: number;
  /**
   * É a última página?
   */
  hasNextPage: boolean;
  /**
   * Elementos presente nessa página
   */
  elements: T[];
  /**
   * Tamanho da página: Número de elementos por página
   */
  size: number;
}