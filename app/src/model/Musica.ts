import {RotuloMusica} from './RotuloMusica';
import {IndiceMusica} from './IndiceMusica';
import {MusicaItemListData} from 'src/view/component/list/data/MusicaItemListData';

export interface ReferenciaMusica {
  id_musica: number;
  titulo: string;
}

export interface InformacoesBasicasMusica extends ReferenciaMusica {
  letra: string;

  contribuidores: Contribuidores;
  midias: Midias;
}

export interface Contribuidores {
  compositores?: string[];
  letristas?: string[];
  musicistas?: string[];
}
export interface Midias {
  YouTube?: string;
  CifraClub?: string;
  Partitura?: string;
}

export interface InformacoesDetalhadasMusica extends InformacoesBasicasMusica {
  rotulos: RotuloMusica[];
  indices: IndiceMusica[];
}

export function musicaAsItemListData(musica: InformacoesBasicasMusica): MusicaItemListData {
  return {
    id_musica: musica.id_musica,
    titulo: musica.titulo,
    trecho_letra: musica.letra.slice(0, 70),
  };
}
