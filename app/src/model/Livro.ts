export interface EdicaoLivro {
  id_edicao_livro: number;
  id_livro: number;
  titulo: string;
  edicao: string;
  editora: string;

  ano: number;
  site: string | undefined;
  descricao: string;
}
