export interface RotuloMusica {
  id_rotulo_musica: number;
  id_rotulo: number;
  id_categoria_rotulo: number;
  nome: string;
}