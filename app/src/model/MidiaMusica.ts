import {TipoMidia} from './TipoMidia';

export interface MidiaMusica {
  id_midia_musica: number;
  id_musica: number;

  id_tipo_midia: TipoMidia;
  url: string;
}
