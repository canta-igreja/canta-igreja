import {MusicaItemListData} from 'src/view/component/list/data/MusicaItemListData';

export interface ReferenciaRepertorioSlug {
  conta: string;
  repertorio: string;
}

export interface RepertorioSlug {
  /**
   * Slug de quem originou essa peça
   *
   * Quando um repertório é compartilhado a partir do aplicativo, a referência é o do próprio repertório
   * Quando um repertório é importado para o aplicativo, a referência é o do repertório que foi importado
   * (a conta de quem o criou e o id do repertório que foi importado)
   */
  referencia: ReferenciaRepertorioSlug;
  /**
   * Slug da peça
   *
   * Quando um repertório é compartilhado no aplicativo, o corrente é o do próprio repertório.
   *
   * Caso o repertório seja importado de um terceiro e seja compartilhado, então o corrente
   * se referenciará a conta da pessoa logada no aplicativo e um novo slug será gerado, de modo que
   * corrente !== referencia, exceto no caso em que a pessoa está compartilhando novamente um repertório
   * que ela mesmo havia compartilhado anteriormente: nesse caso, preserva-se a igualdade referencia === corrente.
   *
   * Observação: Caso um repertório seja criado a partir de outro (duplicação), slug todo deve ser undefined
   * enquanto a cópia não for duplicada.
   */
  corrente: ReferenciaRepertorioSlug;
}

export interface Repertorio {
  id_repertorio: number;
  slug?: RepertorioSlug;

  titulo: string;
  resumo: string;

  ordem_musicas: number[];

  data: {
    edicao: number;
  };
}

export interface ItemRepertorio {
  id_item_repertorio: number;
  id_repertorio: number;

  musica?: MusicaItemListData; // FIXME Revisar se armazena aqui ou se usa da forma normalizada
  termo?: string;

  momento: {
    valor?: string;
    sugestoes: string[];
  };
  tonalidade: {
    valor?: string;
    sugestoes: string[];
  };
}
