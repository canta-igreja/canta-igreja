import {Contribuidores, Midias, ReferenciaMusica} from '../Musica';

export interface MusicaResponse extends ReferenciaMusica {
  id_musica: number;
  titulo: string;

  tonalidade_sugerida?: string;

  letra: string;

  colaboradores: Contribuidores;
  midias: Midias;

  crc32: number;
}
