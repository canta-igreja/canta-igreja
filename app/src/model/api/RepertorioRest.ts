import {MusicaResponse} from './MusicaResponse';

export enum SimilaridadeRepertorios {
  NAO_EXISTE_CORRESPONDENTE,
  CORRESPONDENTE_IGUAL,
  CORRESPONDENTE_COM_VERSAO_DISTINTA,
}

type ItemRepertorioRest = {
  id_musica?: number;
  termo?: string;
  momento?: string;
  tonalidade?: string;
};

interface RepertorioBase {
  titulo: string;
  itens: ItemRepertorioRest[];
}

export interface PersistirRepertorioRequest extends RepertorioBase {
  titulo: string;
  itens: ItemRepertorioRest[];
}

export interface RepertorioPersistidoResponse extends RepertorioBase {
  slug: string;
  conta: {
    nome: string;
    slug: string;
  };

  criado_em: string;
  atualizado_em?: string;
}

export interface RepertorioQuery {
  titulo: string;

  itens: {
    /**
     * Id da música
     */
    id?: number;
    termo?: string;

    momento?: string;
    /**
     * Tonalidade
     */
    tom?: string;
  }[];
}

export interface RepertorioImportavelDTO {
  repertorio: RepertorioPersistidoResponse;
  musicas: MusicaResponse[];
}
