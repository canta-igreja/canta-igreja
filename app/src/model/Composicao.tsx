export enum TipoComposicao {
  Composicao = 1,
  Letra = 2,
  Musica = 3
}

export interface InformacoesComposicaoMusica {
  id_composicao_musica: number;
  id_tipo_composicao: TipoComposicao;
  id_artista?: number;
  nome_artistico?: string;
  adaptacao?: string;
}