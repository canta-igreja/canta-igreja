export interface Obra {
  id_obra: number;
  titulo: string;
  descricao: string;
  crc32?: number;
  disponivel: boolean;
}

export interface ExemplarBiblioteca {
  id_exemplar: number;
  id_obra: number;

  titulo: string;
  descricao: string;

  crc32: number;
  ativo: boolean;
}
