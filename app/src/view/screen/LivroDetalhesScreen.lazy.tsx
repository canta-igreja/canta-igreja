import React, {FC, Suspense, lazy} from 'react';
import {ScreenFallback} from 'src/view/component/screen/ScreenFallback';

const LivroDetalhesScreen = lazy(() => import('./LivroDetalhesScreen'));

export const LivroDetalhesScreenLazy: FC = (props: any) => (
  <Suspense fallback={<ScreenFallback />}>
    <LivroDetalhesScreen {...props} />
  </Suspense>
);
