import React, {FC, useCallback} from 'react';
import {View} from 'react-native';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {useSelector} from 'react-redux';
import {UtilStyles} from 'src/arch/style/util.styles';
import {ItemRepertorio} from 'src/model/repertorio/Repertorio';
import {AppStack} from 'src/router/AppStackNavigator.types';
import {AppStackNavigationProps} from 'src/router/navigation/NavigationTypes';
import {ItensRepertorioSelect} from 'src/store/ItensRepertorio/ItensRepertorio.selector';
import {ItensRepertorioAction} from 'src/store/ItensRepertorio/ItensRepertorio.action';
import {useAppDispatch} from 'src/store/store.hooks';
import {DetalhesMusicaUtil} from 'src/util/DetalhesMusicaUtil';
import {AutoCompleteFragment} from 'src/view/fragment/AutoCompleteFragment/AutoCompleteFragment';

const MomentoModalScreen: FC<AppStackNavigationProps<AppStack.MomentoModal>> = ({route, navigation}) => {
  const {styles} = useStyles(stylesheet);

  const dispatch = useAppDispatch();

  const item = useSelector(ItensRepertorioSelect.byId(route.params.idItemRepertorio));

  const fecharModal = useCallback(
    async (valor: string) => {
      if (item) {
        const itemAtualizado: ItemRepertorio = {...item, momento: {...item.momento, valor}};
        await dispatch(ItensRepertorioAction.atualizarItem(itemAtualizado));
      }

      navigation.goBack();
    },
    [item, dispatch, navigation],
  );

  if (item === undefined) {
    return <></>;
  }

  const valorInicial = item.momento.valor;
  const sugestoes = item.momento.sugestoes;

  return (
    <View style={styles.safeAreaView}>
      <AutoCompleteFragment
        titulo={item.musica?.titulo ?? item.termo}
        label="Momento"
        placeholder="Exemplo: Glória"
        valorInicial={valorInicial}
        opcoes={DetalhesMusicaUtil.momentos}
        sugestoes={sugestoes}
        onOk={fecharModal}
      />
    </View>
  );
};
export default MomentoModalScreen;

const stylesheet = createStyleSheet((theme, runtime) => ({
  safeAreaView: {
    flex: 1,
    backgroundColor: theme.color.background.level_1,
    ...UtilStyles.safeAreaViewInnerPadding(runtime, {top: false}),
  },
}));
