import React, {FC, Suspense, lazy} from 'react';
import {ScreenFallback} from 'src/view/component/screen/ScreenFallback';

const PreviaMusicaModalScreen = lazy(() => import('./PreviaMusicaModalScreen'));

export const PreviaMusicaModalScreenLazy: FC = (props: any) => (
  <Suspense fallback={<ScreenFallback />}>
    <PreviaMusicaModalScreen {...props} />
  </Suspense>
);
