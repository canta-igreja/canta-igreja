import {useEffect, useState} from 'react';
import {Toast} from 'src/arch/util/Toast';
import {SimilaridadeRepertorios} from 'src/model/api/RepertorioRest';
import {ReferenciaRepertorioSlug} from 'src/model/repertorio/Repertorio';
import {useNavStack} from 'src/router/navigation/useAppNavigation';
import {useAppDispatch} from 'src/store/store.hooks';
import {EstadoRequisicao} from 'src/store/types';
import {ModalImportarRepertorioAction} from 'src/store/ui/ModalImportarRepertorio/ModalImportarRepertorio.action';

export const useImportarRepertorio = (conta?: string, repertorio?: string) => {
  const [estado, setEstado] = useState<EstadoRequisicao>(EstadoRequisicao.OCIOSO);
  const dispatch = useAppDispatch();
  const navStack = useNavStack();

  useEffect(() => {
    async function importar(referenciaRepertorio: ReferenciaRepertorioSlug) {
      try {
        const {repertorioImportavel, repertorioSimilar} = await dispatch(
          ModalImportarRepertorioAction.iniciar(referenciaRepertorio),
        ).unwrap();

        if (
          repertorioSimilar.similaridade === SimilaridadeRepertorios.NAO_EXISTE_CORRESPONDENTE &&
          repertorioImportavel
        ) {
          navStack.navigateToImportarRepertorioModal(repertorioImportavel);
        } else if (repertorioSimilar.repertorio) {
          if (repertorioSimilar.similaridade === SimilaridadeRepertorios.CORRESPONDENTE_IGUAL) {
            // Nada de especial a fazer para esse caso
          } else if (repertorioSimilar.similaridade === SimilaridadeRepertorios.CORRESPONDENTE_COM_VERSAO_DISTINTA) {
            Toast.show('Abrindo repertório salvo anteriormente');
          }

          navStack.navigateToRepertorio(repertorioSimilar.repertorio);
        }
      } catch (e) {
        // Link com problema (geralmente incompleto)
        Toast.show('Não foi possível importar repertório. Você está com internet?');
        console.error(e);
      }
    }

    if (conta && repertorio) {
      setEstado(EstadoRequisicao.CARREGANDO);
      importar({conta, repertorio})
        .then(() => setEstado(EstadoRequisicao.BEM_SUCEDIDO))
        .catch(e => {
          console.error(e);
          setEstado(EstadoRequisicao.FALHOU);
        });
    } else {
      console.log('Tela acessada pelo usuário (ou seja, sem deep link)');
    }
  }, [dispatch, navStack, conta, repertorio, setEstado]);

  return estado;
};
