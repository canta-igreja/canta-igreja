import React, {FC, Suspense, lazy} from 'react';
import {ScreenFallback} from 'src/view/component/screen/ScreenFallback';

const ImportarRepertorioModalScreen = lazy(() => import('./ImportarRepertorioModalScreen'));

export const ImportarRepertorioModalScreenLazy: FC = (props: any) => (
  <Suspense fallback={<ScreenFallback />}>
    <ImportarRepertorioModalScreen {...props} />
  </Suspense>
);
