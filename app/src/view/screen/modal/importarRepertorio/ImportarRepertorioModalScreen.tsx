import {Button, Text} from '@ui-kitten/components';
import React, {FC, useCallback, useState} from 'react';
import {View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {UtilStyles} from 'src/arch/style/util.styles';
import {RepertorioImportavelDTO} from 'src/model/api/RepertorioRest';
import {AppStack} from 'src/router/AppStackNavigator.types';
import {AppStackNavigationProps} from 'src/router/navigation/NavigationTypes';
import {useNavStack} from 'src/router/navigation/useAppNavigation';
import {RepertoriosAction} from 'src/store/Repertorios/Repertorios.action';
import {useAppDispatch} from 'src/store/store.hooks';
import {LetraUtil} from 'src/util/LetraUtil';
import {ListUtil} from 'src/util/ListUtil';
import {CarregandoIcone} from 'src/view/base/icones/CarregandoIcone';
import {SubtituloPagina, TituloPagina} from 'src/view/base/Texto';
import {ThreeLinesListItem} from 'src/view/component/list/item/ThreeLinesItemList';

const ImportarRepertorioModalScreen: FC<AppStackNavigationProps<AppStack.ImportarRepertorioModal>> = ({route}) => {
  const {styles} = useStyles(stylesheet);
  const [importando, setImportando] = useState<boolean>(false);

  const dispatch = useAppDispatch();
  const repertorioImportavel = route.params.repertorio;

  const navStack = useNavStack();
  const importarRepertorio = useCallback(async () => {
    setImportando(true);
    const repertorio = await dispatch(RepertoriosAction.importar(repertorioImportavel)).unwrap();
    navStack.goBack();
    navStack.navigateToRepertorio(repertorio);
  }, [dispatch, setImportando, navStack, repertorioImportavel]);

  return (
    <View style={styles.safeAreaView}>
      <View style={styles.header}>
        <TituloPagina>Importar repertório</TituloPagina>
        <Text appearance="hint">Importe o repertório caso você queira salvar o repertório.</Text>
        <Text appearance="hint">Você poderá acessar as músicas após importar o repertório.</Text>

        <Button style={styles.button} onPress={importarRepertorio} disabled={importando}>
          {importando ? (
            <>
              <Text>
                Importando <CarregandoIcone />
              </Text>
            </>
          ) : (
            <Text>Importar repertório</Text>
          )}
        </Button>
      </View>

      <SubtituloPagina style={styles.subtitulo}>{repertorioImportavel.repertorio.titulo}</SubtituloPagina>

      <ItensRepertorioImportavel repertorio={repertorioImportavel} />
    </View>
  );
};

export default ImportarRepertorioModalScreen;

const ItensRepertorioImportavel: FC<{repertorio: RepertorioImportavelDTO}> = ({repertorio}) => {
  const musicas = ListUtil.toMap(repertorio.musicas, it => it.id_musica);

  return (
    <ScrollView>
      {repertorio.repertorio.itens.map(it => {
        const musica = musicas[it.id_musica ?? -1];
        const tonalidade = it.tonalidade ? `${it.tonalidade} - ` : '';

        return (
          <ThreeLinesListItem
            disabled={true}
            header={it.momento}
            title={tonalidade + (musica?.titulo ?? it.termo)}
            description={
              (musica?.letra && LetraUtil.limparLetra(musica?.letra)) ?? '(música incluída manualmente pelo usuário)'
            }
          />
        );
      })}
    </ScrollView>
  );
};

const stylesheet = createStyleSheet((theme, runtime) => ({
  safeAreaView: {
    flex: 1,
    backgroundColor: theme.color.background.level_1,
    ...UtilStyles.safeAreaViewInnerPadding(runtime, {top: false}),
  },
  header: {
    marginHorizontal: theme.spacing.s16,
  },
  button: {
    marginTop: theme.spacing.s8,
    marginBottom: theme.spacing.s4,
  },
  subtitulo: {
    marginTop: theme.spacing.s8,
    marginBottom: theme.spacing.s4,
    marginHorizontal: theme.spacing.s16,
  },
}));
