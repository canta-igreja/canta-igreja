import React, {FC} from 'react';
import {ScrollView, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {createStyleSheet, UnistylesRuntime, useStyles} from 'react-native-unistyles';
import {UtilStyles} from 'src/arch/style/util.styles';
import {AppStack} from 'src/router/AppStackNavigator.types';
import {AppStackNavigationProps} from 'src/router/navigation/NavigationTypes';
import {MusicaView} from 'src/view/fragment/Musica/MusicaView';

const PreviaMusicaModalScreen: FC<AppStackNavigationProps<AppStack.PreviaMusicaModal>> = ({route}) => {
  const {styles} = useStyles(stylesheet);

  const idMusica = route.params.idMusica;
  const titulo = route.params.titulo?.replace(/\|/g, '');

  return (
    <View style={styles.safeAreaView}>
      <ScrollView style={styles.scroll}>
        <View style={styles.container}>
          <MusicaView titulo={titulo} idMusica={idMusica} />
        </View>
      </ScrollView>
    </View>
  );
};
export default PreviaMusicaModalScreen;

const stylesheet = createStyleSheet((theme, runtime) => ({
  safeAreaView: {
    flex: 1,
    backgroundColor: theme.color.background.level_1,
    ...UtilStyles.safeAreaViewInnerPadding(runtime, {top: false}),
  },
  scroll: {
    backgroundColor: theme.color.background.level_1,
  },
  container: {
    flex: 1,
    paddingHorizontal: theme.spacing.s16,
    paddingVertical: theme.spacing.s8,
  },
}));
