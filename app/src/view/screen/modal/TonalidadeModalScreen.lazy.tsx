import React, {FC, Suspense, lazy} from 'react';
import {ScreenFallback} from 'src/view/component/screen/ScreenFallback';

const TonalidadeModalScreen = lazy(() => import('./TonalidadeModalScreen'));

export const TonalidadeModalScreenLazy: FC = (props: any) => (
  <Suspense fallback={<ScreenFallback />}>
    <TonalidadeModalScreen {...props} />
  </Suspense>
);
