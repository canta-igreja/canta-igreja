import React, {FC, Suspense, lazy} from 'react';
import {ScreenFallback} from 'src/view/component/screen/ScreenFallback';

const MomentoModalScreen = lazy(() => import('./MomentoModalScreen'));

export const MomentoModalScreenLazy: FC = (props: any) => (
  <Suspense fallback={<ScreenFallback />}>
    <MomentoModalScreen {...props} />
  </Suspense>
);
