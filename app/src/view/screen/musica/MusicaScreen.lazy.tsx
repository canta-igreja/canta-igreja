import React, {FC, Suspense, lazy} from 'react';
import {ScreenFallback} from 'src/view/component/screen/ScreenFallback';

const MusicaScreen = lazy(() => import('./MusicaScreen'));

export const MusicaScreenLazy: FC = (props: any) => (
  <Suspense fallback={<ScreenFallback />}>
    <MusicaScreen {...props} />
  </Suspense>
);
