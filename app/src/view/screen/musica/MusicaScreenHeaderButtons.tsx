import {DrawerActions} from '@react-navigation/native';
import {Button} from '@ui-kitten/components';
import {FC, memo, useCallback} from 'react';
import {View} from 'react-native';
import {createStyleSheet} from 'react-native-unistyles';
import {AppStack} from 'src/router/AppStackNavigator.types';
import {useMusicasStackNavigation, useStackRoute} from 'src/router/navigation/useAppNavigation';
import {ConfiguracoesIcon, IncluirEmRepertorioIcon} from 'src/view/base/icones/Icones';
import {FavoritarMusicaAtualBotao} from '../../fragment/Musica/FavoritarMusicaAtualBotao';

export const MusicaScreenHeaderButtons: FC = memo(() => {
  const navigation = useMusicasStackNavigation();
  const route = useStackRoute<AppStack.Musica>();

  const idMusica = route.params.params.idMusica;

  const incluirEmRepertorio = useCallback(
    () => navigation.navigate(AppStack.IncluirMusicaEmRepertorio, {idMusica}),
    [idMusica, navigation],
  );
  const abrirDrawer = useCallback(() => navigation.dispatch(DrawerActions.toggleDrawer()), [navigation]);

  return (
    <View style={stylesheet.container}>
      <FavoritarMusicaAtualBotao idMusica={idMusica} />
      <Button status="basic" appearance="ghost" accessoryLeft={IncluirEmRepertorioIcon} onPress={incluirEmRepertorio} />
      <Button status="basic" appearance="ghost" accessoryLeft={ConfiguracoesIcon} onPress={abrirDrawer} />
    </View>
  );
});

const stylesheet = createStyleSheet({
  container: {
    flexDirection: 'row',
  },
});
