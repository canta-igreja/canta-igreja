import React, {FC} from 'react';
import {ScrollView, View} from 'react-native';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {UtilStyles} from 'src/arch/style/util.styles';
import {MusicaDrawerStack} from 'src/router/MusicaDrawerNavigator.types';
import {MusicaDrawerNavigationProps} from 'src/router/navigation/NavigationTypes';
import {MusicaView} from 'src/view/fragment/Musica/MusicaView';

const MusicaScreen: FC<MusicaDrawerNavigationProps<MusicaDrawerStack.MusicaDrawer>> = ({route}) => {
  const {styles} = useStyles(stylesheet);

  const idMusica = route.params.idMusica;
  const idItemRepertorio = route.params.idItemRepertorio;

  const titulo = route.params.titulo?.replace(/\|/g, '');

  return (
    <View style={styles.safeAreaView}>
      <ScrollView style={styles.scroll}>
        <View style={styles.container}>
          <MusicaView
            titulo={titulo}
            idMusica={idMusica}
            idItemRepertorio={idItemRepertorio}
            marcarComoVisualizada={true}
          />
        </View>
      </ScrollView>
    </View>
  );
};
export default MusicaScreen;

const stylesheet = createStyleSheet((theme, runtime) => {
  return {
    safeAreaView: {
      flex: 1,
      backgroundColor: theme.color.background.level_1,
      ...UtilStyles.safeAreaViewInnerPadding(runtime, {top: false}),
    },
    scroll: {
      backgroundColor: theme.color.background.level_1,
    },
    container: {
      flex: 1,
      paddingHorizontal: theme.spacing.s16,
      paddingVertical: theme.spacing.s8,
    },
  };
});
