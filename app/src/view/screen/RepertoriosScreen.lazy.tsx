import React, {FC, Suspense, lazy} from 'react';
import {ScreenFallback} from '../component/screen/ScreenFallback';

const RepertoriosScreen = lazy(() => import('./RepertoriosScreen'));

export const RepertoriosScreenLazy: FC = (props: any) => (
  <Suspense fallback={<ScreenFallback />}>
    <RepertoriosScreen {...props} />
  </Suspense>
);
