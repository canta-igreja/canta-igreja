import React, {FC, Suspense, lazy} from 'react';
import {ScreenFallback} from 'src/view/component/screen/ScreenFallback';

const MusicasRepertorioPadraoScreen = lazy(() => import('./MusicasRepertorioPadraoScreen'));

export const MusicasRepertorioPadraoScreenLazy: FC = (props: any) => (
  <Suspense fallback={<ScreenFallback />}>
    <MusicasRepertorioPadraoScreen {...props} />
  </Suspense>
);
