import React, {FC, useEffect} from 'react';
import {View} from 'react-native';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {useSelector} from 'react-redux';
import {UtilStyles} from 'src/arch/style/util.styles';
import {AppStack} from 'src/router/AppStackNavigator.types';
import {MusicaDrawerStack} from 'src/router/MusicaDrawerNavigator.types';
import {AppStackNavigationProps} from 'src/router/navigation/NavigationTypes';
import {useAppDispatch} from 'src/store/store.hooks';
import {ListaMusicasAction} from 'src/store/ui/ListaMusicas/ListaMusicas.action';
import {ListaMusicasSelect} from 'src/store/ui/ListaMusicas/ListaMusicas.selector';
import {getWindowByRepertorioPadrao} from 'src/store/ui/ListaMusicas/ListaMusicas.types';
import {TituloPagina} from 'src/view/base/Texto';
import {MusicaItemListData} from 'src/view/component/list/data/MusicaItemListData';
import {MusicasListaEmptyState} from '../fragment/InformationState/MusicasListaEmptyState';
import {SelecaoMusicasFragment} from '../fragment/SelecaoMusicasFragment';

const MusicasRepertorioPadraoScreen: FC<AppStackNavigationProps<AppStack.MusicasRepertorioPadrao>> = ({
  route,
  navigation,
}) => {
  const {styles} = useStyles(stylesheet);

  const window = getWindowByRepertorioPadrao(route.params.repertorio);

  const dispatch = useAppDispatch();

  const musicas = useSelector(ListaMusicasSelect.allIds(window));
  const estadoRequisicao = useSelector(ListaMusicasSelect.estadoRequisicao(window));
  const repertorio = route.params.repertorio;

  const irParaMusica = (musica: MusicaItemListData) =>
    navigation.navigate(AppStack.Musica, {
      screen: MusicaDrawerStack.MusicaDrawer,
      params: {
        idMusica: musica.id_musica,
        titulo: musica.titulo,
        indice: musica,
      },
    });

  useEffect(() => {
    dispatch(ListaMusicasAction.carregarRepertorioPadrao(repertorio));

    return () => {
      dispatch(ListaMusicasAction.limpar(window));
    };
  }, [dispatch, repertorio, window]);

  return (
    <View style={styles.container}>
      <TituloPagina style={styles.titulo}>{repertorio}</TituloPagina>
      <SelecaoMusicasFragment
        estadoRequisicao={estadoRequisicao}
        carregarMais={undefined}
        isFinalResultados={true}
        termoBuscado={undefined}
        musicas={musicas}
        onSelectMusica={irParaMusica}
        EmptyState={MusicasListaEmptyState}
      />
    </View>
  );
};
export default MusicasRepertorioPadraoScreen;

const stylesheet = createStyleSheet((theme, runtime) => ({
  container: {
    flex: 1,
    backgroundColor: theme.color.background.level_1,
    ...UtilStyles.safeAreaViewInnerPadding(runtime, {top: false}),
  },
  titulo: {
    paddingHorizontal: theme.spacing.s16,
    paddingBottom: theme.spacing.s16,
  },
}));
