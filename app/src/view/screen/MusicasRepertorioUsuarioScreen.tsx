import KeepAwake from '@sayem314/react-native-keep-awake';
import {Button, Text} from '@ui-kitten/components';
import React, {FC, useCallback, useEffect, useMemo, useRef} from 'react';
import {NativeSyntheticEvent, View} from 'react-native';
import ContextMenu, {ContextMenuOnPressNativeEvent} from 'react-native-context-menu-view';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {useSelector} from 'react-redux';
import {UtilStyles} from 'src/arch/style/util.styles';
import {Toast} from 'src/arch/util/Toast';
import {ItemRepertorio} from 'src/model/repertorio/Repertorio';
import {AppStack} from 'src/router/AppStackNavigator.types';
import {MusicaDrawerStack} from 'src/router/MusicaDrawerNavigator.types';
import {AppStackNavigationProps} from 'src/router/navigation/NavigationTypes';
import {ItensRepertorioSelect} from 'src/store/ItensRepertorio/ItensRepertorio.selector';
import {ItensRepertorioAction} from 'src/store/ItensRepertorio/ItensRepertorio.action';
import {RepertoriosAction} from 'src/store/Repertorios/Repertorios.action';
import {RepertoriosSelect} from 'src/store/Repertorios/Repertorios.selector';
import {useAppDispatch} from 'src/store/store.hooks';
import {Negrito, TituloPagina} from 'src/view/base/Texto';
import {
  InformacoesAdicionaisBottomSheet,
  InformacoesAdicionaisBottomSheetRef,
} from 'src/view/fragment/MusicasRepertorio/InformacoesAdicionaisBottomSheet';
import {MusicasRepertorioView} from 'src/view/fragment/MusicasRepertorio/MusicasRepertorioView';
import {MaisIcon, TresPontosVerticalIcon} from '../base/icones/Icones';
import {OkCancelarModal} from '../component/modal/OkCancelarModal';
import {useModalState} from '../component/modal/useCloseModalOnBackButton';
import {HeaderScreen} from '../component/screen/HeaderScreen';
import {NomeRepertorioModal} from '../fragment/Repertorios/RepertorioModal';

// FIXME - Swipe left-to-right: tonalidade - https://snack.expo.dev/@computerjazz/swipeable-item
// FIXME - Swipe right-to-left: remover música
export const MusicasRepertorioUsuarioScreen: FC<AppStackNavigationProps<AppStack.MusicasRepertorioUsuario>> = ({
  route,
  navigation,
}) => {
  const {styles} = useStyles(stylesheet);

  const bottomSheetRef = useRef<InformacoesAdicionaisBottomSheetRef>(null);

  const dispatch = useAppDispatch();

  const isCarregando = useSelector(ItensRepertorioSelect.isCarregando);

  const idRepertorio = route.params.idRepertorio;
  const repertorio = useSelector(RepertoriosSelect.byId(idRepertorio));
  const itensRepertorio = useSelector(RepertoriosSelect.itensById(idRepertorio));

  const tituloRepertorio = repertorio?.titulo ?? 'Repertório excluído';

  const [isRenomearOpen, abrirModalRenomear, fecharModalRenomear] = useModalState();
  const [isExcluirOpen, abrirModalExcluir, fecharModalExcluir] = useModalState();

  const compartilharRepertorio = () => repertorio && dispatch(RepertoriosAction.compartilhar(repertorio));
  const gerarFolhetoRepertorio = () => repertorio && dispatch(RepertoriosAction.gerarFolheto(repertorio));

  const onItemSettings = useCallback(
    (item: ItemRepertorio) => bottomSheetRef.current?.open(item.id_item_repertorio),
    [],
  );
  const updateInformacoesMusica = useCallback(
    (item: ItemRepertorio) => dispatch(ItensRepertorioAction.atualizarItem(item)),
    [dispatch],
  );

  const deleteMusica = (item: ItemRepertorio) => {
    repertorio &&
      dispatch(RepertoriosAction.removerItem({repertorio, item})) //
        .then(() => Toast.show(`"${item.termo ?? item.musica?.titulo ?? 'Música'}" removida`));
  };
  const reposicionarMusica = useCallback(
    (posicaoOrigem: number, posicaoDestino: number) => {
      repertorio && dispatch(ItensRepertorioAction.reposicionarItem({repertorio, posicaoOrigem, posicaoDestino}));
    },
    [dispatch, repertorio],
  );
  const renomearRepertorio = (novoNome: string) => {
    repertorio &&
      dispatch(RepertoriosAction.renomear({repertorio, novoTitulo: novoNome})) //
        .then(() => fecharModalRenomear());
  };
  const excluirRepertorio = () => {
    repertorio &&
      dispatch(RepertoriosAction.excluirRepertorio(repertorio)) //
        .then(() => fecharModalExcluir())
        .then(() => navigation.goBack())
        .then(() => Toast.show(`Repertório "${repertorio.titulo}" excluído`));
  };

  const irParaMusica = useCallback(
    (item: ItemRepertorio) => {
      item.musica &&
        navigation.navigate(AppStack.Musica, {
          screen: MusicaDrawerStack.MusicaDrawer,
          params: {
            idItemRepertorio: item.id_item_repertorio,
            idMusica: item.musica.id_musica,
            titulo: item.musica.titulo,
          },
        });
    },
    [navigation],
  );
  const buscarMusicaParaIncluir = useCallback(
    () =>
      repertorio && navigation.navigate(AppStack.BuscaMusicaParaRepertorio, {idRepertorio: repertorio.id_repertorio}),
    [navigation, repertorio],
  );

  useEffect(() => {
    dispatch(ItensRepertorioAction.carregar(idRepertorio));

    return () => {
      dispatch(ItensRepertorioAction.limpar());
    };
  }, [dispatch, idRepertorio]);

  return (
    <View style={styles.container}>
      <KeepAwake />
      <HeaderScreen>
        <TituloPagina style={styles.titulo}>{tituloRepertorio}</TituloPagina>
        <View style={styles.iconeContainer}>
          <Button
            status="basic"
            appearance="ghost"
            accessoryLeft={MaisIcon}
            onPress={buscarMusicaParaIncluir}
            accessibilityLabel="Incluir música"
          />
          <Opcoes
            onCompartilhar={compartilharRepertorio}
            onGerarFolheto={gerarFolhetoRepertorio}
            onRenomear={abrirModalRenomear}
            onExcluir={abrirModalExcluir}
          />
        </View>
      </HeaderScreen>
      <MusicasRepertorioView
        isCarregando={isCarregando}
        itens={itensRepertorio}
        onSelectMusica={irParaMusica}
        onSwapMusicas={reposicionarMusica}
        onItemSettings={onItemSettings}
      />

      <InformacoesAdicionaisBottomSheet
        ref={bottomSheetRef}
        onUpdate={updateInformacoesMusica}
        onDelete={deleteMusica}
      />
      {isRenomearOpen && (
        <NomeRepertorioModal
          titulo="Renomear repertório"
          okLabel="Salvar"
          isOpen={isRenomearOpen}
          valorInicial={tituloRepertorio}
          onConfirmado={renomearRepertorio}
          onCancelado={fecharModalRenomear}
        />
      )}
      {isExcluirOpen && (
        <OkCancelarModal
          titulo="Excluir repertório"
          okLabel="Excluir"
          isOpen={isExcluirOpen}
          onOk={excluirRepertorio}
          onCancelado={fecharModalExcluir}>
          <>
            <Text>
              Você deseja excluir <Negrito>{tituloRepertorio}</Negrito>?
            </Text>
            <Text appearance="hint">Depois de excluído, não será possível acessá-lo novamente</Text>
          </>
        </OkCancelarModal>
      )}
    </View>
  );
};
export default MusicasRepertorioUsuarioScreen;

interface OpcoesProps {
  onCompartilhar: () => void;
  onGerarFolheto: () => void;
  onRenomear: () => void;
  onExcluir: () => void;
}
const Opcoes: FC<OpcoesProps> = ({onCompartilhar, onRenomear, onExcluir, onGerarFolheto}) => {
  const {styles} = useStyles(stylesheet);

  const onPress = (e: NativeSyntheticEvent<ContextMenuOnPressNativeEvent>) => {
    if (e.nativeEvent.index === 0) {
      onCompartilhar();
    } else if (e.nativeEvent.index === 1) {
      onRenomear();
    } else if (e.nativeEvent.index === 2) {
      onGerarFolheto();
    } else if (e.nativeEvent.index === 3) {
      onExcluir();
    }
  };

  const opcoes = useMemo(
    () => [{title: 'Compartilhar'}, {title: 'Renomear'}, {title: 'Gerar folheto'}, {title: 'Excluir repertório'}],
    [],
  );

  return (
    <ContextMenu style={styles.contextMenu} actions={opcoes} dropdownMenuMode onPress={onPress}>
      <Button status="basic" appearance="ghost" accessoryLeft={TresPontosVerticalIcon} accessibilityLabel={'Opções'} />
    </ContextMenu>
  );
};

const stylesheet = createStyleSheet((theme, runtime) => ({
  container: {
    flex: 1,
    backgroundColor: theme.color.background.level_1,
    ...UtilStyles.safeAreaViewInnerPadding(runtime, {top: false}),
  },
  titulo: {
    paddingHorizontal: theme.spacing.s16,
  },

  iconeContainer: {
    flexDirection: 'row',
  },
  contextMenu: {
    justifyContent: 'center',
  },
}));
