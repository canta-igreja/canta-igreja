import {Layout, List, Spinner, Text} from '@ui-kitten/components';
import React, {FC, useCallback, useEffect, useMemo} from 'react';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {useSelector} from 'react-redux';
import {AppStack} from 'src/router/AppStackNavigator.types';
import {AppStackNavigationProps} from 'src/router/navigation/NavigationTypes';
import {LivrosAction} from 'src/store/Livros/Livros.action';
import {LivrosSelect} from 'src/store/Livros/Livros.selector';
import {useAppDispatch} from 'src/store/store.hooks';
import {EdicaoLivro} from '../../model/Livro';
import {TituloPagina} from '../base/Texto';
import {buildLivroItemList} from '../component/list/item/LivroItemList';
import {HeaderScreen} from '../component/screen/HeaderScreen';

export const LivrosScreen: FC<AppStackNavigationProps<AppStack.Inicio>> = ({navigation}) => {
  const {styles} = useStyles(stylesheet);

  const dispatch = useAppDispatch();
  const livros = useSelector(LivrosSelect.allIds);
  const isCarregando = useSelector(LivrosSelect.isCarregando);

  const irParaLivro = useCallback(
    (livro: EdicaoLivro) => navigation.navigate(AppStack.Livro, {idEdicaoLivro: livro.id_edicao_livro}),
    [navigation],
  );

  const ItemListView = useMemo(() => buildLivroItemList(irParaLivro), [irParaLivro]);

  useEffect(() => {
    dispatch(LivrosAction.carregar());
  }, [dispatch]);

  return (
    <Layout style={styles.layout} level="3">
      <HeaderScreen>
        <TituloPagina>Livros</TituloPagina>
        {isCarregando && <Spinner size="medium" />}
      </HeaderScreen>
      <Text>Fluxo para testar se as informações dos índices das músicas funcionam</Text>

      {/* <LivrosView livros={livros} onSelectLivro={irParaLivro} /> */}
      <List
        // style={styles.container}
        data={livros}
        renderItem={ItemListView}
      />
    </Layout>
  );
};

const stylesheet = createStyleSheet(theme => ({
  layout: {
    flex: 1,
    paddingHorizontal: theme.spacing.s16,
    paddingTop: theme.spacing.s8,
  },
}));
