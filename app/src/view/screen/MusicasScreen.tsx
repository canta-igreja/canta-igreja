import React, {FC} from 'react';
import {View} from 'react-native';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {UtilStyles} from 'src/arch/style/util.styles';
import {HomeStack} from 'src/router/HomeBottomNavigator.types';
import {HomeNavigationProps} from 'src/router/navigation/NavigationTypes';
import {BuscaMusicasView} from '../fragment/Musicas/BuscaMusicasView';

export const MusicasScreen: FC<HomeNavigationProps<HomeStack.Repertorios>> = ({}) => {
  const {styles} = useStyles(stylesheet);

  return (
    <View style={styles.safeAreaView}>
      <BuscaMusicasView />
    </View>
  );
};

const stylesheet = createStyleSheet((theme, runtime) => ({
  safeAreaView: {
    flex: 1,
    backgroundColor: theme.color.background.level_1,
    ...UtilStyles.safeAreaViewInnerPadding(runtime, {bottom: false}),
  },
}));
