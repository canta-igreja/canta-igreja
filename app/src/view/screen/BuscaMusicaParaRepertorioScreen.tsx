import {Layout, ListItem} from '@ui-kitten/components';
import debounce from 'debounce-promise';
import React, {FC, useCallback, useEffect} from 'react';
import {View} from 'react-native';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {useSelector} from 'react-redux';
import {UtilStyles} from 'src/arch/style/util.styles';
import {Toast} from 'src/arch/util/Toast';
import {ItemRepertorio} from 'src/model/repertorio/Repertorio';
import {AppStack} from 'src/router/AppStackNavigator.types';
import {AppStackNavigationProps} from 'src/router/navigation/NavigationTypes';
import {RepertoriosAction} from 'src/store/Repertorios/Repertorios.action';
import {RepertoriosSelect} from 'src/store/Repertorios/Repertorios.selector';
import {useAppDispatch} from 'src/store/store.hooks';
import {ListaMusicasAction} from 'src/store/ui/ListaMusicas/ListaMusicas.action';
import {ListaMusicasSelect} from 'src/store/ui/ListaMusicas/ListaMusicas.selector';
import {ListaMusicasWindow} from 'src/store/ui/ListaMusicas/ListaMusicas.types';
import {MusicaItemListData} from 'src/view/component/list/data/MusicaItemListData';
import {PesquisarInput} from '../component/input/PesquisarInput';
import {SelecaoMusicasFragment} from '../fragment/SelecaoMusicasFragment';

const BuscaMusicaParaRepertorioScreen: FC<AppStackNavigationProps<AppStack.BuscaMusicaParaRepertorio>> = ({
  navigation,
  route,
}) => {
  const {styles} = useStyles(stylesheet);

  const dispatch = useAppDispatch();
  const repertorio = useSelector(RepertoriosSelect.byId(route.params.idRepertorio));

  const musicas = useSelector(ListaMusicasSelect.allIds(ListaMusicasWindow.buscaMusicasParaRepertorio));

  const termoBuscado = useSelector(ListaMusicasSelect.termoBuscado(ListaMusicasWindow.buscaMusicasParaRepertorio));
  const estadoRequisicao = useSelector(
    ListaMusicasSelect.estadoRequisicao(ListaMusicasWindow.buscaMusicasParaRepertorio),
  );
  const isFinalResultados = useSelector(
    ListaMusicasSelect.isFinalResultados(ListaMusicasWindow.buscaMusicasParaRepertorio),
  );

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const pesquisar = useCallback(
    debounce(
      (termoBusca: string) =>
        dispatch(
          ListaMusicasAction.buscar(ListaMusicasWindow.buscaMusicasParaRepertorio, {
            termoBusca,
          }),
        ),
      500,
    ),
    [dispatch],
  );
  const carregarMais = useCallback(
    () => dispatch(ListaMusicasAction.paginaSeguinte(ListaMusicasWindow.buscaMusicasParaRepertorio)),
    [dispatch],
  );

  const onSelectMusica = (item: MusicaItemListData) =>
    navigation.navigate(AppStack.PreviaMusicaModal, {idMusica: item.id_musica, titulo: item.titulo});

  // FIXME - Duplicação de código
  const onAddMusica = useCallback(
    (item: MusicaItemListData) => {
      repertorio &&
        dispatch(RepertoriosAction.incluirItem({repertorio, musica: item})) //
          .then(it => {
            const itemPersistido = (it.payload as any).item as ItemRepertorio;
            navigation.pop();
            Toast.show(`Música ${itemPersistido.musica?.titulo} incluída em ${repertorio.titulo}`);
          });
    },
    [dispatch, navigation, repertorio],
  );
  const onAddTermoMusica = useCallback(() => {
    repertorio &&
      dispatch(RepertoriosAction.incluirItem({repertorio, termo: termoBuscado})) //
        .then(it => {
          const itemPersistido = (it.payload as any).item as ItemRepertorio;
          navigation.pop();
          Toast.show(`Termo ${itemPersistido.termo} incluído em ${repertorio.titulo}`);
        });
  }, [dispatch, navigation, repertorio, termoBuscado]);

  useEffect(() => {
    return () => {
      dispatch(ListaMusicasAction.limpar(ListaMusicasWindow.buscaMusicasParaRepertorio));
    };
  }, [dispatch]);

  return (
    <View style={styles.safeAreaView}>
      <Layout style={styles.container}>
        <PesquisarInput onPesquisar={pesquisar} considerarNumero={false} />

        {termoBuscado && <ListItem title={`Incluir termo "${termoBuscado}" como música`} onPress={onAddTermoMusica} />}

        <SelecaoMusicasFragment
          estadoRequisicao={estadoRequisicao}
          carregarMais={carregarMais}
          isFinalResultados={isFinalResultados}
          termoBuscado={termoBuscado}
          musicas={musicas}
          onSelectMusica={onSelectMusica}
          onAddMusica={onAddMusica}
        />
      </Layout>
    </View>
  );
};
export default BuscaMusicaParaRepertorioScreen;

const stylesheet = createStyleSheet((theme, runtime) => ({
  safeAreaView: {
    flex: 1,
    backgroundColor: theme.color.background.level_1,
    ...UtilStyles.safeAreaViewInnerPadding(runtime, {top: false}),
  },
  container: {
    flex: 1,
  },
}));
