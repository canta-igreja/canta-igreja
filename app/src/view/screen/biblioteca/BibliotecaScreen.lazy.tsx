import React, {FC, Suspense, lazy} from 'react';
import {ScreenFallback} from 'src/view/component/screen/ScreenFallback';

const BibliotecaScreen = lazy(() => import('./BibliotecaScreen'));

export const BibliotecaScreenLazy: FC = (props: any) => (
  <Suspense fallback={<ScreenFallback />}>
    <BibliotecaScreen {...props} />
  </Suspense>
);
