import {FC} from 'react';

import {ErrorState} from 'src/view/fragment/InformationState/ErrorState';
import {ErrorInternetEmptyState} from 'src/view/fragment/InformationState/Icones/EmptyStateIcones';

interface Props {
  retry: () => void;
}

export const BibliotecaPageErrorState: FC<Props> = ({retry}) => {
  return (
    <ErrorState
      image={ErrorInternetEmptyState}
      header="Erro ao acessar biblioteca"
      description={'Talvez você esteja sem internet'}
      buttonText="Tentar novamente"
      onAction={retry}
    />
  );
};

export const BibliotecaModalErrorState: FC<Props> = ({retry}) => {
  return (
    <ErrorState
      description={'Não foi possível consultar as músicas disponíveis para serem baixadas'}
      buttonText="Tentar novamente"
      onAction={retry}
    />
  );
};
