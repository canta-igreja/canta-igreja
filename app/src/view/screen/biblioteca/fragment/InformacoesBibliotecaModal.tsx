import {Text} from '@ui-kitten/components';
import React, {FC, useCallback} from 'react';
import {ObraItemListData, SituacaoItemObra} from 'src/view/component/list/data/ObraItemListData';
import {OkModal} from 'src/view/component/modal/OkModal';
import {ObraItemListView} from '../../../component/list/item/ObraItemList';

interface Props {
  isOpen: boolean;
  fecharModal: () => void;
}

const itemNaoBaixado: ObraItemListData = {
  situacao: SituacaoItemObra.NAO_BAIXADA,
  obra: {
    id_obra: 1,
    titulo: 'Não baixada',
    descricao: 'A coletânea de letras ainda não foi baixada',
    disponivel: true,
  },
};

const itemDesatualizado: ObraItemListData = {
  situacao: SituacaoItemObra.NAO_ATUALIZADA,
  obra: {
    id_obra: 2,
    titulo: 'Nova atualização disponível',
    descricao: 'A atualização pode incluir novas letras, adicionar e corrigir informações',
    disponivel: true,
  },
};

const itemAtualizado: ObraItemListData = {
  situacao: SituacaoItemObra.ATUALIZADA,
  obra: {
    id_obra: 3,
    titulo: 'Coletânea atualizada',
    descricao: 'As informações estão atualizadas',
    disponivel: true,
  },
};

export const InformacoesBibliotecaModal: FC<Props> = ({isOpen, fecharModal}) => {
  const doNothing = useCallback(() => {}, []);

  return (
    <OkModal isOpen={isOpen} titulo="Sobre a Biblioteca" fecharModal={fecharModal}>
      <>
        <Text>Baixe novas letras para o seu celular e acesse sem precisar de internet.</Text>

        <ObraItemListView disabled item={itemNaoBaixado} onPress={doNothing} />
        <ObraItemListView disabled item={itemDesatualizado} onPress={doNothing} />
        <ObraItemListView disabled item={itemAtualizado} onPress={doNothing} />
      </>
    </OkModal>
  );
};
