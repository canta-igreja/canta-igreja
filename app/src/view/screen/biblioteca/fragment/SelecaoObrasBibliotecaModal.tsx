import {CheckBox, ListItem, Text} from '@ui-kitten/components';
import React, {FC, useCallback, useEffect, useMemo, useState} from 'react';
import {View} from 'react-native';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {useSelector} from 'react-redux';
import {Obra} from 'src/model/obra/Obra';
import {BibliotecaAction} from 'src/store/Biblioteca/Biblioteca.action';
import {BibliotecaApi} from 'src/store/Biblioteca/Biblioteca.api';
import {useAppDispatch} from 'src/store/store.hooks';
import {ModalObrasAction} from 'src/store/ui/ModalObras/ModalObras.action';
import {ModalObrasSelect} from 'src/store/ui/ModalObras/ModalObras.selector';
import {CarregandoIcone} from 'src/view/base/icones/CarregandoIcone';
import {ObraItemListData, SituacaoItemObra} from 'src/view/component/list/data/ObraItemListData';
import {OkModal} from 'src/view/component/modal/OkModal';
import {BibliotecaModalErrorState} from './BibliotecaErrorState';

interface Props {
  isOpen: boolean;
}

export const SelecaoObrasBibliotecaModal: FC = () => {
  const isModalObrasOpen = useSelector(ModalObrasSelect.isAberto);

  return <>{isModalObrasOpen && <SelecaoObrasBibliotecaOpenedModal isOpen={isModalObrasOpen} />}</>;
};

const SelecaoObrasBibliotecaOpenedModal: FC<Props> = ({isOpen}) => {
  const {styles} = useStyles(stylesheet);

  const dispatch = useAppDispatch();
  const {obrasSelecionadas, onObraSelect} = useObrasABaixar();

  const {data, isLoading, isError, refetch} = BibliotecaApi.useGetItensObrasQuery();

  useEffect(() => {
    refetch();
  }, [refetch]);

  const fecharModal = useCallback(async () => {
    const obrasDesatualizadas =
      data?.filter(it => it.situacao === SituacaoItemObra.NAO_ATUALIZADA).map(it => it.obra) ?? [];

    dispatch(ModalObrasAction.fechar());
    await dispatch(BibliotecaAction.baixarObras([...obrasSelecionadas, ...obrasDesatualizadas]));
  }, [dispatch, data, obrasSelecionadas]);

  return (
    <OkModal isOpen={isOpen} titulo="Biblioteca" fecharModal={fecharModal}>
      <>
        <Text style={styles.question}>Quais as músicas deseja ter no aplicativo?</Text>
        <Text style={styles.explanation} appearance="hint" category="p2">
          As músicas ficam salvas e serão atualizadas automaticamente. Você pode selecionar mais em qualquer momento.
        </Text>

        {isLoading && <CarregandoIcone />}
        {isError && <BibliotecaModalErrorState retry={refetch} />}
        <View>{data && <Itens itens={data} onObraSelect={onObraSelect} />}</View>
      </>
    </OkModal>
  );
};

interface ItensProps {
  itens: ObraItemListData[];
  onObraSelect: (obra: Obra, selecionada: boolean) => void;
}

const Itens: FC<ItensProps> = ({itens, onObraSelect}) => {
  const [selecaoItens, setSelecaoItens] = useState<{[key: number]: boolean}>({});

  return (
    <>
      {/* <ListItem accessoryLeft={<CheckBox checked={false} />} title={'Todas'} />
      <Divider /> */}
      {itens.map(it => {
        const selecionavel = it.situacao === SituacaoItemObra.NAO_BAIXADA;
        const selecionada = !selecionavel || selecaoItens[it.obra.id_obra];

        const onPress = () => {
          if (!selecionavel) {
            return;
          }
          onObraSelect(it.obra, !selecionada);
          setSelecaoItens(selecao => ({
            ...selecao,
            [it.obra.id_obra]: !selecionada,
          }));
        };

        return (
          <ListItem
            accessoryLeft={<CheckBox checked={selecionada} disabled={!selecionavel} onChange={onPress} />}
            title={it.obra.titulo}
            description={it.obra.descricao.trim()}
            onPress={onPress}
          />
        );
      })}
    </>
  );
};

const useObrasABaixar = () => {
  const obrasSelecionadas = useMemo(() => new Set<Obra>(), []);

  const onObraSelect = useCallback(
    (obra: Obra, selecionada: boolean) => {
      if (selecionada) {
        obrasSelecionadas.add(obra);
      } else {
        obrasSelecionadas.delete(obra);
      }
    },
    [obrasSelecionadas],
  );

  return useMemo(
    () => ({
      obrasSelecionadas,
      onObraSelect,
    }),
    [obrasSelecionadas, onObraSelect],
  );
};

const stylesheet = createStyleSheet(theme => ({
  question: {
    paddingBottom: theme.spacing.s4,
  },
  explanation: {
    paddingBottom: theme.spacing.s2,
  },
}));
