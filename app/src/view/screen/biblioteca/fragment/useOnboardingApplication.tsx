import NetInfo from '@react-native-community/netinfo';
import * as Sentry from '@sentry/react-native';
import {useCallback, useEffect, useMemo} from 'react';
import {InteractionManager} from 'react-native';
import {wait} from 'src/arch/util/delay';
import {ConfiguracaoAplicacao, ConfiguracoesAplicacaoRepository} from 'src/repository/ConfiguracoesAplicacaoRepository';
import {BibliotecaAction} from 'src/store/Biblioteca/Biblioteca.action';
import {BibliotecaApi} from 'src/store/Biblioteca/Biblioteca.api';
import {useAppDispatch} from 'src/store/store.hooks';
import {ModalObrasAction} from 'src/store/ui/ModalObras/ModalObras.action';
import {SituacaoItemObra} from 'src/view/component/list/data/ObraItemListData';

export function useOnboardingApplication() {
  const dispatch = useAppDispatch();

  const selecaoObrasOnboarding = useMemo(
    () => ConfiguracoesAplicacaoRepository.getBoolean(ConfiguracaoAplicacao.ONBOARDING_MODAL),
    [],
  );

  const [get] = BibliotecaApi.useLazyGetItensObrasQuery();

  const getResource = useCallback(async () => {
    let tentativa = 0;
    const maximoTentativas = 10;
    const INTERVALO = 0.5 * 60 * 1000;

    while (tentativa < maximoTentativas) {
      tentativa += 1;

      await wait(tentativa * INTERVALO);
      console.log(`Atualizando banco - Tentativa ${tentativa} de ${maximoTentativas}`);

      const netStatus = await NetInfo.fetch();

      if (!netStatus.isConnected) {
        console.log(`Tentativa ${tentativa} - Dispositivo sem conexão com a internet`);
        continue;
      }

      try {
        console.log(`Tentativa ${tentativa} - Solicitando obras`);
        const obras = await get().unwrap();

        const obrasDesatualizadas = obras
          .filter(it => it.situacao === SituacaoItemObra.NAO_ATUALIZADA)
          .map(it => it.obra);

        await dispatch(BibliotecaAction.baixarObras(obrasDesatualizadas));

        console.log(`Tentativa ${tentativa} - Bem suscedida`);
        return;
      } catch (e) {
        Sentry.captureMessage(`Tentativa ${tentativa}`);
        Sentry.captureException(e);

        console.error(e);
        console.log(`Tentativa ${tentativa}`, e);
      }
    }
  }, [dispatch, get]);

  useEffect(() => {
    if (!selecaoObrasOnboarding) {
      dispatch(ModalObrasAction.abrir());
      ConfiguracoesAplicacaoRepository.persistir(ConfiguracaoAplicacao.ONBOARDING_MODAL, true);
    } else {
      return InteractionManager.runAfterInteractions(() => getResource()).cancel;
    }
  }, [dispatch, getResource, selecaoObrasOnboarding]);
}
