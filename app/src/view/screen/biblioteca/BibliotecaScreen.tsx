import {Layout, ListItem, Text} from '@ui-kitten/components';
import React, {FC, useCallback} from 'react';
import {View} from 'react-native';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {UtilStyles} from 'src/arch/style/util.styles';
import {AppStack} from 'src/router/AppStackNavigator.types';
import {AppStackNavigationProps} from 'src/router/navigation/NavigationTypes';
import {BibliotecaLink} from 'src/service/BibliotecaService';
import {BibliotecaAction} from 'src/store/Biblioteca/Biblioteca.action';
import {useAppDispatch} from 'src/store/store.hooks';
import {ModalObrasAction} from 'src/store/ui/ModalObras/ModalObras.action';
import {ItemListaArredondada, ListaArredondada} from 'src/view/base/ListaArredondada';
import {SubtituloPagina, TituloPagina} from 'src/view/base/Texto';
import {BaixarIcon, LivroAbertoIcon, RepertoriosIcon} from 'src/view/base/icones/Icones';
import {HeaderScreen} from 'src/view/component/screen/HeaderScreen';

const BibliotecaScreen: FC<AppStackNavigationProps<AppStack.Inicio>> = ({}) => {
  const {styles} = useStyles(stylesheet);

  const dispatch = useAppDispatch();
  const abrirModalObras = useCallback(() => dispatch(ModalObrasAction.abrir()), [dispatch]);

  const openLiturgiaDiaria = useCallback(
    async () => dispatch(BibliotecaAction.abrirLink(BibliotecaLink.LITURIA_DIARIA)),
    [dispatch],
  );
  const openSugestoesRepertorio = useCallback(
    async () => dispatch(BibliotecaAction.abrirLink(BibliotecaLink.SUGESTOES_REPERTORIO)),
    [dispatch],
  );

  return (
    <View style={styles.safeAreaView}>
      <Layout style={styles.layout} level="1">
        <View style={styles.titulo}>
          <HeaderScreen>
            <TituloPagina>Biblioteca</TituloPagina>
          </HeaderScreen>
        </View>

        <ListaArredondada>
          <ItemListaArredondada
            tipo="unico"
            title="Letras de músicas baixadas"
            accessoryLeft={<BaixarIcon />}
            onPress={abrirModalObras}
          />
        </ListaArredondada>

        <View>
          <SubtituloPagina style={styles.subtitulo}>Para servir bem na missa</SubtituloPagina>
          <Text category="p2" style={styles.descricaoSubsecao}>
            Materiais da CNBB
          </Text>

          <ListItem accessoryLeft={LivroAbertoIcon} title="Liturgia diária" onPress={openLiturgiaDiaria} />
          <ListItem accessoryLeft={RepertoriosIcon} title="Sugestões de repertório" onPress={openSugestoesRepertorio} />
          {/* <ListItem title="Documentos" /> */}
        </View>
      </Layout>
    </View>
  );
};
export default BibliotecaScreen;

const stylesheet = createStyleSheet((theme, runtime) => ({
  safeAreaView: {
    flex: 1,
    backgroundColor: theme.color.background.level_1,
    ...UtilStyles.safeAreaViewInnerPadding(runtime, {bottom: false}),
    paddingTop: runtime.orientation === 'landscape' ? runtime.insets.top : runtime.insets.top + theme.spacing.s8,
  },
  layout: {
    flex: 1,
    width: '100%',
  },
  titulo: {
    paddingHorizontal: theme.spacing.s16,
  },
  subtitulo: {
    paddingHorizontal: theme.spacing.s16,
    paddingBottom: theme.spacing.s4,
  },
  descricaoSubsecao: {
    paddingHorizontal: theme.spacing.s16,
    paddingBottom: theme.spacing.s8,
  },
}));
