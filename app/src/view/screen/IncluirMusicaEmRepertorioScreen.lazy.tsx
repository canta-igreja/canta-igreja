import React, {FC, Suspense, lazy} from 'react';
import {ScreenFallback} from 'src/view/component/screen/ScreenFallback';

const IncluirMusicaEmRepertorioScreen = lazy(() => import('./IncluirMusicaEmRepertorioScreen'));

export const IncluirMusicaEmRepertorioScreenLazy: FC = (props: any) => (
  <Suspense fallback={<ScreenFallback />}>
    <IncluirMusicaEmRepertorioScreen {...props} />
  </Suspense>
);
