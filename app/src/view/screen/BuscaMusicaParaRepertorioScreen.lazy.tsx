import React, {FC, Suspense, lazy} from 'react';
import {ScreenFallback} from 'src/view/component/screen/ScreenFallback';

const BuscaMusicaParaRepertorioScreen = lazy(() => import('./BuscaMusicaParaRepertorioScreen'));

export const BuscaMusicaParaRepertorioScreenLazy: FC = (props: any) => (
  <Suspense fallback={<ScreenFallback />}>
    <BuscaMusicaParaRepertorioScreen {...props} />
  </Suspense>
);
