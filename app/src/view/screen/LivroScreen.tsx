import {Text} from '@ui-kitten/components';
import React, {FC, useEffect} from 'react';
import {View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {createStyleSheet} from 'react-native-unistyles';
import {EdicaoLivro} from 'src/model/Livro';
import {AppStack} from 'src/router/AppStackNavigator.types';
import {AppStackNavigationProps} from 'src/router/navigation/NavigationTypes';
import {LivrosAction} from 'src/store/Livros/Livros.action';
import {LivrosSelect} from 'src/store/Livros/Livros.selector';
import {useAppDispatch, useAppSelector} from 'src/store/store.hooks';
import {BuscaMusicasView} from 'src/view/fragment/Musicas/BuscaMusicasView';
import {Pilula} from '../base/Pilula';
import {TituloPagina} from '../base/Texto';
import {LivroIcon, SecoesIcon} from '../base/icones/Icones';

export const LivroScreen: FC<AppStackNavigationProps<AppStack.Livro>> = ({route, navigation}) => {
  const dispatch = useAppDispatch();

  const idLivro = route.params.idEdicaoLivro;
  const livro = useAppSelector(LivrosSelect.byId(idLivro));
  const irParaLivroDetalhes = () =>
    livro && navigation.navigate(AppStack.LivroDetalhes, {idEdicaoLivro: livro.id_edicao_livro});

  useEffect(() => {
    dispatch(LivrosAction.carregarLivro(idLivro));
  }, [dispatch, idLivro]);

  return (
    <SafeAreaView style={stylesheet.container}>
      {livro && <LivroHeader livro={livro} irParaLivroDetalhes={irParaLivroDetalhes} />}

      <BuscaMusicasView livro={livro} />
    </SafeAreaView>
  );
};

interface LivroHeaderProps {
  livro: EdicaoLivro;
  irParaLivroDetalhes: () => void;
}
const LivroHeader: FC<LivroHeaderProps> = ({livro, irParaLivroDetalhes}) => {
  return (
    <View style={stylesheet.header}>
      <TituloPagina>{livro.titulo}</TituloPagina>
      <Text appearance="hint">
        {livro.edicao} - {livro.editora}
      </Text>
      <View style={stylesheet.buttonsContainer}>
        <Pilula disabled={true} status="basic" onPress={irParaLivroDetalhes}>
          <View style={stylesheet.buttonsContent}>
            <SecoesIcon style={stylesheet.icone} />
            <Text style={stylesheet.texto}>Seções</Text>
          </View>
        </Pilula>
        <Pilula status="basic" onPress={irParaLivroDetalhes}>
          <View style={stylesheet.buttonsContent}>
            <LivroIcon style={stylesheet.icone} />
            <Text style={stylesheet.texto}>Informações</Text>
          </View>
        </Pilula>
      </View>
    </View>
  );
};

const stylesheet = createStyleSheet({
  container: {
    flex: 1,
  },
  header: {
    alignItems: 'flex-start',
    paddingTop: 0,
    paddingBottom: 4,
    paddingHorizontal: 16,
    backgroundColor: '#fff',
  },
  buttonsContainer: {
    paddingTop: 12,
    flexDirection: 'row',
  },
  buttonsContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icone: {
    width: 20,
    height: 20,
    tintColor: '#2E3A59',
  },
  texto: {
    color: '#2E3A59',
    paddingLeft: 4,
  },
});
