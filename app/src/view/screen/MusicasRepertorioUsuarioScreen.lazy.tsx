import React, {FC, Suspense, lazy} from 'react';
import {ScreenFallback} from 'src/view/component/screen/ScreenFallback';

const MusicasRepertorioUsuarioScreen = lazy(() => import('./MusicasRepertorioUsuarioScreen'));

export const MusicasRepertorioUsuarioScreenLazy: FC = (props: any) => (
  <Suspense fallback={<ScreenFallback />}>
    <MusicasRepertorioUsuarioScreen {...props} />
  </Suspense>
);
