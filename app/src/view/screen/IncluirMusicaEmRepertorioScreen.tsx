import {Button} from '@ui-kitten/components';
import React, {FC, useCallback, useEffect} from 'react';
import {View} from 'react-native';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {useSelector} from 'react-redux';
import {UtilStyles} from 'src/arch/style/util.styles';
import {Toast} from 'src/arch/util/Toast';
import {Repertorio} from 'src/model/repertorio/Repertorio';
import {AppStack} from 'src/router/AppStackNavigator.types';
import {AppStackNavigationProps} from 'src/router/navigation/NavigationTypes';
import {MusicasSelect} from 'src/store/Musicas/Musicas.selector';
import {RepertoriosAction} from 'src/store/Repertorios/Repertorios.action';
import {useAppDispatch} from 'src/store/store.hooks';
import {CriarRepertorioModal} from 'src/view/fragment/Repertorios/RepertorioModal';
import {RepertoriosFragment} from 'src/view/fragment/Repertorios/RepertoriosFragment';
import {TituloPagina} from '../base/Texto';
import {MaisIcon} from '../base/icones/Icones';
import {useModalState} from '../component/modal/useCloseModalOnBackButton';
import {HeaderScreen} from '../component/screen/HeaderScreen';

const IncluirMusicaEmRepertorioScreen: FC<AppStackNavigationProps<AppStack.IncluirMusicaEmRepertorio>> = ({
  navigation,
  route,
}) => {
  const {styles} = useStyles(stylesheet);

  const idMusica = route.params.idMusica;

  const dispatch = useAppDispatch();
  const musica = useSelector(MusicasSelect.byId(idMusica));
  const [isCriarRepertorioOpen, abrirModalCriarRepertorio, fecharModalCriarRepertorio] = useModalState();

  const salvarEmRepertorio = useCallback(
    async (repertorio: Repertorio) => {
      musica &&
        (await dispatch(RepertoriosAction.incluirItem({repertorio, musica}))
          .then(() => navigation.pop())
          .then(() => Toast.show(`Música ${musica.titulo} incluída em ${repertorio.titulo}`)));
    },
    [dispatch, musica, navigation],
  );

  const criarRepertorioIncluindoMusica = useCallback(
    (repertorioCriado: Repertorio) => {
      salvarEmRepertorio(repertorioCriado) //
        .then(() => fecharModalCriarRepertorio());
    },
    [fecharModalCriarRepertorio, salvarEmRepertorio],
  );

  useEffect(() => {
    dispatch(RepertoriosAction.carregar());
  }, [dispatch]);

  return (
    <View style={styles.safeAreaView}>
      <HeaderScreen>
        <TituloPagina style={styles.titulo}>Salvar no repertório</TituloPagina>
        <Button
          status="basic"
          appearance="ghost"
          accessoryLeft={MaisIcon}
          onPress={abrirModalCriarRepertorio}
          accessibilityLabel={'Criar novo repertório'}
        />
      </HeaderScreen>

      <RepertoriosFragment onRepertorioSelected={salvarEmRepertorio} />

      <CriarRepertorioModal
        onCancelado={fecharModalCriarRepertorio}
        onCriado={criarRepertorioIncluindoMusica}
        isOpen={isCriarRepertorioOpen}
      />
    </View>
  );
};
export default IncluirMusicaEmRepertorioScreen;

const stylesheet = createStyleSheet((theme, runtime) => ({
  safeAreaView: {
    flex: 1,
    backgroundColor: theme.color.background.level_1,
    ...UtilStyles.safeAreaViewInnerPadding(runtime, {top: false}),
    paddingTop: theme.spacing.s8,
  },
  titulo: {
    paddingHorizontal: theme.spacing.s16,
  },
}));
