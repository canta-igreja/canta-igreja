import {Button, Text} from '@ui-kitten/components';
import React, {FC, useEffect} from 'react';
import {ScrollView, View} from 'react-native';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {UtilStyles} from 'src/arch/style/util.styles';
import {AppStack} from 'src/router/AppStackNavigator.types';
import {AppStackNavigationProps} from 'src/router/navigation/NavigationTypes';
import {LivrosAction} from 'src/store/Livros/Livros.action';
import {LivrosSelect} from 'src/store/Livros/Livros.selector';
import {useAppDispatch, useAppSelector} from 'src/store/store.hooks';
import {openLink} from '../../arch/util/linking';
import {TituloPagina} from '../base/Texto';

const LivroDetalhesScreen: FC<AppStackNavigationProps<AppStack.LivroDetalhes>> = ({route}) => {
  const {styles} = useStyles(stylesheet);

  const dispatch = useAppDispatch();

  const idLivro = route.params.idEdicaoLivro;
  const livro = useAppSelector(LivrosSelect.byId(idLivro));
  const comprarVersao = () => livro?.site && openLink(livro.site);

  const hasSite = livro?.site !== null && livro?.site !== undefined;

  useEffect(() => {
    dispatch(LivrosAction.carregarLivro(idLivro));
  }, [dispatch, idLivro]);

  return (
    <View style={styles.safeAreaView}>
      <ScrollView style={styles.scroll}>
        <TituloPagina style={styles.titulo}>{livro?.titulo ?? ''}</TituloPagina>
        <Text appearance="hint">
          {livro?.edicao ?? ''} - {livro?.ano ?? ''} - {livro?.editora ?? ''}
        </Text>
        <Text style={styles.conteudo}>{livro?.descricao}</Text>
        <Text style={styles.MensagemIncentivo}>
          Nós da Canta Igreja incentivamos você a adquirir o livro como incentivo ao trabalho realizado pela editora.
          Caso considere que {livro?.titulo ?? 'tal livro'} adequado para acompanhar seus louvores a Deus, considere
          obtê-lo.
        </Text>
        <Button style={styles.button} disabled={!hasSite} onPress={comprarVersao}>
          Mais informações
        </Button>
      </ScrollView>
    </View>
  );
};

export default LivroDetalhesScreen;

const stylesheet = createStyleSheet((theme, runtime) => ({
  safeAreaView: {
    flex: 1,
    backgroundColor: theme.color.background.level_1,
    ...UtilStyles.safeAreaViewInnerPadding(runtime, {top: false}),
  },
  scroll: {
    backgroundColor: theme.color.background.level_1,
    padding: theme.spacing.s16,
    paddingTop: 0,
  },
  titulo: {
    marginEnd: theme.spacing.s16,
  },
  conteudo: {
    marginTop: theme.spacing.s16,
  },
  MensagemIncentivo: {
    marginTop: theme.spacing.s8,
    marginBottom: theme.spacing.s16,
  },
  button: {
    marginBottom: theme.spacing.s16,
  },
}));
