import {Button} from '@ui-kitten/components';
import React, {FC, useCallback, useEffect} from 'react';
import {View} from 'react-native';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {UtilStyles} from 'src/arch/style/util.styles';
import {Repertorio} from 'src/model/repertorio/Repertorio';
import {AppStack} from 'src/router/AppStackNavigator.types';
import {AppStackNavigationProps} from 'src/router/navigation/NavigationTypes';
import {useNavStack} from 'src/router/navigation/useAppNavigation';
import {RepertoriosAction} from 'src/store/Repertorios/Repertorios.action';
import {useAppDispatch} from 'src/store/store.hooks';
import {CriarRepertorioModal} from 'src/view/fragment/Repertorios/RepertorioModal';
import {RepertoriosFragment} from 'src/view/fragment/Repertorios/RepertoriosFragment';
import {RepertoriosPadraoFragment} from 'src/view/fragment/Repertorios/RepertoriosPadraoFragment';
import {SubtituloPagina, TituloPagina} from '../base/Texto';
import {MaisIcon} from '../base/icones/Icones';
import {useModalState} from '../component/modal/useCloseModalOnBackButton';
import {HeaderScreen} from '../component/screen/HeaderScreen';
import {CarregandoRepertorioModal} from '../fragment/Repertorios/CarregandoRepertorioModal';

const RepertoriosScreen: FC<AppStackNavigationProps<AppStack.Repertorios>> = ({navigation, route}) => {
  const {styles} = useStyles(stylesheet);
  const dispatch = useAppDispatch();
  const navStack = useNavStack();

  const [isCriarRepertorioOpen, abrirModalCriarRepertorio, fecharModalCriarRepertorio] = useModalState();

  const irParaRepertorio = useCallback(
    (repertorio: Repertorio) => navStack.navigateToRepertorio(repertorio),
    [navStack],
  );
  const criarRepertorio = useCallback(
    (repertorio: Repertorio) => {
      fecharModalCriarRepertorio();
      irParaRepertorio(repertorio);
    },
    [fecharModalCriarRepertorio, irParaRepertorio],
  );

  useEffect(() => {
    dispatch(RepertoriosAction.carregar());
  }, [dispatch]);

  return (
    <View style={styles.container}>
      <CarregandoRepertorioModal navigation={navigation} route={route} />

      <HeaderScreen>
        <TituloPagina style={styles.titulo}>Repertórios</TituloPagina>
        <Button
          status="basic"
          appearance="ghost"
          accessoryLeft={MaisIcon}
          onPress={abrirModalCriarRepertorio}
          accessibilityLabel="Criar novo repertório"
        />
      </HeaderScreen>

      <RepertoriosPadraoFragment />

      <SubtituloPagina style={styles.subtitulo}>Criadas recentemente</SubtituloPagina>
      <RepertoriosFragment onRepertorioSelected={irParaRepertorio} />

      {isCriarRepertorioOpen && (
        <CriarRepertorioModal
          onCancelado={fecharModalCriarRepertorio}
          onCriado={criarRepertorio}
          isOpen={isCriarRepertorioOpen}
        />
      )}
    </View>
  );
};
export default RepertoriosScreen;

const stylesheet = createStyleSheet((theme, runtime) => ({
  container: {
    flex: 1,
    backgroundColor: theme.color.background.level_1,
    ...UtilStyles.safeAreaViewInnerPadding(runtime, {bottom: false}),
    paddingTop: runtime.orientation === 'landscape' ? runtime.insets.top : runtime.insets.top + theme.spacing.s8,
  },
  titulo: {
    paddingHorizontal: theme.spacing.s16,
  },
  subtitulo: {
    paddingHorizontal: theme.spacing.s16,
  },
}));
