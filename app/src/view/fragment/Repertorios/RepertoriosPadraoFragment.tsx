import {FC, memo, useCallback} from 'react';
import {AppStack} from 'src/router/AppStackNavigator.types';
import {useMusicasStackNavigation} from 'src/router/navigation/useAppNavigation';
import {RepertorioPadrao} from 'src/store/ItensRepertorio/ItensRepertorio.types';
import {ItemListaArredondada, ListaArredondada} from 'src/view/base/ListaArredondada';
import {FavoritadaIcon, RecentesIcon} from 'src/view/base/icones/Icones';

export const RepertoriosPadraoFragment: FC = memo(() => {
  const navigation = useMusicasStackNavigation();

  const irParaFavoritas = useCallback(
    () => navigation.navigate(AppStack.MusicasRepertorioPadrao, {repertorio: RepertorioPadrao.FAVORITOS}),
    [navigation],
  );
  const irParaRecemVisualizadas = useCallback(
    () => navigation.navigate(AppStack.MusicasRepertorioPadrao, {repertorio: RepertorioPadrao.RECEM_VISUALIZADOS}),
    [navigation],
  );

  return (
    <ListaArredondada>
      <ItemListaArredondada
        tipo="superior"
        title="Músicas favoritas"
        accessoryLeft={FavoritadaIcon}
        onPress={irParaFavoritas}
      />
      <ItemListaArredondada
        tipo="inferior"
        title="Músicas vistas recentemente"
        accessoryLeft={<RecentesIcon />}
        onPress={irParaRecemVisualizadas}
      />
    </ListaArredondada>
  );
});
