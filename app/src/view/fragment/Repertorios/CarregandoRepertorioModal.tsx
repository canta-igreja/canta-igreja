import React, {FC} from 'react';
import {View} from 'react-native';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {AppStack} from 'src/router/AppStackNavigator.types';
import {AppStackNavigationProps} from 'src/router/navigation/NavigationTypes';
import {EstadoRequisicao} from 'src/store/types';
import {CarregandoIcone} from 'src/view/base/icones/CarregandoIcone';
import {SubtituloPagina} from 'src/view/base/Texto';
import {useImportarRepertorio} from 'src/view/screen/modal/importarRepertorio/useImportarRepertorio';

export const CarregandoRepertorioModal: FC<AppStackNavigationProps<AppStack.Repertorios>> = ({route}) => {
  const {styles} = useStyles(stylesheet);

  const estadoImportacao = useImportarRepertorio(route.params?.conta, route.params?.repertorio);

  if (estadoImportacao !== EstadoRequisicao.CARREGANDO) {
    return <></>;
  }

  return (
    <View style={styles.modalContainer}>
      <View style={styles.modalContent}>
        <SubtituloPagina>Carregando repertório</SubtituloPagina>
        <View style={styles.modalLoadingContainer}>
          <CarregandoIcone />
        </View>
      </View>
    </View>
  );
};

const stylesheet = createStyleSheet((theme, runtime) => ({
  modalContainer: {
    position: 'absolute',
    justifyContent: 'center',
    backgroundColor: '#33333333',

    marginTop: runtime.insets.top,
    padding: theme.spacing.s16,
    zIndex: 10,
    width: '100%',
    height: '110%',
  },
  modalContent: {
    padding: theme.spacing.s16,
    backgroundColor: 'white',
    borderRadius: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  modalLoadingContainer: {
    marginTop: theme.spacing.s16,
    marginBottom: theme.spacing.s16,
  },
}));
