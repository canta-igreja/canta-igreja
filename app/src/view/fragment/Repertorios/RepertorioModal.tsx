import React, {FC} from 'react';
import {Repertorio} from 'src/model/repertorio/Repertorio';
import {RepertoriosAction} from 'src/store/Repertorios/Repertorios.action';
import {useAppDispatch} from 'src/store/store.hooks';
import {InputModal} from 'src/view/component/modal/InputModal';

interface CriarRepertorioModalProps {
  isOpen: boolean;

  onCriado: (repertorioCriado: Repertorio) => void;
  onCancelado: () => void;
}

export const CriarRepertorioModal: FC<CriarRepertorioModalProps> = ({isOpen, onCriado, onCancelado}) => {
  const dispatch = useAppDispatch();

  const onSubmit = async (nome: string) => {
    dispatch(RepertoriosAction.criar({nome})) //
      .then(it => onCriado(it.payload as Repertorio));
  };

  return (
    <NomeRepertorioModal
      titulo="Criar novo repertório"
      okLabel={'Criar'}
      isOpen={isOpen}
      onConfirmado={onSubmit}
      onCancelado={onCancelado}
    />
  );
};

interface Props {
  titulo: string;
  okLabel: string;

  isOpen: boolean;
  valorInicial?: string;

  onConfirmado: (nome: string) => void;
  onCancelado: () => void;
}

export const NomeRepertorioModal: FC<Props> = props => (
  <InputModal {...props} placeholder="Nome do repertório" tamanhoMaximo={50} />
);
