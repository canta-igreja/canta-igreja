import {Divider, List, Text} from '@ui-kitten/components';
import React, {FC, memo, useMemo} from 'react';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {useSelector} from 'react-redux';
import {Repertorio} from 'src/model/repertorio/Repertorio';
import {RepertoriosSelect} from 'src/store/Repertorios/Repertorios.selector';
import {KeyExtractor} from 'src/util/KeyExtractor';
import {FimDaLista} from 'src/view/component/list/item/FimDaLista';
import {buildRepertorioItemList} from 'src/view/component/list/item/RepertorioItemList';

interface Props {
  onRepertorioSelected: (repertorio: Repertorio) => void;
}

export const RepertoriosFragment: FC<Props> = memo(({onRepertorioSelected}) => {
  const {styles} = useStyles(stylesheet);

  const repertorios = useSelector(RepertoriosSelect.allIds);
  const isCarregando = useSelector(RepertoriosSelect.isCarregando);

  const ItemListaView = useMemo(() => buildRepertorioItemList(onRepertorioSelected), [onRepertorioSelected]);

  const algoCarregadoAnteriormente = repertorios.length !== 0;
  if (isCarregando && !algoCarregadoAnteriormente) {
    return <FimDaLista isCarregando={true} />;
  }

  if (repertorios.length === 0) {
    return (
      <Text style={styles.emptyStateText} appearance="hint">
        Sem repertórios criados. Crie uma por meio do botão "mais" no canto superior direito.
      </Text>
    );
  }

  return (
    <List<number>
      style={styles.list}
      data={repertorios}
      ItemSeparatorComponent={Divider}
      renderItem={ItemListaView}
      keyExtractor={KeyExtractor.fromNumber}
    />
  );
});

const stylesheet = createStyleSheet(theme => ({
  list: {
    backgroundColor: theme.color.background.level_1,
  },
  emptyStateText: {
    paddingHorizontal: theme.spacing.s16,
    paddingVertical: theme.spacing.s8,
  },
}));
