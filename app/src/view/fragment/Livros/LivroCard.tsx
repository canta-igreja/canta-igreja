import {Layout, Text} from '@ui-kitten/components';
import React, {FC} from 'react';
import {TouchableOpacity, View} from 'react-native';

import {EdicaoLivro} from 'src/model/Livro';

interface LivroCardProps {
  livro: EdicaoLivro;
  onSelectLivro: (livro: EdicaoLivro) => void;
}

export const LivroCard: FC<LivroCardProps> = ({livro, onSelectLivro}) => {
  const onPress = () => onSelectLivro(livro);

  return (
    <TouchableOpacity onPress={onPress}>
      {/* Card: */}
      <Layout level="1">
        {/* Imagem: */}
        <View />
        {/* Título: */}
        <Text category="s1">{livro.titulo.substr(0, 28)}</Text>
      </Layout>
    </TouchableOpacity>
  );
};

/*
const Card = styled(Layout)`
  border-radius: 8px;
  border-color: #e4e9f2;
  border-width: 2px;

  height: 180px;
  width: 140px;
  margin-top: 16px;
  margin-bottom: 16px;
`;

const Titulo = styled(Text)`
  padding-vertical: 2px;
  padding-horizontal: 4px;
  font-weight: bold;
  text-align: center;
  line-height: 24px;
`;

const Imagem = styled.View`
  border-top-start-radius: 8px;
  border-top-end-radius: 8px;
  height: 120px;
  background-color: #cce;
`;
*/
