import React, {FC} from 'react';
import {View} from 'react-native';
import {createStyleSheet} from 'react-native-unistyles';
import {EdicaoLivro} from 'src/model/Livro';
import {LivroCard} from './LivroCard';

interface LivrosViewProps {
  livros: EdicaoLivro[];
  onSelectLivro: (livro: EdicaoLivro) => void;
}

export const LivrosView: FC<LivrosViewProps> = ({livros, onSelectLivro}) => {
  const itens = livros.map(it => <LivroCard key={it.id_edicao_livro} livro={it} onSelectLivro={onSelectLivro} />);

  return <View style={stylesheet.container}>{itens}</View>;
};

const stylesheet = createStyleSheet({
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
});
