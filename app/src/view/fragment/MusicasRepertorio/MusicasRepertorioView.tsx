import {Divider} from '@ui-kitten/components';
import React, {FC, memo, useCallback} from 'react';
import {StyleSheet} from 'react-native';
import DraggableFlatList, {DragEndParams, RenderItemParams} from 'react-native-draggable-flatlist';
import {KeyExtractor} from 'src/util/KeyExtractor';
import {ItemRepertorioListItem, OnItemRepertorio} from 'src/view/component/list/item/ItemRepertorioItemList';
import {MusicasListaEmptyState} from '../InformationState/MusicasListaEmptyState';

type OnSwapMusicasType = (posicaoOrigem: number, posicaoDestino: number) => void;

interface Props {
  itens: number[];

  isCarregando: boolean;

  onSelectMusica: OnItemRepertorio;
  onSwapMusicas: OnSwapMusicasType;
  onItemSettings: OnItemRepertorio;
}

export const MusicasRepertorioView: FC<Props> = memo(
  ({itens, isCarregando, onSelectMusica, onItemSettings, onSwapMusicas}) => {
    const ItemView = useCallback(
      ({item, drag}: RenderItemParams<number>) => (
        <ItemRepertorioListItem
          idItemRepertorio={item}
          drag={drag}
          onSelect={onSelectMusica}
          onSettings={onItemSettings}
        />
      ),
      [onSelectMusica, onItemSettings],
    );
    const onDragEnd = useCallback(
      ({from, to}: DragEndParams<number>) => from !== to && onSwapMusicas(from, to),
      [onSwapMusicas],
    );

    // FIXME - Se for o mesmo repertório, deixar renderizar?
    if (isCarregando) {
      return <></>;
    }
    if (!isCarregando && itens.length === 0) {
      return <MusicasListaEmptyState />;
    }

    return (
      <DraggableFlatList<number>
        data={itens}
        ItemSeparatorComponent={Divider}
        renderItem={ItemView}
        keyExtractor={KeyExtractor.fromNumber}
        onDragEnd={onDragEnd}
        style={styles.draggable}
        containerStyle={styles.draggableContainer}
      />
    );
  },
);

const styles = StyleSheet.create({
  draggable: {
    flexGrow: 1,
  },
  draggableContainer: {
    flexShrink: 1,
  },
});
