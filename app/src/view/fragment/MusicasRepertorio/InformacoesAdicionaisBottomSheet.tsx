import {BottomSheetBackdrop, BottomSheetModal, BottomSheetModalProvider, BottomSheetView} from '@gorhom/bottom-sheet';
import {Divider, ListItem, StyleService, Text, TextElement, useStyleSheet} from '@ui-kitten/components';
import {RenderProp} from '@ui-kitten/components/devsupport';
import {FC, forwardRef, MutableRefObject, useCallback, useEffect, useMemo, useRef, useState} from 'react';
import {Platform, StyleSheet, TextProps} from 'react-native';
import {createStyleSheet, UnistylesRuntime, useStyles} from 'react-native-unistyles';
import {useSelector} from 'react-redux';
import {ItemRepertorio} from 'src/model/repertorio/Repertorio';
import {useNavStack} from 'src/router/navigation/useAppNavigation';
import {ItensRepertorioSelect} from 'src/store/ItensRepertorio/ItensRepertorio.selector';
import {EditarIcon, ExcluirIcon} from 'src/view/base/icones/Icones';
import {Negrito} from 'src/view/base/Texto';
import {InputModal} from 'src/view/component/modal/InputModal';
import {OkCancelarModal} from 'src/view/component/modal/OkCancelarModal';
import {useModalState} from 'src/view/component/modal/useCloseModalOnBackButton';

interface Props {
  onUpdate: (item: ItemRepertorio) => void;
  onDelete: (item: ItemRepertorio) => void;
}

export interface InformacoesAdicionaisBottomSheetRef {
  open: (idItemRepertorio: number) => void;
}

export const InformacoesAdicionaisBottomSheet = forwardRef<InformacoesAdicionaisBottomSheetRef, Props>(
  ({onDelete, onUpdate}, ref) => {
    const selfRef = ref as MutableRefObject<InformacoesAdicionaisBottomSheetRef>;
    const bottomSheetRef = useRef<BottomSheetModal>(null);

    const [idItemRepertorio, setIdItemRepertorio] = useState<number | undefined>();

    useEffect(() => {
      if (selfRef) {
        selfRef.current = {
          open: (it: number) => {
            setIdItemRepertorio(it);
            // #27 não funciona na primeira renderização
            bottomSheetRef.current?.present();
          },
        };
      }
    }, [selfRef]);

    return (
      <>
        {idItemRepertorio && (
          <BottomSheetContent
            ref={bottomSheetRef}
            idItemRepertorio={idItemRepertorio}
            onUpdate={onUpdate}
            onDelete={onDelete}
          />
        )}
      </>
    );
  },
);

interface ContentProps {
  idItemRepertorio: number;
  onUpdate: (item: ItemRepertorio) => void;
  onDelete: (item: ItemRepertorio) => void;
}
const BottomSheetContent = forwardRef<BottomSheetModal, ContentProps>(({idItemRepertorio, onUpdate, onDelete}, ref) => {
  const {styles: unistyles} = useStyles(stylesheet);

  const bottomSheetRef = ref as MutableRefObject<BottomSheetModal>;

  const navStack = useNavStack();

  const snapPoints = useMemo(() => ['20%', '50%'], []);

  const item = useSelector(ItensRepertorioSelect.byId(idItemRepertorio));

  const [confirmarExclusaoIsOpen, abrirModalConfirmarExclusao, fecharModalConfirmarExclusao] = useModalState();
  const [renomearTermoIsOpen, abrirModalRenomearTermo, fecharModalRenomearTermo] = useModalState();

  const abrirModalMomento = useCallback(() => item && navStack.navigateToMomentoModal(item), [navStack, item]);
  const abrirModalTonalidade = useCallback(() => item && navStack.navigateToTonalidadeModal(item), [navStack, item]);

  const onPressRemover = abrirModalConfirmarExclusao;
  const onTermoRenomeado = useCallback(
    (termo: string) => {
      if (item) {
        onUpdate({...item, termo});
        bottomSheetRef.current?.close();
        fecharModalRenomearTermo();
      }
    },
    [bottomSheetRef, fecharModalRenomearTermo, item, onUpdate],
  );
  const onExcluirMusica = useCallback(() => {
    if (item) {
      onDelete(item);
      bottomSheetRef.current?.close();
      fecharModalConfirmarExclusao();
    }
  }, [bottomSheetRef, fecharModalConfirmarExclusao, item, onDelete]);

  const momentoText = useMemo(() => <InformationListItem>{item?.momento.valor}</InformationListItem>, [item]);
  const tonalidadeText = useMemo(() => <InformationListItem>{item?.tonalidade.valor}</InformationListItem>, [item]);

  /**
   * #27
   * Ao renderizar a primeira vez, deve abrir o modal
   *
   * Como esse componente é renderizado somente o quando algum item é selecionado,
   * então chamar para aparecer logo após renderizar um item não funciona
   */
  useEffect(() => bottomSheetRef.current?.present(), [bottomSheetRef]);

  const bottomSheetMemoized = useMemo(
    () => (
      <BottomSheetModalProvider>
        <BottomSheetModal
          ref={bottomSheetRef}
          index={1}
          snapPoints={snapPoints}
          style={styles.shadow}
          backdropComponent={BottomSheetBackdrop}
          keyboardBehavior="fillParent">
          <BottomSheetView style={unistyles.contentContainer}>
            <Negrito>{item?.termo ?? item?.musica?.titulo}</Negrito>
            <ListItem title="Momento" onPress={abrirModalMomento} accessoryRight={momentoText} />
            <ListItem title="Tonalidade" onPress={abrirModalTonalidade} accessoryRight={tonalidadeText} />
            <Divider />

            {item?.termo && <RenomearTermoListItem onPress={abrirModalRenomearTermo} />}
            <ExcluirMusicaListItem onPress={onPressRemover} />
          </BottomSheetView>
        </BottomSheetModal>
      </BottomSheetModalProvider>
    ),
    [
      abrirModalMomento,
      abrirModalRenomearTermo,
      abrirModalTonalidade,
      bottomSheetRef,
      item?.musica?.titulo,
      item?.termo,
      momentoText,
      onPressRemover,
      snapPoints,
      tonalidadeText,
      unistyles.contentContainer,
    ],
  );
  return (
    <>
      {bottomSheetMemoized}
      {renomearTermoIsOpen && (
        <InputModal
          titulo={`Renomear ${item?.termo}`}
          okLabel="Renomear"
          placeholder="Novo nome"
          valorInicial={item?.termo}
          isOpen={renomearTermoIsOpen}
          onConfirmado={onTermoRenomeado}
          onCancelado={fecharModalRenomearTermo}
          tamanhoMaximo={300}
        />
      )}
      {confirmarExclusaoIsOpen && (
        <OkCancelarModal
          okLabel="Remover"
          isOpen={confirmarExclusaoIsOpen}
          titulo="Remover música"
          onOk={onExcluirMusica}
          onCancelado={fecharModalConfirmarExclusao}>
          <Text style={unistyles.textModal} category="s2">
            Você deseja realmente remover "{item?.termo ?? item?.musica?.titulo ?? ''}"?
          </Text>
        </OkCancelarModal>
      )}
    </>
  );
});

const InformationListItem: FC<{children: undefined | string | number | TextElement}> = ({children}) => {
  const {styles} = useStyles(stylesheet);

  return (
    <Text style={styles.informationListItemText} category="s2" appearance="hint">
      {children}
    </Text>
  );
};

const ExcluirMusicaIcon = () => {
  const styles = useStyleSheet(themedStyles);

  return (
    <>
      <ExcluirIcon style={styles.buttonDanger} />
    </>
  );
};

const RenomearTermoListItem: FC<{onPress: () => void}> = ({onPress}) => {
  return <ListItem title="Renomear música" accessoryRight={EditarIcon} onPress={onPress} />;
};
const ExcluirMusicaListItem: FC<{onPress: () => void}> = ({onPress}) => {
  return <ListItem title={ExcluirListItemText} accessoryRight={ExcluirMusicaIcon} onPress={onPress} />;
};

const ExcluirListItemText: RenderProp<TextProps> = props => {
  const styles = useStyleSheet(themedStyles);

  const listItemStyles = [...(props?.style ?? ({} as any)), styles.listItemTitle];
  return (
    <Text {...props} style={listItemStyles}>
      Remover música
    </Text>
  );
};

const styles = StyleSheet.create({
  shadow: Platform.select({
    ios: {
      shadowOffset: {
        width: 0,
        height: 12,
      },
      shadowOpacity: 0.75,
      shadowRadius: 16.0,
      shadowColor: '#000',
    },
    android: {
      borderRadius: 16,
      elevation: 24,
    },
    web: {
      boxShadow: '0px -4px 16px rgba(0,0,0, 0.25)',
    },
  }) as any,
});

const themedStyles = StyleService.create({
  listItemTitle: {
    color: 'color-danger-default',
  },
  buttonDanger: {
    width: 20,
    height: 20,
    marginHorizontal: 10,
    tintColor: 'color-danger-default',
  },
});

const stylesheet = createStyleSheet(theme => ({
  contentContainer: {
    paddingLeft: UnistylesRuntime.insets.left === 0 ? theme.spacing.s16 : UnistylesRuntime.insets.left,
    paddingRight: UnistylesRuntime.insets.right === 0 ? theme.spacing.s16 : UnistylesRuntime.insets.right,
    marginBottom: 20,
  },
  textModal: {
    marginHorizontal: theme.spacing.s16,
  },
  informationListItemText: {
    marginRight: 12,
  },
}));
