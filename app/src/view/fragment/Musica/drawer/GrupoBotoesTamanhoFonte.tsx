import React, {FC, memo, useCallback} from 'react';
import {useSelector} from 'react-redux';
import {ConfiguracoesAction} from 'src/store/ui/Configuracoes/Configuracoes.action';
import {ConfiguracoesSelect} from 'src/store/ui/Configuracoes/Configuracoes.selector';
import {TamanhoFonte} from 'src/store/ui/Configuracoes/Configuracoes.types';
import {GrupoBotoes, ItemGrupoBotoes} from '../../../base/GrupoBotoes';
import {useAppDispatch} from 'src/store/store.hooks';

const itens: ItemGrupoBotoes<TamanhoFonte>[] = [
  {nome: 'Normal', valor: TamanhoFonte.NORMAL},
  {nome: 'Moderado', valor: TamanhoFonte.MODERADO},
  {nome: 'Grande', valor: TamanhoFonte.GRANDE},
];

export const GrupoBotoesTamanhoFonte: FC = memo(() => {
  const dispatch = useAppDispatch();

  const valor = useSelector(ConfiguracoesSelect.musica.tamanhoFonte);
  const setValor = useCallback((it: TamanhoFonte) => dispatch(ConfiguracoesAction.alterarTamanhoFonte(it)), [dispatch]);

  return <GrupoBotoes<TamanhoFonte> itens={itens} atual={valor} onChange={setValor} />;
});
