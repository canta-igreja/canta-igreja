import {Divider, DrawerItem, Text} from '@ui-kitten/components';
import React, {FC, useCallback} from 'react';
import {ScrollView, View} from 'react-native';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {useSelector} from 'react-redux';
import {AppStack} from 'src/router/AppStackNavigator.types';
import {useMusicasStackNavigation, useStackRoute} from 'src/router/navigation/useAppNavigation';
import {MusicaService} from 'src/service/MusicaService';
import {AlertaIcon, CompartilharIcon, IncluirEmRepertorioIcon, LivroIcon} from 'src/view/base/icones/Icones';
import {useModalState} from 'src/view/component/modal/useCloseModalOnBackButton';
import {LivrosComMusicaModal} from '../LivrosComMusicaModal';
import {GrupoBotoesEstiloRefrao} from './GrupoBotoesEstiloRefrao';
import {GrupoBotoesFamiliaFonte} from './GrupoBotoesFamiliaFonte';
import {GrupoBotoesTamanhoFonte} from './GrupoBotoesTamanhoFonte';
import {MusicasSelect} from 'src/store/Musicas/Musicas.selector';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {CompartilharService} from 'src/service/CompartilharService';

const ConfiguracoesMusicasDrawer: FC = () => {
  const {styles} = useStyles(stylesheet);
  const insets = useSafeAreaInsets();

  const navigation = useMusicasStackNavigation();
  const route = useStackRoute<AppStack.Musica>();

  const idMusica = route.params.params.idMusica;

  const [exibirLivrosMusicaModal, abrirModal, fecharModal] = useModalState();

  const musica = useSelector(MusicasSelect.byId(idMusica));

  const openFormulario = useCallback(async () => musica && (await MusicaService.relatarErro(musica)), [musica]);
  const compartilharLetra = useCallback(
    async () => musica && (await new CompartilharService().musica(musica)),
    [musica],
  );
  const adicionarEmRepertorio = useCallback(
    () => navigation.navigate(AppStack.IncluirMusicaEmRepertorio, {idMusica}),
    [idMusica, navigation],
  );

  return (
    <ScrollView>
      <View style={[styles.opcoesGerais, {paddingRight: insets.right}]}>
        <Text category="h5">Configurações</Text>

        <Text style={styles.opcao} category="p2">
          Tamanho da fonte
        </Text>
        <GrupoBotoesTamanhoFonte />

        <Text style={styles.opcao} category="p2">
          Refrão
        </Text>
        <GrupoBotoesEstiloRefrao />

        <Text style={styles.opcao} category="p2">
          Fonte
        </Text>
        <GrupoBotoesFamiliaFonte />
      </View>

      <View style={styles.outrasOpcoes}>
        <Divider />
        <DrawerItem
          title="Adicionar a um repertório"
          accessoryLeft={IncluirEmRepertorioIcon}
          onPress={adicionarEmRepertorio}
        />

        <Divider />
        <DrawerItem title="Livros com esta música" accessoryLeft={LivroIcon} onPress={abrirModal} />
        <DrawerItem title="Compartilhar" accessoryLeft={CompartilharIcon} onPress={compartilharLetra} />
        <Divider />
        <DrawerItem title="Relatar erro" accessoryLeft={AlertaIcon} onPress={openFormulario} />
      </View>

      {musica && <LivrosComMusicaModal musica={musica} isOpen={exibirLivrosMusicaModal} fecharModal={fecharModal} />}
    </ScrollView>
  );
};
export default ConfiguracoesMusicasDrawer;

const stylesheet = createStyleSheet(theme => ({
  opcoesGerais: {
    paddingHorizontal: theme.spacing.s16,
    paddingVertical: theme.spacing.s4,
  },
  opcao: {
    paddingTop: theme.spacing.s16,
    paddingBottom: theme.spacing.s8,
  },
  outrasOpcoes: {
    paddingTop: theme.spacing.s16,
  },
}));
