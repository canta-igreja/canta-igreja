import React, {FC, memo, useCallback} from 'react';
import {useSelector} from 'react-redux';
import {ConfiguracoesAction} from 'src/store/ui/Configuracoes/Configuracoes.action';
import {ConfiguracoesSelect} from 'src/store/ui/Configuracoes/Configuracoes.selector';
import {EstiloRefrao} from 'src/store/ui/Configuracoes/Configuracoes.types';
import {GrupoBotoes, ItemGrupoBotoes} from '../../../base/GrupoBotoes';
import {useAppDispatch} from 'src/store/store.hooks';

const itens: ItemGrupoBotoes<EstiloRefrao>[] = [
  {nome: 'Negrito', valor: EstiloRefrao.NEGRITO},
  {nome: 'Itálico', valor: EstiloRefrao.ITALICO},
  {nome: 'CAIXA ALTA', valor: EstiloRefrao.CAIXA_ALTA},
];

export const GrupoBotoesEstiloRefrao: FC = memo(() => {
  const dispatch = useAppDispatch();

  const valor = useSelector(ConfiguracoesSelect.musica.estiloRefrao);
  const setValor = useCallback((it: EstiloRefrao) => dispatch(ConfiguracoesAction.alterarEstiloRefrao(it)), [dispatch]);

  return <GrupoBotoes<EstiloRefrao> itens={itens} atual={valor} onChange={setValor} />;
});
