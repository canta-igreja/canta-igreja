import React, {FC, memo, useCallback} from 'react';
import {useSelector} from 'react-redux';
import {ConfiguracoesAction} from 'src/store/ui/Configuracoes/Configuracoes.action';
import {ConfiguracoesSelect} from 'src/store/ui/Configuracoes/Configuracoes.selector';
import {FonteMusica} from 'src/store/ui/Configuracoes/Configuracoes.types';
import {GrupoBotoes, ItemGrupoBotoes} from '../../../base/GrupoBotoes';
import {useAppDispatch} from 'src/store/store.hooks';

const itens: ItemGrupoBotoes<FonteMusica>[] = [
  {nome: 'Padrão', valor: FonteMusica.PADRAO},
  {nome: 'Monoespaçada', valor: FonteMusica.MONOESPACADA},
];

export const GrupoBotoesFamiliaFonte: FC = memo(() => {
  const dispatch = useAppDispatch();

  const valor = useSelector(ConfiguracoesSelect.musica.fonteMusica);
  const setValor = useCallback((it: FonteMusica) => dispatch(ConfiguracoesAction.alterarFamiliaFonte(it)), [dispatch]);

  return <GrupoBotoes<FonteMusica> itens={itens} atual={valor} onChange={setValor} />;
});
