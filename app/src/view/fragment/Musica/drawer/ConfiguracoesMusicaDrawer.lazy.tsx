import React, {FC, Suspense, lazy} from 'react';
import {CarregandoIcone} from 'src/view/base/icones/CarregandoIcone';
import {promiseDelay} from 'src/arch/util/delay';

const ConfiguracoesMusicasDrawer = lazy(() => promiseDelay(import('./ConfiguracoesMusicaDrawer'), 2000));

export const ConfiguracoesMusicaDrawerLazy: FC = (props: any) => (
  <Suspense fallback={<CarregandoIcone />}>
    <ConfiguracoesMusicasDrawer {...props} />
  </Suspense>
);
