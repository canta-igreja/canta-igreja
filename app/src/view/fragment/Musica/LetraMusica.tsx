import React, {FC, memo, useMemo} from 'react';
import {Platform, StyleSheet} from 'react-native';

import {useSelector} from 'react-redux';
import {useTheme} from '@ui-kitten/components';
import Markdown from 'react-native-markdown-display';

import {ConfiguracoesSelect} from 'src/store/ui/Configuracoes/Configuracoes.selector';
import {EstiloRefrao, FonteMusica, TamanhoFonte} from 'src/store/ui/Configuracoes/Configuracoes.types';
import {MarkdownItConfiguration} from 'src/util/markdownit';

interface LetraMusicaProps {
  children: string;
}

export const LetraMusica: FC<LetraMusicaProps> = memo(({children}) => {
  const theme = useTheme();

  const configuracoes = MarkdownItConfiguration.lyrics;

  const estiloRefrao = useEstiloRefrao();
  const tamanhoFonte = useTamanhoFonte();
  const familiaFonte = useFamiliaFonte();

  const estilos = {
    ...styles,
    body: {
      color: theme['text-basic-color'],
      ...tamanhoFonte,
      ...familiaFonte,
    },
    em: {
      ...styles.em,
      ...estiloRefrao,
    },
    strong: {
      ...styles.em,
      ...estiloRefrao,
    },
    list_item: {
      marginBottom: 4,
    },
  };

  if (children === undefined || children === null || children.trim() === '') {
    children = 'Nenhuma letra cadastrada para esta música até o momento';
  }

  return (
    <Markdown style={estilos as any} markdownit={configuracoes}>
      {children}
    </Markdown>
  );
});

const useEstiloRefrao = () => {
  const estiloRefrao = useSelector(ConfiguracoesSelect.musica.estiloRefrao);

  return useMemo(() => {
    if (estiloRefrao === EstiloRefrao.NEGRITO) {
      return {fontWeight: 'bold'};
    } else if (estiloRefrao === EstiloRefrao.ITALICO) {
      return {fontStyle: 'italic'};
    } else {
      return {textTransform: 'uppercase'};
    }
  }, [estiloRefrao]);
};

export const useTamanhoFonte = () => {
  const tamanhoFonte = useSelector(ConfiguracoesSelect.musica.tamanhoFonte);

  return useMemo(() => {
    if (tamanhoFonte === TamanhoFonte.NORMAL) {
      return {fontSize: 14};
    } else if (tamanhoFonte === TamanhoFonte.MODERADO) {
      return {fontSize: 18};
    } else {
      return {fontSize: 22};
    }
  }, [tamanhoFonte]);
};

const useFamiliaFonte = () => {
  const familiaFonte = useSelector(ConfiguracoesSelect.musica.fonteMusica);

  return useMemo(() => {
    let fontFamily: string | undefined = '';
    if (Platform.OS === 'ios') {
      fontFamily = familiaFonte === FonteMusica.PADRAO ? undefined : 'Courier';
    } else {
      fontFamily = familiaFonte === FonteMusica.PADRAO ? 'inherit' : 'monospace';
    }

    return {fontFamily};
  }, [familiaFonte]);
};

const styles = StyleSheet.create({
  em: {
    fontStyle: 'normal',
    fontWeight: 'normal',
  },
});
