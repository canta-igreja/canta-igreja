import React, {FC} from 'react';
import {InformacoesDetalhadasMusica} from 'src/model/Musica';
import {Pilulas} from 'src/view/base/Pilula';

interface Props {
  musica: InformacoesDetalhadasMusica;
}

export const PilulasMusica: FC<Props> = ({musica}) => {
  const pilulas = musica.rotulos.map(it => ({key: it.id_rotulo_musica, texto: it.nome}));

  return <Pilulas conteudo={pilulas} />;
};
