import {Button} from '@ui-kitten/components';
import React, {FC, useCallback, useMemo, useState} from 'react';
import Animated from 'react-native-reanimated';
import {useSelector} from 'react-redux';
import {Animation} from 'src/arch/util/Animation';
import {MusicasSelect} from 'src/store/Musicas/Musicas.selector';
import {useAppDispatch} from 'src/store/store.hooks';
import {FavoritadaIcon, NaoFavoritadaIcon} from 'src/view/base/icones/Icones';
import {MusicaService} from '../../../service/MusicaService';
import {MusicasAction} from 'src/store/Musicas/Musicas.action';

interface Props {
  idMusica: number;
}

export const FavoritarMusicaAtualBotao: FC<Props> = ({idMusica}) => {
  const dispatch = useAppDispatch();

  const [favorita, setFavorita] = useState<boolean | undefined>(MusicaService.isFavorita(idMusica));

  const musica = useSelector(MusicasSelect.byId(idMusica));
  const musicaCarregada = musica !== undefined;

  const onPress = useCallback(() => {
    setFavorita(valorAtual => {
      const novoValor = !valorAtual;
      dispatch(MusicasAction.setFavorita({idMusica, favorita: novoValor}));
      return novoValor;
    });
  }, [dispatch, idMusica]);

  const icone = useMemo(() => (favorita ? <FavoritadaIcon /> : <NaoFavoritadaIcon />), [favorita]);

  return musicaCarregada ? (
    <Animated.View entering={Animation.FadeIn}>
      <Button status="basic" appearance="ghost" accessoryLeft={icone} onPress={onPress} />
    </Animated.View>
  ) : (
    <></>
  );
};
