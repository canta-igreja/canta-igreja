import React, {FC} from 'react';
import {Negrito} from '../../base/Texto';
import {TextElement} from '@ui-kitten/components';

interface TituloMusicaProps {
  children: string | number | TextElement;
}

export const TituloMusica: FC<TituloMusicaProps> = ({children}) => <Negrito category="h4">{children}</Negrito>;
