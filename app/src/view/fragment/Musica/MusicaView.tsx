import KeepAwake from '@sayem314/react-native-keep-awake';
import {Text} from '@ui-kitten/components';
import React, {FC, useCallback, useEffect} from 'react';
import {TouchableOpacity, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {useSelector} from 'react-redux';
import {Animation} from 'src/arch/util/Animation';
import {delay} from 'src/arch/util/delay';
import {TipoComposicao} from 'src/model/Composicao';
import {InformacoesDetalhadasMusica} from 'src/model/Musica';
import {ItemRepertorio} from 'src/model/repertorio/Repertorio';
import {useNavStack} from 'src/router/navigation/useAppNavigation';
import {MusicasAction} from 'src/store/Musicas/Musicas.action';
import {MusicasSelect} from 'src/store/Musicas/Musicas.selector';
import {ItensRepertorioSelect} from 'src/store/ItensRepertorio/ItensRepertorio.selector';
import {useAppDispatch} from 'src/store/store.hooks';
import {ConfiguracoesAction} from 'src/store/ui/Configuracoes/Configuracoes.action';
import {ItemHeader} from 'src/view/base/Texto';
import {Compositores} from 'src/view/fragment/Musica/Compositores';
import {LetraMusica} from 'src/view/fragment/Musica/LetraMusica';
import {Midias} from 'src/view/fragment/Musica/Midias';
import {PilulasMusica} from 'src/view/fragment/Musica/PilulasMusica';
import {TituloMusica} from 'src/view/fragment/Musica/TituloMusica';

interface Props {
  idMusica: number;
  titulo?: string;

  idItemRepertorio?: number;
  marcarComoVisualizada?: boolean;
}

export const MusicaView: FC<Props> = ({titulo, idMusica, idItemRepertorio, marcarComoVisualizada = false}) => {
  const {styles} = useStyles(stylesheet);
  const dispatch = useAppDispatch();

  const musica = useSelector(MusicasSelect.byId(idMusica));
  const itemRepertorio = useSelector(ItensRepertorioSelect.byId(idItemRepertorio ?? 0));

  const apresentarLetra = musica !== undefined;
  const hasCompositor =
    musica?.contribuidores?.compositores || musica?.contribuidores?.letristas || musica?.contribuidores?.musicistas;

  useEffect(() => {
    delay(() => {
      dispatch(ConfiguracoesAction.carregarConfiguracoes());
      dispatch(MusicasAction.carregarMusica(idMusica));
    }, 10);
  }, [dispatch, idMusica]);

  useMarcarMusicaAsVisualizada(marcarComoVisualizada, musica);

  return (
    <>
      <KeepAwake />

      {itemRepertorio && <MomentoMusicaView item={itemRepertorio} />}
      <TituloMusica>{titulo ?? musica?.titulo ?? ''}</TituloMusica>
      {itemRepertorio && <TonsMusicaView item={itemRepertorio} />}

      {apresentarLetra && (
        <Animated.View entering={Animation.FadeIn}>
          {!hasCompositor && (
            <Compositores formaContribuicao={TipoComposicao.Composicao} contribuidores={['Desconhecido']} />
          )}
          <Compositores
            formaContribuicao={TipoComposicao.Composicao}
            contribuidores={musica?.contribuidores?.compositores}
          />
          <Compositores formaContribuicao={TipoComposicao.Letra} contribuidores={musica?.contribuidores?.letristas} />
          <Compositores formaContribuicao={TipoComposicao.Musica} contribuidores={musica?.contribuidores?.musicistas} />

          <View style={styles.menuAcoes}>
            {musica && <PilulasMusica musica={musica} />}
            {musica && <Midias musica={musica} />}
          </View>

          <LetraMusica>{musica?.letra ?? ''}</LetraMusica>
        </Animated.View>
      )}
    </>
  );
};

interface TonsMusicaViewProps {
  item: ItemRepertorio;
}

const MomentoMusicaView: FC<TonsMusicaViewProps> = ({item}) => {
  const {styles} = useStyles(stylesheet);
  const navStack = useNavStack();

  const hasMomentoDefinido = item.momento.valor !== undefined;
  const estilo = hasMomentoDefinido ? styles.momento : styles.momentoNaoDefinido;
  const texto = hasMomentoDefinido ? item.momento.valor : 'Momento não definido';

  const abrirModalMomento = useCallback(() => item && navStack.navigateToMomentoModal(item), [navStack, item]);

  return (
    <TouchableOpacity onPress={abrirModalMomento}>
      <ItemHeader style={estilo}>{texto}</ItemHeader>
    </TouchableOpacity>
  );
};

const TonsMusicaView: FC<TonsMusicaViewProps> = ({item}) => {
  const navStack = useNavStack();

  const hasTomDefinido = item.tonalidade.valor !== undefined;
  const hasSugestoes = item.tonalidade.sugestoes.length > 0;

  const abrirModalTonalidade = useCallback(() => item && navStack.navigateToTonalidadeModal(item), [navStack, item]);

  return (
    <TouchableOpacity onPress={abrirModalTonalidade}>
      <>
        {hasTomDefinido && (
          <Text appearance="hint" category="p1">
            Tom: <Text appearance="default">{item.tonalidade.valor}</Text>
          </Text>
        )}
        {!hasTomDefinido && hasSugestoes && (
          <Text appearance="hint" category="p1">
            Tons sugeridos: <Text appearance="default">{item.tonalidade.sugestoes.join(', ')}</Text>
          </Text>
        )}
        {!hasTomDefinido && !hasSugestoes && (
          <Text appearance="hint" category="p1">
            Sem tom definido
          </Text>
        )}
      </>
    </TouchableOpacity>
  );
};

const useMarcarMusicaAsVisualizada = (
  marcarComoVisualizada: boolean,
  musica: InformacoesDetalhadasMusica | undefined,
) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (musica && marcarComoVisualizada) {
      dispatch(MusicasAction.marcarMusicaComoVisualizada(musica));
    }
  }, [dispatch, marcarComoVisualizada, musica]);
};

const stylesheet = createStyleSheet(theme => ({
  momento: {
    color: '#60a5fa',
    textTransform: 'uppercase',
  },
  momentoNaoDefinido: {
    textTransform: 'uppercase',
  },
  menuAcoes: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: theme.spacing.s8,
  },
  repertorio: {
    flexDirection: 'row-reverse',
  },
}));
