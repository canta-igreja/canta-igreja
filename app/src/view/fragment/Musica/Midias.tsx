import {Button} from '@ui-kitten/components';
import React, {FC} from 'react';
import {View} from 'react-native';
import {createStyleSheet} from 'react-native-unistyles';
import {openLink} from 'src/arch/util/linking';
import {InformacoesDetalhadasMusica} from 'src/model/Musica';
import {CifraClubIcon, PartituraIcon, YoutubeIcon} from '../../base/icones/Icones';

interface MidiasProps {
  musica: InformacoesDetalhadasMusica;
}

export const Midias: FC<MidiasProps> = ({musica}) => {
  const partitura = musica.midias.Partitura;
  const youtube = musica.midias.YouTube;
  const cifraclub = musica.midias.CifraClub;

  const openYoutube = () => youtube && openLink(`https://youtu.be/${youtube}`);
  const openCifraClub = () => cifraclub && openLink(cifraclub);
  const openPartitura = () => partitura && openLink(partitura);

  return (
    <View style={stylesheet.container}>
      {youtube && (
        <Button
          accessibilityHint="Escutar música no Youtube"
          onPress={openYoutube}
          appearance="ghost"
          accessoryLeft={YoutubeIcon}
        />
      )}
      {cifraclub && (
        <Button
          accessibilityHint="Acessar cifra no CifraClub"
          onPress={openCifraClub}
          appearance="ghost"
          accessoryLeft={CifraClubIcon}
        />
      )}
      {partitura && (
        <Button
          accessibilityHint="Acessar partitura (pdf)"
          onPress={openPartitura}
          appearance="ghost"
          accessoryLeft={PartituraIcon}
        />
      )}
    </View>
  );
};

const stylesheet = createStyleSheet({
  container: {
    flexDirection: 'row',
  },
});
