import {Text} from '@ui-kitten/components';
import React, {FC} from 'react';
import {View} from 'react-native';
import {createStyleSheet} from 'react-native-unistyles';
import {TipoComposicao} from 'src/model/Composicao';

interface CompositoresProps {
  formaContribuicao: TipoComposicao;
  contribuidores: string[] | undefined;
}

export const Compositores: FC<CompositoresProps> = ({formaContribuicao, contribuidores = []}) => {
  const formasContribuicao = {
    [TipoComposicao.Composicao]: 'Composição:',
    [TipoComposicao.Letra]: 'Letra:',
    [TipoComposicao.Musica]: 'Música:',
  };
  const forma = formasContribuicao[formaContribuicao];

  if (contribuidores.length === 0) {
    return <></>;
  }

  const nomes = contribuidores.join(', ');

  return (
    <View style={stylesheet.container}>
      <Text appearance="hint" category="p1">{`${forma} ${nomes}`}</Text>
    </View>
  );
};

const stylesheet = createStyleSheet({
  container: {
    flexDirection: 'row',
  },
});
