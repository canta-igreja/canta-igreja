import {List, ListItem} from '@ui-kitten/components';
import React, {FC, useCallback} from 'react';
import {ListRenderItem} from 'react-native';
import {IndiceMusica} from 'src/model/IndiceMusica';
import {InformacoesDetalhadasMusica} from 'src/model/Musica';
import {useNavStack} from 'src/router/navigation/useAppNavigation';
import {OkModal} from '../../component/modal/OkModal';
import {useCloseModalOnBackButton} from '../../component/modal/useCloseModalOnBackButton';

interface LivrosComMusicaModalProps {
  musica: InformacoesDetalhadasMusica;
  isOpen: boolean;
  fecharModal: () => void;
}

export const LivrosComMusicaModal: FC<LivrosComMusicaModalProps> = ({isOpen, musica, fecharModal}) => {
  const closeModal = useCloseModalOnBackButton(isOpen, fecharModal);
  const navStack = useNavStack();

  const irParaLivro = useCallback(
    (indiceMusica: IndiceMusica) => {
      navStack.navigateDetalhesEdicaoLivro(indiceMusica);
      closeModal();
    },
    [closeModal, navStack],
  );

  const renderItem: ListRenderItem<IndiceMusica> = ({item}) => (
    <ListItem
      title={`${item.indice} - ${item.titulo_livro}`}
      description={`${item.edicao} - ${item.editora}`}
      onPress={() => irParaLivro(item)}
    />
  );

  const hasLivros = musica.indices.length > 0;

  return (
    <OkModal isOpen={isOpen} titulo="Livros com esta música" fecharModal={closeModal}>
      <>
        {!hasLivros && <ListItem disabled title="Nenhum livro no catálogo para essa música" />}

        <List<IndiceMusica>
          data={musica.indices}
          keyExtractor={(it: IndiceMusica) => `${it.id_musica}_${it.id_edicao_livro}`}
          renderItem={renderItem}
        />
      </>
    </OkModal>
  );
};
