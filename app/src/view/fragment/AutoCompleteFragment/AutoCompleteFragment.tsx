import React, {FC, useMemo} from 'react';
import {AutoCompleteContent} from './AutoCompleteContent';
import {AutoCompleteOption} from './AutoCompleteFragment.type';
import {HeaderScreen} from 'src/view/component/screen/HeaderScreen';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {View} from 'react-native';
import {SubtituloPagina} from 'src/view/base/Texto';

interface Props {
  titulo?: string;
  label: string;
  placeholder: string;
  opcoes: string[];
  sugestoes: string[];
  valorInicial: string | undefined;
  onOk: (valor: string) => void;
}

export const AutoCompleteFragment: FC<Props> = ({
  titulo,
  label,
  placeholder,
  opcoes,
  sugestoes,
  valorInicial,
  onOk,
}) => {
  const {styles} = useStyles(stylesheet);
  const options: AutoCompleteOption[] = useMemo(() => {
    const recomendacoesSet = new Set(sugestoes);

    return [
      ...sugestoes.map(it => ({title: it, description: 'Sugerido com base nas últimas utilizações'})),
      ...opcoes.filter(it => !recomendacoesSet.has(it)).map(it => ({title: it})),
    ];
  }, [opcoes, sugestoes]);

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <HeaderScreen>{titulo && <SubtituloPagina style={styles.titulo}>{titulo}</SubtituloPagina>}</HeaderScreen>
      </View>

      <AutoCompleteContent
        valorInicial={valorInicial}
        label={label}
        placeholder={placeholder}
        opcoes={options}
        onOk={onOk}
      />
    </View>
  );
};

const stylesheet = createStyleSheet(theme => ({
  container: {
    flex: 1,

    backgroundColor: theme.color.background.level_1,
  },
  header: {
    paddingHorizontal: theme.spacing.s16,
  },
  titulo: {
    paddingBottom: theme.spacing.s8,
  },
}));
