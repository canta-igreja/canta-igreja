import {Divider, Input, ListItem, Button} from '@ui-kitten/components';
import React, {FC, useCallback, useEffect, useRef} from 'react';
import {FlatList, View} from 'react-native';
import {KeyExtractor} from 'src/util/KeyExtractor';
import {delay} from 'src/arch/util/delay';
import {useAutoCompleteReducer} from './AutoCompleteFragment.reducer';
import {AutoCompleteOption} from './AutoCompleteFragment.type';
import {createStyleSheet, useStyles} from 'react-native-unistyles';

interface Props {
  label: string;
  placeholder: string;

  valorInicial?: string;
  opcoes: AutoCompleteOption[];

  onOk: (query: string) => void;
}

/**
 * Separado para evidar renderizações desnecessárias no Modal, que é custos
 */
export const AutoCompleteContent: FC<Props> = ({label, placeholder, valorInicial, opcoes, onOk}) => {
  const {styles} = useStyles(stylesheet);
  const inputRef = useRef<Input>();

  const {content, actions} = useAutoCompleteReducer(opcoes, valorInicial);

  const onOptionSelect = useCallback(
    (query: string) => {
      inputRef?.current?.focus();
      actions.onChangeText(query);
    },
    [inputRef, actions],
  );

  const onPress = useCallback(() => {
    // Permitir valores em branco de modo a apagar um valor colocado sem querer
    onOk(inputRef.current?.props.value ?? '');
  }, [inputRef, onOk]);

  useEffect(() => {
    inputRef?.current && delay(() => inputRef.current?.focus());
  }, [inputRef]);

  const ItemView = useCallback(
    ({item}: {item: AutoCompleteOption}) => <ListItemPressable item={item} onPress={onOptionSelect} />,
    [onOptionSelect],
  );

  return (
    <>
      <View style={styles.head}>
        <Input
          style={styles.input}
          ref={inputRef as any}
          label={label}
          placeholder={placeholder}
          value={content.query}
          onChangeText={actions.onChangeText}
          showSoftInputOnFocus={true}
        />
        <View style={styles.buttonContainer}>
          <Button status="basic" size="medium" appearance="filled" onPress={onPress}>
            OK
          </Button>
        </View>
      </View>

      <FlatList<AutoCompleteOption>
        keyboardShouldPersistTaps="always"
        data={content.items}
        ItemSeparatorComponent={Divider}
        renderItem={ItemView}
        keyExtractor={KeyExtractor.fromAutoCompleteOption}
      />
    </>
  );
};

interface ItemViewProps {
  item: AutoCompleteOption;
  onPress: (content: string) => void;
}

const ListItemPressable: FC<ItemViewProps> = ({item, onPress}) => {
  const onItemPress = useCallback(() => onPress(item.title), [onPress, item.title]);

  return <ListItem title={item.title} description={item.description} onPress={onItemPress} />;
};

const stylesheet = createStyleSheet(theme => ({
  head: {
    flexDirection: 'row',

    paddingHorizontal: theme.spacing.s16,
  },
  buttonContainer: {
    flex: 0.25,
    paddingTop: 18,
    marginLeft: 8,
  },
  input: {
    flex: 0.75,
  },
}));
