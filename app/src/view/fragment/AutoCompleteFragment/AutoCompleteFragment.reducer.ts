import {useMemo, useReducer} from 'react';
import {DetalhesMusicaUtil} from 'src/util/DetalhesMusicaUtil';
import {AutoCompleteOption} from './AutoCompleteFragment.type';

interface State {
  initialQuery: string | undefined;
  query: string | undefined;
  items: AutoCompleteOption[];
  originalOptions: AutoCompleteOption[];
}

type Action =
  | {type: 'initialize'; query: string} //
  | {type: 'onChangeText'; query: string}
  | {type: 'clearState'};

interface AutoCompleteActions {
  initialize: (query: string) => void;
  onChangeText: (query: string) => void;
  clearState: () => void;
}

export const useAutoCompleteReducer = (options: AutoCompleteOption[], valorInicial?: string) => {
  const state: State = {
    initialQuery: valorInicial,
    query: valorInicial,
    items: options,
    originalOptions: options,
  };
  const [content, dispatch] = useReducer(reducer, state);

  const actions: AutoCompleteActions = useMemo(
    () => ({
      initialize: (query: string) => dispatch({type: 'initialize', query}),
      onChangeText: (query: string) => dispatch({type: 'onChangeText', query}),
      clearState: () => dispatch({type: 'clearState'}),
    }),
    [],
  );

  // https://react.dev/learn/you-might-not-need-an-effect#adjusting-some-state-when-a-prop-changes
  if (valorInicial && valorInicial !== content.initialQuery) {
    actions.initialize(valorInicial);
  }

  return {content, actions};
};

const reducer = (state: State, action: Action) => {
  switch (action.type) {
    case 'onChangeText': {
      const items = DetalhesMusicaUtil.filterLabelled(action.query, state.originalOptions);

      return {
        ...state,
        query: action.query,
        items:
          items.length > 0 //
            ? items
            : [{title: action.query}],
      };
    }
    case 'initialize': {
      return {
        ...state,
        initialQuery: action.query,
        query: action.query,
        items: state.originalOptions,
      };
    }
    case 'clearState': {
      return {
        ...state,
        query: undefined,
        items: state.originalOptions,
      };
    }
  }
};
