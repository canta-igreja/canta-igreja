import {FC, useCallback} from 'react';
import {GenericState} from './GenericState';
import {Button} from '@ui-kitten/components';
import {createStyleSheet, useStyles} from 'react-native-unistyles';

interface Props {
  image?: (props: any) => React.JSX.Element;
  header?: string;
  description: string;

  buttonText: string;
  onAction: () => void;
}

export const ErrorState: FC<Props> = ({buttonText, onAction, ...props}) => {
  const {styles} = useStyles(stylesheet);

  const button = useCallback(
    () => (
      <Button style={styles.button} size="small" status="basic" onPress={onAction}>
        {buttonText}
      </Button>
    ),
    [buttonText, onAction, styles.button],
  );

  return <GenericState {...props} Action={button} />;
};

const stylesheet = createStyleSheet(theme => ({
  button: {
    marginTop: theme.spacing.s8,
  },
}));
