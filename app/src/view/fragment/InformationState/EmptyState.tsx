import { FC } from "react";
import { GenericState } from "./GenericState";

interface Props {
  image: (props: any) => React.JSX.Element,
  header: string,
  description: string
}

export const EmptyState: FC<Props> = (props) => {
  return <GenericState
    {...props}
  />
}
