import {FC, ReactElement} from 'react';
import {Text} from '@ui-kitten/components';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {View} from 'react-native';

interface Props {
  image?: (props: any) => React.JSX.Element;
  header?: string;
  description?: string;
  Action?: () => ReactElement;
}

export const GenericState: FC<Props> = ({image: Image, header, description, Action}) => {
  const {styles} = useStyles(stylesheet);

  return (
    <View style={styles.container}>
      {Image && <Image />}
      {header && (
        <Text category="h4" status="info">
          {header}
        </Text>
      )}
      {description && (
        <Text style={styles.description} appearance="hint">
          {description}
        </Text>
      )}
      {Action && <Action />}
    </View>
  );
};

const stylesheet = createStyleSheet(theme => ({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    padding: theme.spacing.s16,
  },

  description: {
    marginTop: theme.spacing.s8,
    textAlign: 'center',
  },
}));
