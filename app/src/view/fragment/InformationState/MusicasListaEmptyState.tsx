import {FC} from 'react';

import {EmptyState} from './EmptyState';
import {NoDocumentsEmptyState} from './Icones/EmptyStateIcones';

export const MusicasListaEmptyState: FC = () => {
  return (
    <EmptyState
      image={NoDocumentsEmptyState}
      header="Sem músicas por aqui"
      description={'Parece que nenhuma música foi adicionada a essa lista.'}
    />
  );
};
