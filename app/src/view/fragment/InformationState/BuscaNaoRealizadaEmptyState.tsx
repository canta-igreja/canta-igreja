import {FC} from 'react';

import {EmptyState} from './EmptyState';
import {SearchEmptyState} from './Icones/EmptyStateIcones';

export const BuscaNaoRealizadaEmptyState: FC = () => {
  return (
    <EmptyState
      image={SearchEmptyState}
      header="Faça uma busca"
      description="Insira um trecho da música no campo de busca"
    />
  );
};
