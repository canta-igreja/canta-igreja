import {FC} from 'react';

import {EmptyState} from './EmptyState';
import {NoDocumentsEmptyState} from './Icones/EmptyStateIcones';

export const MusicasEmptyState: FC = () => {
  return (
    <EmptyState
      image={NoDocumentsEmptyState}
      header="Música não localizada"
      description={`Talvez ainda não esteja no catálogo. 
Será que há algum erro de digitação na busca?`}
    />
  );
};
