import {Button} from '@ui-kitten/components';
import React, {FC} from 'react';
import {createStyleSheet} from 'react-native-unistyles';
import {useSelector} from 'react-redux';
import {useAppDispatch} from 'src/store/store.hooks';
import {ListaMusicasAction} from 'src/store/ui/ListaMusicas/ListaMusicas.action';
import {ListaMusicasSelect} from 'src/store/ui/ListaMusicas/ListaMusicas.selector';
import {PaginaAnteriorIcon, PaginaSeguinteIcon} from '../../base/icones/Icones';
import {ListaMusicasWindow} from 'src/store/ui/ListaMusicas/ListaMusicas.types';

interface Props {
  window?: ListaMusicasWindow;
}
export const PaginacaoMusicasView: FC<Props> = ({window = ListaMusicasWindow.buscaMusicas}) => {
  const dispatch = useAppDispatch();

  const hasAnyMusica = useSelector(ListaMusicasSelect.hasAnyMusica(window));
  const isCarregando = useSelector(ListaMusicasSelect.isCarregando(window));
  const isPaginaInicial = useSelector(ListaMusicasSelect.isPaginaInicial(window));
  const hasPaginasACarregar = useSelector(ListaMusicasSelect.hasPaginasACarregar(window));

  const carregarPaginaAnterior = () => {
    if (!isPaginaInicial && !isCarregando) {
      // dispatch(ListaMusicasAction.paginaAnterior(window));
    }
  };
  const carregarPaginaSeguinte = () => {
    if (hasPaginasACarregar && !isCarregando) {
      dispatch(ListaMusicasAction.paginaSeguinte(window));
    }
  };

  if (!hasAnyMusica) {
    return <></>;
  }

  return (
    <>
      <Button
        style={[stylesheet.paginacao, stylesheet.paginacao.variants.botao.anterior]}
        accessoryLeft={PaginaAnteriorIcon}
        onPress={carregarPaginaAnterior}
        disabled={isPaginaInicial}
      />
      <Button
        style={[stylesheet.paginacao, stylesheet.paginacao.variants.botao.proxima]}
        accessoryLeft={PaginaSeguinteIcon}
        onPress={carregarPaginaSeguinte}
        disabled={!hasPaginasACarregar}
      />
    </>
  );
};

const stylesheet = createStyleSheet({
  paginacao: {
    width: 50,
    height: 50,
    borderRadius: 30,
    position: 'absolute',
    bottom: 10,

    variants: {
      botao: {
        proxima: {
          right: 10,
        },
        anterior: {
          right: 70,
        },
      },
    },
  },
});
