import {Layout} from '@ui-kitten/components';
import debounce from 'debounce-promise';
import React, {FC, useCallback} from 'react';
import {StyleSheet} from 'react-native';
import {useSelector} from 'react-redux';
import {EdicaoLivro} from 'src/model/Livro';
import {AppStack} from 'src/router/AppStackNavigator.types';
import {MusicaDrawerStack} from 'src/router/MusicaDrawerNavigator.types';
import {useMusicasStackNavigation} from 'src/router/navigation/useAppNavigation';
import {useAppDispatch} from 'src/store/store.hooks';
import {ListaMusicasAction} from 'src/store/ui/ListaMusicas/ListaMusicas.action';
import {ListaMusicasSelect} from 'src/store/ui/ListaMusicas/ListaMusicas.selector';
import {ListaMusicasWindow} from 'src/store/ui/ListaMusicas/ListaMusicas.types';
import {MusicaItemListData} from 'src/view/component/list/data/MusicaItemListData';
import {PesquisarInput} from '../../component/input/PesquisarInput';
import {SelecaoMusicasFragment} from '../SelecaoMusicasFragment';

type Props = {
  livro?: EdicaoLivro;
};

export const BuscaMusicasView: FC<Props> = ({livro}) => {
  const dispatch = useAppDispatch();
  const navigation = useMusicasStackNavigation();

  const musicas = useSelector(ListaMusicasSelect.allIds(ListaMusicasWindow.buscaMusicas));

  const termoBuscado = useSelector(ListaMusicasSelect.termoBuscado(ListaMusicasWindow.buscaMusicas));
  const estadoRequisicao = useSelector(ListaMusicasSelect.estadoRequisicao(ListaMusicasWindow.buscaMusicas));
  const isFinalResultados = useSelector(ListaMusicasSelect.isFinalResultados(ListaMusicasWindow.buscaMusicas));

  const irParaMusica = (musica: MusicaItemListData) =>
    navigation.navigate(AppStack.Musica, {
      screen: MusicaDrawerStack.MusicaDrawer,
      params: {idMusica: musica.id_musica, titulo: musica.titulo, indice: musica, livro: livro},
    });

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const pesquisar = useCallback(
    debounce(
      (texto: string) =>
        dispatch(
          ListaMusicasAction.buscar(ListaMusicasWindow.buscaMusicas, {
            idLivro: livro?.id_edicao_livro,
            termoBusca: texto,
          }),
        ),
      500,
    ),
    [dispatch, livro],
  );

  const carregarMais = useCallback(
    () => dispatch(ListaMusicasAction.paginaSeguinte(ListaMusicasWindow.buscaMusicas)),
    [dispatch],
  );

  // useEffect(() => {
  //   dispatch(MusicasAction.lista.iniciar(livro?.id_edicao_livro));
  // }, [dispatch, livro]);

  return (
    <Layout level="1" style={stylesheet.layout}>
      <PesquisarInput onPesquisar={pesquisar} considerarNumero={livro !== undefined} />
      <SelecaoMusicasFragment
        estadoRequisicao={estadoRequisicao}
        carregarMais={carregarMais}
        isFinalResultados={isFinalResultados}
        termoBuscado={termoBuscado}
        musicas={musicas}
        onSelectMusica={irParaMusica}
      />
    </Layout>
  );
};

const stylesheet = StyleSheet.create({
  layout: {
    flex: 1,
  },
});
