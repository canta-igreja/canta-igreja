import React, {FC, useMemo} from 'react';
import {MusicaItemListData} from 'src/view/component/list/data/MusicaItemListData';
import {EstadoRequisicao} from 'src/store/types';
import {buildMusicaItemList} from 'src/view/component/list/item/MusicaItemList';
import {MusicasList} from '../component/list/MusicasList';

type OnMusicaType = (musica: MusicaItemListData) => void;

interface MusicasViewProps {
  estadoRequisicao: EstadoRequisicao;
  carregarMais: undefined | (() => void);
  isFinalResultados: boolean;

  termoBuscado?: string;

  musicas: number[];

  EmptyState?: FC;

  onSelectMusica: OnMusicaType;
  onAddMusica?: OnMusicaType;
}

export const SelecaoMusicasFragment: FC<MusicasViewProps> = ({
  termoBuscado,
  musicas,
  onSelectMusica,
  onAddMusica,
  isFinalResultados,
  carregarMais,
  estadoRequisicao,
  EmptyState,
}) => {
  const MusicaItemView = useMemo(
    () => buildMusicaItemList(onSelectMusica, onAddMusica, termoBuscado),
    [onAddMusica, onSelectMusica, termoBuscado],
  );

  return (
    <MusicasList
      estadoRequisicao={estadoRequisicao}
      carregarMais={carregarMais}
      isFinalResultados={isFinalResultados}
      musicas={musicas}
      renderItem={MusicaItemView}
      EmptyState={EmptyState}
    />
  );
};
