import {Button} from '@ui-kitten/components';
import React, {FC, useMemo} from 'react';
import {ModalGenerico} from './ModalGenerico';
import {createStyleSheet} from 'react-native-unistyles';

interface Props {
  titulo?: string;
  isOpen: boolean;
  fecharModal: () => void;
  children: React.ReactElement;
}

export const OkModal: FC<Props> = ({titulo, isOpen, fecharModal, children}) => {
  const rodape = useMemo(
    () => (
      <Button style={stylesheet.button} status="basic" onPress={fecharModal}>
        OK
      </Button>
    ),
    [fecharModal],
  );

  return (
    <ModalGenerico titulo={titulo} rodape={rodape} isOpen={isOpen} fecharModal={fecharModal}>
      {children}
    </ModalGenerico>
  );
};

const stylesheet = createStyleSheet({
  button: {
    width: 94,
  },
});
