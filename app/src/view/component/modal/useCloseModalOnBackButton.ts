import {useFocusEffect} from '@react-navigation/native';
import {useCallback, useState} from 'react';
import {BackHandler} from 'react-native';

export const useModalState: () => [boolean, () => void, () => void] = () => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const abrirModal = useCallback(() => setIsOpen(true), []);
  const fecharModal = useCallback(() => setIsOpen(false), []);

  const closeModal = useCloseModalOnBackButton(isOpen, fecharModal);

  return [isOpen, abrirModal, closeModal];
};

export const useCloseModalOnBackButton = (isOpen: boolean, onClose: () => void) => {
  useFocusEffect(
    useCallback(() => {
      const onBackButton = () => {
        if (isOpen) {
          onClose();
          return true;
        }
        return false;
      };

      const subscription = BackHandler.addEventListener('hardwareBackPress', onBackButton);
      return () => subscription.remove();
    }, [isOpen, onClose]),
  );

  return onClose;
};
