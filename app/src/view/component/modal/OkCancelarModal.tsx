import {Button} from '@ui-kitten/components';
import React, {FC} from 'react';
import {View} from 'react-native';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {ModalGenerico} from './ModalGenerico';

interface OkCancelarModalProps {
  titulo: string;
  isOpen: boolean;
  isOkEnabled?: boolean;

  onOk: () => void;
  onCancelado: () => void;

  okLabel?: string;
  cancelarLabel?: string;

  children: React.ReactElement;
}

export const OkCancelarModal: FC<OkCancelarModalProps> = ({
  children,
  titulo,
  isOpen,
  onOk,
  onCancelado,
  okLabel,
  cancelarLabel,
  isOkEnabled,
}) => {
  const {styles} = useStyles(stylesheet);

  const rodape = (
    <View style={styles.rodape}>
      <Button style={styles.botaoEsquerdo} status="basic" onPress={onOk} disabled={isOkEnabled === false}>
        {okLabel ?? 'OK'}
      </Button>
      <Button style={styles.botaoDireito} status="basic" onPress={onCancelado}>
        {cancelarLabel ?? 'Cancelar'}
      </Button>
    </View>
  );

  return (
    <ModalGenerico titulo={titulo} rodape={rodape} isOpen={isOpen} fecharModal={onCancelado}>
      {children}
    </ModalGenerico>
  );
};

const stylesheet = createStyleSheet(theme => ({
  rodape: {
    display: 'flex',
    flexDirection: 'row',
  },
  botaoDireito: {
    minWidth: 100,
    marginLeft: theme.spacing.s8,
  },
  botaoEsquerdo: {
    minWidth: 100,
    marginRight: theme.spacing.s8,
  },
}));
