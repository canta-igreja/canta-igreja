import React, {FC} from 'react';
import {Modal, Text} from '@ui-kitten/components';
import {useKeyboardSize} from 'src/util/Keyboard';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {View} from 'react-native';

interface Props {
  titulo?: string;
  rodape: React.ReactElement;
  isOpen: boolean;
  fecharModal: () => void;
  children: React.ReactElement;
}

export const ModalGenerico: FC<Props> = ({titulo, isOpen, rodape, fecharModal, children}) => {
  const keyboardSize = useKeyboardSize();
  const {styles} = useStyles(stylesheet);

  return (
    <Modal
      visible={isOpen}
      backdropStyle={styles.backdrop}
      onBackdropPress={fecharModal}
      supportedOrientations={['portrait', 'portrait-upside-down', 'landscape', 'landscape-left', 'landscape-right']}>
      <View style={[styles.fundo, {marginBottom: keyboardSize + 20}]}>
        {titulo && (
          <Text
            //  style={styles.title}
            category="h5">
            {titulo}
          </Text>
        )}

        <View style={styles.content}>{children}</View>

        <View style={styles.rodape}>{rodape}</View>
      </View>
    </Modal>
  );
};

const stylesheet = createStyleSheet(theme => ({
  fundo: {
    backgroundColor: 'white',

    padding: theme.spacing.s16,

    borderRadius: theme.border.radius.sm,
    borderColor: '#e4e9f2',
    borderWidth: theme.border.width.md,
  },
  title: {
    fontWeight: 'bold',
  },
  content: {
    paddingVertical: theme.spacing.s16,
  },
  rodape: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
}));
