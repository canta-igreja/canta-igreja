import {Input, Text} from '@ui-kitten/components';
import React, {FC, useCallback, useRef, useState} from 'react';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {OkCancelarModal} from 'src/view/component/modal/OkCancelarModal';

interface Props {
  titulo: string;
  placeholder: string;
  okLabel: string;
  tamanhoMaximo: number;

  isOpen: boolean;
  valorInicial?: string;

  onConfirmado: (nome: string) => void;
  onCancelado: () => void;
}

export const InputModal: FC<Props> = ({
  titulo,
  placeholder,
  tamanhoMaximo,
  okLabel,
  isOpen,
  valorInicial,
  onConfirmado,
  onCancelado,
}) => {
  const {styles} = useStyles(stylesheet);
  const inputRef = useRef<Input>();
  const [name, setName] = useState(valorInicial ?? '');

  const isValido = 0 < name.length && name.length <= tamanhoMaximo;

  const onSubmit = useCallback(() => {
    const valor = inputRef.current?.props.value;

    if (valor) {
      const isValido = 0 < valor.length && valor.length <= tamanhoMaximo;
      if (isValido) {
        setName('');
        onConfirmado(valor);
      }
    }
  }, [onConfirmado, tamanhoMaximo]);

  return (
    <OkCancelarModal
      okLabel={okLabel}
      isOpen={isOpen}
      titulo={titulo}
      onOk={onSubmit}
      onCancelado={onCancelado}
      isOkEnabled={isValido}>
      <>
        <Input
          ref={inputRef as any}
          // defaultValue={valorInicial}
          value={name}
          placeholder={placeholder}
          onSubmitEditing={onSubmit}
          onChangeText={setName}
          maxLength={tamanhoMaximo}
        />
        <Text style={styles.characterCount} category="s2" appearance="hint">
          {name.length}/{tamanhoMaximo}
        </Text>
      </>
    </OkCancelarModal>
  );
};

const stylesheet = createStyleSheet(theme => ({
  characterCount: {
    paddingTop: theme.spacing.s8,
    textAlign: 'center',
  },
}));
