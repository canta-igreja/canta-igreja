import {Input} from '@ui-kitten/components';
import React, {FC, useCallback} from 'react';
import {NativeSyntheticEvent, TextInputSubmitEditingEventData} from 'react-native';
import {createStyleSheet, useStyles} from 'react-native-unistyles';
import {BuscarIcon} from 'src/view/base/icones/Icones';

interface PesquisarInputProps {
  onPesquisar: (texto: string) => void;
  considerarNumero: boolean;
}

export const PesquisarInput: FC<PesquisarInputProps> = ({onPesquisar, considerarNumero}) => {
  const {styles} = useStyles(stylesheet);

  const onSubmit = useCallback(
    (it: NativeSyntheticEvent<TextInputSubmitEditingEventData>) => onPesquisar(it.nativeEvent.text),
    [onPesquisar],
  );
  const onSubmitQuandoMuda = useCallback((texto: string) => onPesquisar(texto), [onPesquisar]);

  const placeholder = considerarNumero ? 'Número ou trecho da música' : 'Nome ou trecho da música';

  return (
    <Input
      style={styles.input}
      placeholder={placeholder}
      accessoryRight={BuscarIcon}
      onChangeText={onSubmitQuandoMuda}
      // onSubmitEditing={onSubmit}
    />
  );
};

const stylesheet = createStyleSheet(theme => ({
  input: {
    paddingHorizontal: {
      xs: 0,
      sm: theme.spacing.s8,
    },
    paddingTop: theme.spacing.s8,
    zIndex: 1,
  },
}));
