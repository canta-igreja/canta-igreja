import {FlashList, ListRenderItem} from '@shopify/flash-list';
import {Divider} from '@ui-kitten/components';
import React, {FC, useCallback, useMemo} from 'react';
import {EstadoRequisicao} from 'src/store/types';
import {KeyExtractor} from 'src/util/KeyExtractor';
import {FimDaLista} from 'src/view/component/list/item/FimDaLista';
import {BuscaNaoRealizadaEmptyState} from '../../fragment/InformationState/BuscaNaoRealizadaEmptyState';
import {MusicasEmptyState} from '../../fragment/InformationState/MusicasEmptyState';

interface Props {
  estadoRequisicao: EstadoRequisicao;
  carregarMais: undefined | (() => void);
  isFinalResultados: boolean;

  musicas: number[];
  renderItem: ListRenderItem<number>;

  EmptyState?: FC;
}

export const MusicasList: FC<Props> = ({
  estadoRequisicao,
  carregarMais,
  isFinalResultados,

  musicas,
  renderItem,

  EmptyState = MusicasEmptyState,
}) => {
  const isCarregando = estadoRequisicao === EstadoRequisicao.CARREGANDO;
  const ListFooter = useMemo(() => <FimDaLista isCarregando={!isFinalResultados} />, [isFinalResultados]);
  const onEndReached = useCallback(() => !isCarregando && carregarMais && carregarMais(), [carregarMais, isCarregando]);

  if (estadoRequisicao === EstadoRequisicao.OCIOSO) {
    return <BuscaNaoRealizadaEmptyState />;
  } else if (estadoRequisicao !== EstadoRequisicao.CARREGANDO && musicas.length === 0) {
    return <EmptyState />;
  }

  return (
    <FlashList<number>
      keyboardShouldPersistTaps="always"
      data={musicas}
      renderItem={renderItem}
      keyExtractor={KeyExtractor.fromNumber}
      ItemSeparatorComponent={Divider}
      ListFooterComponent={ListFooter}
      onEndReached={onEndReached}
      estimatedItemSize={60}
    />
  );
};
