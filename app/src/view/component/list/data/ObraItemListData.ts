import {Obra} from 'src/model/obra/Obra';

export enum SituacaoItemObra {
  NAO_BAIXADA,
  NAO_ATUALIZADA,
  ATUALIZADA,
}

export interface ObraItemListData {
  obra: Obra;
  situacao: SituacaoItemObra;
}
