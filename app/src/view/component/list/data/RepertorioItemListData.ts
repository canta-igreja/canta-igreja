export interface RepertorioItemListData {
  id_repertorio: number;
  titulo: string;
  resumo: string;
}
