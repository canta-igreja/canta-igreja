import {ReferenciaMusica} from 'src/model/Musica';

export interface MusicaItemListData extends ReferenciaMusica {
  trecho_letra: string;

  livro?: {
    indice: string;
    titulo: string;
    sigla: string;
  };

  tonalidades?: string[];
  momentos?: string[];
}
