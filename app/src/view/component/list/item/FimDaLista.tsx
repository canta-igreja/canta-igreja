import React, {FC} from 'react';
import {View} from 'react-native';
import {createStyleSheet} from 'react-native-unistyles';
import {CarregandoIcone} from 'src/view/base/icones/CarregandoIcone';

interface Props {
  isCarregando: boolean;
}

export const FimDaLista: FC<Props> = ({isCarregando}) => {
  if (!isCarregando) {
    return <></>;
  }

  return (
    <View style={stylesheet.container}>
      <CarregandoIcone />
    </View>
  );
};

const stylesheet = createStyleSheet({
  container: {
    flexDirection: 'column',
    alignItems: 'center',
    paddingVertical: 16,
  },
});
