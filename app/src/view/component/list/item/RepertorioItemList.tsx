import {ListItem} from '@ui-kitten/components';
import React, {FC, useCallback} from 'react';
import {ListRenderItem} from 'react-native';
import {useSelector} from 'react-redux';
import {Repertorio} from 'src/model/repertorio/Repertorio';
import {RepertoriosSelect} from 'src/store/Repertorios/Repertorios.selector';

type OnRepertorio = (repertorio: Repertorio) => void;

export const buildRepertorioItemList = (onItemSelected: OnRepertorio): ListRenderItem<number> => {
  return ({item}) => <RepertorioListItem onItemSelected={onItemSelected} idRepertorio={item} />;
};

interface Props {
  idRepertorio: number;
  onItemSelected: OnRepertorio;
}
const RepertorioListItem: FC<Props> = ({idRepertorio, onItemSelected}) => {
  const repertorio = useSelector(RepertoriosSelect.byId(idRepertorio));

  const onPress = useCallback(() => repertorio && onItemSelected(repertorio), [onItemSelected, repertorio]);

  return repertorio ? <ListItem title={repertorio.titulo} description={repertorio.resumo} onPress={onPress} /> : null;
};
