import {ListItem} from '@ui-kitten/components';
import React, {FC, useCallback} from 'react';
import {ListRenderItem} from 'react-native';
import {useSelector} from 'react-redux';
import {EdicaoLivro} from 'src/model/Livro';
import {LivrosSelect} from 'src/store/Livros/Livros.selector';

type OnLivro = (livro: EdicaoLivro) => void;

export const buildLivroItemList = (onItemSelected: OnLivro): ListRenderItem<number> => {
  return ({item}) => <LivroListItem onItemSelected={onItemSelected} idLivro={item} />;
};

interface Props {
  idLivro: number;
  onItemSelected: OnLivro;
}
const LivroListItem: FC<Props> = ({idLivro, onItemSelected}) => {
  const livro = useSelector(LivrosSelect.byId(idLivro));

  const onPress = useCallback(() => livro && onItemSelected(livro), [onItemSelected, livro]);

  return livro ? (
    <ListItem
      title={`${livro.titulo}`}
      description={`${livro.edicao} - ${livro.editora}`}
      // accessoryLeft={renderItemIcon}
      onPress={onPress}
    />
  ) : null;
};
