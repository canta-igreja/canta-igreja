import {Layout, ListItem} from '@ui-kitten/components';
import React, {FC, useCallback, useMemo} from 'react';
import {View} from 'react-native';
import {createStyleSheet} from 'react-native-unistyles';
import {useSelector} from 'react-redux';
import {Toast} from 'src/arch/util/Toast';
import {Obra} from 'src/model/obra/Obra';
import {BibliotecaSelector} from 'src/store/Biblioteca/Biblioteca.selector';
import {CarregandoIcone} from 'src/view/base/icones/CarregandoIcone';
import {AtualizadoIcon, AtualizarIcon, BaixarIcon} from 'src/view/base/icones/Icones';
import {ObraItemListData, SituacaoItemObra} from '../data/ObraItemListData';

interface Props {
  item: ObraItemListData;
  onPress: (obra: Obra) => void;
}

export const ObraItemList: FC<Props> = ({item, onPress}) => {
  const onClick = useCallback(() => {
    if (item.situacao === SituacaoItemObra.ATUALIZADA) {
      Toast.show('Músicas já baixadas');
    } else {
      onPress(item.obra);
    }
  }, [item.obra, item.situacao, onPress]);

  return <ObraItemListView item={item} onPress={onClick} />;
};

interface ObraItemListViewProps {
  item: ObraItemListData;
  onPress: () => void;
  disabled?: boolean;
}

export const ObraItemListView: FC<ObraItemListViewProps> = ({item, onPress, disabled}) => {
  const Icone = useMemo(() => <IconeObra item={item} />, [item]);

  return (
    <ListItem
      title={item.obra.titulo}
      description={item.obra.descricao.trim()}
      accessoryLeft={Icone}
      onPress={onPress}
      disabled={disabled}
    />
  );
};

interface IconeObraProps {
  item: ObraItemListData;
}

const IconeObra: FC<IconeObraProps> = ({item}) => {
  const obraEmDownload = useSelector(BibliotecaSelector.obraSendoBaixada);

  let Icone: any = View;
  if (obraEmDownload?.id_obra === item.obra.id_obra) {
    Icone = CarregandoIcone;
  } else if (item.situacao === SituacaoItemObra.NAO_ATUALIZADA) {
    Icone = AtualizarIcon;
  } else if (item.situacao === SituacaoItemObra.ATUALIZADA) {
    Icone = AtualizadoIcon;
  } else {
    Icone = BaixarIcon;
  }

  return (
    <Layout style={stylesheet.itemContainer} level="3">
      <Icone style={stylesheet.icone} />
    </Layout>
  );
};

const stylesheet = createStyleSheet({
  itemContainer: {
    width: 44,
    height: 44,
    borderRadius: 44 / 2,
    paddingHorizontal: 8,

    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  // FIXME - alterar cor para theme
  icone: {width: 28, height: 28, tintColor: '#2E3A59'},
});
// 600: '#8F9BB3',
//       700: '#2E3A59',
//       800: '#222B45',
//       900: '#1A2138',
//       1000: '#151A30',
//       1100: '#101426',
