import {ListRenderItem} from '@shopify/flash-list';
import {Button, TextProps} from '@ui-kitten/components';
import {FalsyText} from '@ui-kitten/components/devsupport';
import React, {FC, useCallback, useMemo} from 'react';
import {Text} from 'react-native';
import {useSelector} from 'react-redux';
import {ListaMusicasSelect} from 'src/store/ui/ListaMusicas/ListaMusicas.selector';
import {ListUtil} from 'src/util/ListUtil';
import {MaisIcon} from 'src/view/base/icones/Icones';
import {ThreeLinesListItem} from 'src/view/component/list/item/ThreeLinesItemList';
import {MusicaItemListData} from '../data/MusicaItemListData';

export type OnMusicaItemListData = (item: MusicaItemListData) => void;

export const buildMusicaItemList = (
  onSelectMusica: OnMusicaItemListData,
  onAddMusica: OnMusicaItemListData | undefined,
  termoBuscado: string | undefined,
): ListRenderItem<number> => {
  return ({item}) => {
    return (
      <MusicaItemList
        idMusica={item}
        termoBuscado={termoBuscado}
        onSelectMusica={onSelectMusica}
        onAddMusica={onAddMusica}
      />
    );
  };
};

interface Props {
  idMusica: number;
  termoBuscado?: string;
  onSelectMusica: OnMusicaItemListData;
  onAddMusica: OnMusicaItemListData | undefined;
}

const MusicaItemList: FC<Props> = ({idMusica, termoBuscado, onSelectMusica, onAddMusica}) => {
  const musica = useSelector(ListaMusicasSelect.byId(idMusica));

  const conteudo = useMemo(() => musica && ListUtil.getConteudoItemList(musica, termoBuscado), [musica, termoBuscado]);

  const Title = useCallback(
    (textProps: TextProps | undefined) => (
      <Text numberOfLines={1} {...textProps}>
        {`${conteudo?.indice}`}
        <FalsyText component={conteudo?.titulo} style={textProps?.style} />
      </Text>
    ),
    [conteudo?.indice, conteudo?.titulo],
  );
  const Description = useCallback(
    (textProps: TextProps | undefined) => (
      <Text numberOfLines={1} style={textProps?.style}>
        <FalsyText component={conteudo?.trechoLetra} style={textProps?.style} />
      </Text>
    ),
    [conteudo?.trechoLetra],
  );

  const onPress = useCallback(() => musica && onSelectMusica(musica), [musica, onSelectMusica]);
  const onAccessoryRightPress = useCallback(() => musica && onAddMusica && onAddMusica(musica), [musica, onAddMusica]);

  const AccessoryRight = useMemo(
    () =>
      onAddMusica && (
        <Button status="basic" appearance="ghost" accessoryLeft={MaisIcon} onPress={onAccessoryRightPress} />
      ),
    [onAddMusica, onAccessoryRightPress],
  );

  return (
    <ThreeLinesListItem
      header={conteudo?.livro}
      title={Title}
      description={Description}
      onPress={onPress}
      accessoryRight={AccessoryRight}
    />
  );
};
