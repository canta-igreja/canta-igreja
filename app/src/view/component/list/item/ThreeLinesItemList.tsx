import {ListItem, ListItemProps, Text, TextProps} from '@ui-kitten/components';
import {FalsyText} from '@ui-kitten/components/devsupport';
import {FC, useCallback} from 'react';
import {ItemHeader} from 'src/view/base/Texto';

interface ItemViewProps extends ListItemProps {
  header?: string;
}

export const ThreeLinesListItem: FC<ItemViewProps> = ({header, title, description, ...props}) => {
  const Title = useCallback(
    (textProps: TextProps | undefined) => (
      <Text {...textProps} numberOfLines={2}>
        <>
          {header && (
            <ItemHeader>
              {header.substring(0, 32)}
              {'\n'}
            </ItemHeader>
          )}
          <FalsyText component={title} style={textProps?.style} />
        </>
      </Text>
    ),
    [header, title],
  );
  const Description = useCallback(
    (textProps: TextProps | undefined) => (
      <Text {...textProps} numberOfLines={1}>
        <FalsyText component={description} style={textProps?.style} />
      </Text>
    ),
    [description],
  );

  return <ListItem {...props} title={Title} description={Description} />;
};
