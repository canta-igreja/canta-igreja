import {Button} from '@ui-kitten/components';
import React, {FC, useCallback, useMemo} from 'react';
import {OpacityDecorator, ScaleDecorator, ShadowDecorator} from 'react-native-draggable-flatlist';
import {useSelector} from 'react-redux';
import {ItemRepertorio} from 'src/model/repertorio/Repertorio';
import {ItensRepertorioSelect} from 'src/store/ItensRepertorio/ItensRepertorio.selector';
import {TresPontosVerticalIcon} from 'src/view/base/icones/Icones';
import {ThreeLinesListItem} from './ThreeLinesItemList';

export type OnItemRepertorio = (item: ItemRepertorio) => void;

interface Props {
  idItemRepertorio: number;
  drag: () => void;
  onSelect: OnItemRepertorio;
  onSettings: OnItemRepertorio;
}

export const ItemRepertorioListItem: FC<Props> = ({idItemRepertorio, drag, onSelect, onSettings}) => {
  const item = useSelector(ItensRepertorioSelect.byId(idItemRepertorio));

  const onPress = useCallback(() => item && onSelect(item), [item, onSelect]);
  const onPressSettings = useCallback(() => item && onSettings(item), [item, onSettings]);

  const tonalidadePrefixo = item?.tonalidade.valor ? `${item.tonalidade.valor}: ` : '';
  const title = `${tonalidadePrefixo}${item?.musica?.titulo ?? item?.termo}`;

  const AccessoryRight = useMemo(
    () => <Button status="basic" appearance="ghost" accessoryLeft={TresPontosVerticalIcon} onPress={onPressSettings} />,
    [onPressSettings],
  );

  // FIXME
  // const navStack = useNavStack();
  // const abrirModalTonalidade = useCallback(() => item && navStack.navigateToTonalidadeModal(item), [navStack, item]);
  // const onSwipe = ({openDirection}: any) => {
  //   if (openDirection === 'right') {
  //     abrirModalTonalidade();
  //   }
  // };

  if (item === undefined) {
    return null;
  }

  return (
    <ShadowDecorator key={item.id_item_repertorio}>
      <ScaleDecorator>
        <OpacityDecorator>
          {/* <SwipeableItem
            item={item}
            swipeDamping={5}
            onChange={onSwipe}
            renderUnderlayRight={() => (
              <View
                style={{
                  flex: 1,
                  backgroundColor: 'tomato',
                  justifyContent: 'flex-end',
                }}>
                <Text>Teste</Text>
              </View>
            )}
            snapPointsRight={[50]}> */}
          <ThreeLinesListItem
            header={item.momento.valor}
            title={title}
            description={item.musica?.trecho_letra}
            onPress={onPress}
            onLongPress={drag}
            accessoryRight={AccessoryRight}
          />
          {/* </SwipeableItem> */}
        </OpacityDecorator>
      </ScaleDecorator>
    </ShadowDecorator>
  );
};
