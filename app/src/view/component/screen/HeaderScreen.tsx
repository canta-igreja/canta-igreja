import {FC} from 'react';
import {View, ViewProps} from 'react-native';
import {createStyleSheet, useStyles} from 'react-native-unistyles';

export const HeaderScreen: FC<ViewProps> = props => {
  const {styles} = useStyles(stylesheet);

  return <View style={styles.view} {...props} />;
};

const stylesheet = createStyleSheet(title => ({
  view: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexWrap: 'wrap',

    paddingBottom: title.spacing.s8,
  },
}));
