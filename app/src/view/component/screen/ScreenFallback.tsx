import React, {FC} from 'react';
import {StyleSheet} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {CarregandoIcone} from 'src/view/base/icones/CarregandoIcone';

export const ScreenFallback: FC = () => (
  <SafeAreaView style={styles.screen}>
    <CarregandoIcone />
  </SafeAreaView>
);

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: 'white',
    padding: 32,
  },
});
