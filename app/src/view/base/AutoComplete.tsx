import React, {FC, useCallback, useState} from 'react';
import {Autocomplete} from '@ui-kitten/components';
import {DetalhesMusicaUtil} from 'src/util/DetalhesMusicaUtil';
import {LayoutChangeEvent, View} from 'react-native';

interface Props {
  label: string;
  placeholder: string;
  maxLength: number;
  defaultOptions: string[];
}

/**
 * @deprecated Não funciona direito
 */
export const AutoComplete: FC<Props> = ({label, placeholder, maxLength, defaultOptions}) => {
  const [content, setContent] = React.useState({
    query: undefined as string | undefined,
    items: defaultOptions,
  });
  const [width, setWidth] = useState(100);

  // Workaround: https://github.com/akveo/react-native-ui-kitten/issues/1736#issuecomment-1911537143
  const handleWidth = (event: LayoutChangeEvent) => {
    const {width: newWidth} = event.nativeEvent.layout;
    setWidth(newWidth);
  };

  const onSelect = useCallback(
    (index: number): void => setContent(it => ({...it, query: content.items[index]})),
    [content.items],
  );
  const onChangeText = useCallback(
    (query: string) => {
      const items = DetalhesMusicaUtil.filter(query, defaultOptions);
      // Workaround - https://github.com/akveo/react-native-ui-kitten/issues/1744#issuecomment-1597818436
      items.length > 0 //
        ? setContent({query, items})
        : setContent({query, items: [query]});
    },
    [defaultOptions],
  );

  return (
    <View onLayout={handleWidth}>
      <Autocomplete
        style={{width}}
        label={label}
        placeholder={placeholder}
        value={content.query}
        placement="inner bottom"
        // placement="top start"
        onSelect={onSelect}
        onChangeText={onChangeText}
        maxLength={maxLength}>
        {content.items.map(DetalhesMusicaUtil.renderAsAutoCompleteItem)}
      </Autocomplete>
    </View>
  );
};
