import {ListItem, ListItemProps} from '@ui-kitten/components';
import {FC} from 'react';
import {ViewProps} from 'react-native';
import {View} from 'react-native';
import {createStyleSheet, useStyles} from 'react-native-unistyles';

export const ListaArredondada: FC<ViewProps> = props => {
  const {styles} = useStyles(stylesheet);

  return <View style={styles.listaArredondada} {...props} />;
};

interface ItemProps extends ListItemProps {
  tipo?: 'unico' | 'superior' | 'intermediario' | 'inferior';
}

export const ItemListaArredondada: FC<ItemProps> = ({tipo, ...props}) => {
  const {styles} = useStyles(stylesheet, {tipo: tipo ?? 'unico'});

  return <ListItem style={styles.itemLista} {...props} />;
};

const stylesheet = createStyleSheet(theme => ({
  listaArredondada: {
    marginHorizontal: theme.spacing.s16,
    marginBottom: theme.spacing.s16,
  },
  itemLista: {
    borderColor: theme.border.color,
    borderLeftWidth: theme.border.width.md,
    borderRightWidth: theme.border.width.md,

    variants: {
      tipo: {
        unico: {
          borderRadius: theme.border.radius.md,
          borderWidth: theme.border.width.md,
        },
        superior: {
          borderTopRadius: theme.border.radius.md,
          borderTopWidth: theme.border.width.md,
          borderTopLeftRadius: theme.border.radius.md,
          borderTopRightRadius: theme.border.radius.md,
        },
        intermediario: {},
        inferior: {
          borderBottomRadius: theme.border.radius.md,
          borderBottomWidth: theme.border.width.md,
          borderBottomLeftRadius: theme.border.radius.md,
          borderBottomRightRadius: theme.border.radius.md,
        },
      },
    },
  },
}));
