import {Text, TextElement, TextProps} from '@ui-kitten/components';
import React, {FC, useMemo} from 'react';
import {createStyleSheet} from 'react-native-unistyles';

interface TituloProps extends TextProps {
  children: string | number | TextElement;
}

export const TituloPagina: FC<TituloProps> = ({children, style}) => {
  return (
    <Negrito category="h3" style={style}>
      {children}
    </Negrito>
  );
};

export const SubtituloPagina: FC<TituloProps> = ({children, style}) => {
  return (
    <Text category="h6" style={style}>
      {children}
    </Text>
  );
};

export const Negrito: FC<TextProps> = props => {
  const styles = useMemo(() => [props?.style, stylesheet.negrito], [props?.style]);
  return <Text {...props} style={styles} />;
};

export const ItemHeader: FC<TextProps> = props => (
  <Text style={stylesheet.itemHeader} category="c1" appearance="hint" {...props} />
);

const stylesheet = createStyleSheet({
  negrito: {
    fontWeight: 'bold',
  },
  itemHeader: {
    textTransform: 'uppercase',
  },
});
