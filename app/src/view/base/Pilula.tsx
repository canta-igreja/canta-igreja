import {FC} from 'react';
import {Text, View} from 'react-native';
import {createStyleSheet} from 'react-native-unistyles';

export const Pilula = (props: any) => <Text style={stylesheet.pilula} {...props} />;

interface PilulasProps {
  conteudo: {
    key: number | string;
    texto: string;
  }[];
}

export const Pilulas: FC<PilulasProps> = ({conteudo}) => {
  if (conteudo.length === 0) {
    return <View />;
  }

  const pilulas = conteudo.map(it => <Pilula key={it.key}>{it.texto}</Pilula>);

  return (
    <View style={stylesheet.container}>
      {pilulas}
      {/* <Pilula size='tiny' appearance="outline">Tempo Comum</Pilula> */}
    </View>
  );
};

const stylesheet = createStyleSheet({
  pilula: {
    borderRadius: 16,
    marginHorizontal: 0,
    padding: 2,
    paddingLeft: 8,
    paddingRight: 8,
    fontWeight: '100',
    fontSize: 12,
    backgroundColor: '#2e3a59',
    color: 'white',
  },
  container: {
    flexDirection: 'row',
    alignSelf: 'center',
    paddingVertical: 4,
  },
});
