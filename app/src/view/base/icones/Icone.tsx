import React, {FC} from 'react';
import Svg from 'react-native-svg';
import {StyleSheet} from 'react-native';

/**
 * 1. Select a icon here: https://akveo.github.io/eva-icons/
 * 2. Find equivalent icon here: https://github.com/akveo/eva-icons/tree/master/package/icons
 * 3. Clean unnecessary svg: https://svgomg.net/
 * 4. Convert to react-native-svg if necessary: https://transform.tools/svg-to-react-native
 */
export const Icone: FC = ({children, ...props}: any) => {
  const {style, ...svgProps} = props;
  let fillColor: string = StyleSheet.flatten(style || {}).tintColor;
  return (
    <Svg style={props.style} fill={fillColor} {...svgProps} viewBox="0 0 24 24">
      {children}
    </Svg>
  );
};
