import {ActivityIndicator} from 'react-native';

export const CarregandoIcone = (props: any) => <ActivityIndicator size="small" color="#102694" {...props} />;
