import React from 'react';
import {Circle, Rect, Path, SvgXml} from 'react-native-svg';
import {Image} from 'react-native';
import {Icone} from './Icone';

/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/outline/svg/search-outline.svg */
export const BuscarIcon = (props: any) => (
  <Icone {...props}>
    <Path d="M20.71 19.29l-3.4-3.39A7.92 7.92 0 0019 11a8 8 0 10-8 8 7.92 7.92 0 004.9-1.69l3.39 3.4a1 1 0 001.42 0 1 1 0 000-1.42zM5 11a6 6 0 116 6 6 6 0 01-6-6z" />
  </Icone>
);

/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/outline/svg/book-outline.svg */
export const LivroIcon = (props: any) => (
  <Icone {...props}>
    <Path d="M19 3H7a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1ZM7 5h11v10H7a3 3 0 0 0-1 .18V6a1 1 0 0 1 1-1Zm0 14a1 1 0 0 1 0-2h11v2Z" />
  </Icone>
);
/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/outline/svg/more-vertical-outline.svg */
export const TresPontosVerticalIcon = (props: any) => (
  <Icone {...props}>
    <Circle cx="12" cy="12" r="2" />
    <Circle cx="12" cy="5" r="2" />
    <Circle cx="12" cy="19" r="2" />
  </Icone>
);

/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/fill/svg/plus.svg */
export const MaisIcon = (props: any) => (
  <Icone {...props}>
    <Path d="M19 11h-6V5a1 1 0 0 0-2 0v6H5a1 1 0 0 0 0 2h6v6a1 1 0 0 0 2 0v-6h6a1 1 0 0 0 0-2Z" />
  </Icone>
);
export const EditarIcon = (props: any) => (
  <Icone {...props}>
    <Path d="M19 20H5a1 1 0 0 0 0 2h14a1 1 0 0 0 0-2ZM5 18h.09l4.17-.38a2 2 0 0 0 1.21-.57l9-9a1.92 1.92 0 0 0-.07-2.71L16.66 2.6A2 2 0 0 0 14 2.53l-9 9a2 2 0 0 0-.57 1.21L4 16.91a1 1 0 0 0 .29.8A1 1 0 0 0 5 18ZM15.27 4 18 6.73l-2 1.95L13.32 6Zm-8.9 8.91L12 7.32l2.7 2.7-5.6 5.6-3 .28Z" />
  </Icone>
);

/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/fill/svg/plus-square-outline.svg */
export const IncluirEmRepertorioIcon = (props: any) => (
  <Icone {...props}>
    <Path d="M18 3H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3Zm1 15a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1Z" />
    <Path d="M15 11h-2V9a1 1 0 0 0-2 0v2H9a1 1 0 0 0 0 2h2v2a1 1 0 0 0 2 0v-2h2a1 1 0 0 0 0-2Z" />
  </Icone>
);
/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/fill/svg/menu.svg */
export const SecoesIcon = (props: any) => (
  <Icone {...props} name="menu">
    <Rect width="18" height="2" x="3" y="11" rx=".9" ry=".9" />
    <Rect width="18" height="2" x="3" y="16" rx=".9" ry=".9" />
    <Rect width="18" height="2" x="3" y="6" rx=".9" ry=".9" />
  </Icone>
);
/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/fill/svg/menu-2.svg */
export const RepertoriosIcon = (props: any) => (
  <Icone {...props}>
    <Circle cx="4" cy="12" r="1" />
    <Rect width="14" height="2" x="7" y="11" rx=".9" ry=".9" />
    <Rect width="18" height="2" x="3" y="16" rx=".9" ry=".9" />
    <Rect width="18" height="2" x="3" y="6" rx=".9" ry=".9" />
  </Icone>
);

export const YoutubeIcon = (props: any) => {
  const xml = `
<svg xmlns="http://www.w3.org/2000/svg" height="477" viewBox="0 -77 512.002 357.75" width="682.667">
  <path d="M501.453-20.907c-5.902-21.934-23.195-39.223-45.125-45.13C416.262-77 255.996-77 255.996-77s-160.262 0-200.328 10.547C34.16-60.552 16.445-42.837 10.543-20.907 0 19.155 0 102.24 0 102.24s0 83.504 10.543 123.149c5.906 21.93 23.195 39.222 45.129 45.128C96.156 281.483 256 281.483 256 281.483s160.262 0 200.328-10.547c21.934-5.902 39.223-23.195 45.129-45.125C512 185.745 512 102.663 512 102.663s.422-83.508-10.547-123.57zm0 0" fill="red"/>
  <path d="M204.969 178.999l133.27-76.758-133.27-76.758zm0 0" fill="#fff"/>
</svg>
`;
  return <SvgXml height="20" width="40" {...props} xml={xml} />;
};

export const CifraClubIcon = () => (
  <Image style={{width: 25, height: 25}} source={require('../../../assets/cifraclub.png')} />
);

/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/fill/svg/arrow-back-outline.svg */
export const PaginaAnteriorIcon = (props: any) => (
  <Icone {...props}>
    <Path d="M19 11H7.14l3.63-4.36a1 1 0 1 0-1.54-1.28l-5 6a1.19 1.19 0 0 0-.09.15c0 .05 0 .08-.07.13A1 1 0 0 0 4 12a1 1 0 0 0 .07.36c0 .05 0 .08.07.13a1.19 1.19 0 0 0 .09.15l5 6A1 1 0 0 0 10 19a1 1 0 0 0 .64-.23 1 1 0 0 0 .13-1.41L7.14 13H19a1 1 0 0 0 0-2Z" />
  </Icone>
);
/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/fill/svg/arrow-forward-outline.svg */
export const PaginaSeguinteIcon = (props: any) => (
  <Icone {...props}>
    <Path d="M5 13h11.9l-3.7 4.4a1 1 0 0 0 1.6 1.2l5-6a1.2 1.2 0 0 0 0-.1l.1-.1a1 1 0 0 0 .1-.4 1 1 0 0 0 0-.4h-.1a1.2 1.2 0 0 0-.1-.2l-5-6A1 1 0 0 0 14 5a1 1 0 0 0-.6.2 1 1 0 0 0-.2 1.4L17 11H5a1 1 0 0 0 0 2Z" />
  </Icone>
);

/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/fill/svg/music-outline.svg */
export const PartituraIcon = (props: any) => (
  <Icone {...props}>
    <Path d="M19 15V4a1 1 0 0 0-.4-.8 1 1 0 0 0-.8-.2l-9 2A1 1 0 0 0 8 6v8.3a3.5 3.5 0 1 0 2 3.2 4.4 4.4 0 0 0 0-.5V6.8l7-1.5v7a3.5 3.5 0 1 0 2 3.2 4.6 4.6 0 0 0 0-.5ZM6.5 19A1.5 1.5 0 1 1 8 17.2a1.5 1.5 0 0 1 0 .3A1.5 1.5 0 0 1 6.5 19Zm9-2a1.5 1.5 0 1 1 1.5-1.8 1.5 1.5 0 0 1 0 .3 1.5 1.5 0 0 1-1.5 1.5Z" />
  </Icone>
);

/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/fill/svg/settings-2-outline.svg */
export const ConfiguracoesIcon = (props: any) => (
  <Icone {...props}>
    <Path d="M12.94 22h-1.89a1.68 1.68 0 0 1-1.68-1.68v-1.09a.34.34 0 0 0-.22-.29.38.38 0 0 0-.41 0l-.74.8a1.67 1.67 0 0 1-2.37 0L4.26 18.4a1.66 1.66 0 0 1-.5-1.19 1.72 1.72 0 0 1 .5-1.21l.74-.74a.34.34 0 0 0 0-.37c-.06-.15-.16-.26-.3-.26H3.68A1.69 1.69 0 0 1 2 12.94v-1.89a1.68 1.68 0 0 1 1.68-1.68h1.09a.34.34 0 0 0 .29-.22.38.38 0 0 0 0-.41L4.26 8a1.67 1.67 0 0 1 0-2.37L5.6 4.26a1.65 1.65 0 0 1 1.18-.5 1.72 1.72 0 0 1 1.22.5l.74.74a.34.34 0 0 0 .37 0c.15-.06.26-.16.26-.3V3.68A1.69 1.69 0 0 1 11.06 2H13a1.68 1.68 0 0 1 1.68 1.68v1.09a.34.34 0 0 0 .22.29.38.38 0 0 0 .41 0l.69-.8a1.67 1.67 0 0 1 2.37 0l1.37 1.34a1.67 1.67 0 0 1 .5 1.19 1.63 1.63 0 0 1-.5 1.21l-.74.74a.34.34 0 0 0 0 .37c.06.15.16.26.3.26h1.09A1.69 1.69 0 0 1 22 11.06V13a1.68 1.68 0 0 1-1.68 1.68h-1.09a.34.34 0 0 0-.29.22.34.34 0 0 0 0 .37l.77.77a1.67 1.67 0 0 1 0 2.37l-1.31 1.33a1.65 1.65 0 0 1-1.18.5 1.72 1.72 0 0 1-1.19-.5l-.77-.74a.34.34 0 0 0-.37 0c-.15.06-.26.16-.26.3v1.09A1.69 1.69 0 0 1 12.94 22Zm-1.57-2h1.26v-.77a2.33 2.33 0 0 1 1.46-2.14 2.36 2.36 0 0 1 2.59.47l.54.54.88-.88-.54-.55a2.34 2.34 0 0 1-.48-2.56 2.33 2.33 0 0 1 2.14-1.45H20v-1.29h-.77a2.33 2.33 0 0 1-2.14-1.46 2.36 2.36 0 0 1 .47-2.59l.54-.54-.88-.88-.55.54a2.39 2.39 0 0 1-4-1.67V4h-1.3v.77a2.33 2.33 0 0 1-1.46 2.14 2.36 2.36 0 0 1-2.59-.47l-.54-.54-.88.88.54.55a2.39 2.39 0 0 1-1.67 4H4v1.26h.77a2.33 2.33 0 0 1 2.14 1.46 2.36 2.36 0 0 1-.47 2.59l-.54.54.88.88.55-.54a2.39 2.39 0 0 1 4 1.67Z" />
    <Path d="M12 15.5a3.5 3.5 0 1 1 3.5-3.5 3.5 3.5 0 0 1-3.5 3.5Zm0-5a1.5 1.5 0 1 0 1.5 1.5 1.5 1.5 0 0 0-1.5-1.5Z" />
  </Icone>
);

/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/fill/svg/share-outline.svg */
export const CompartilharIcon = (props: any) => (
  <Icone {...props}>
    <Path d="M18 15a3 3 0 0 0-2.1.86L8 12.34v-.67l7.9-3.53A3 3 0 1 0 15 6v.34L7.1 9.86a3 3 0 1 0 0 4.28l7.9 3.53V18a3 3 0 1 0 3-3Zm0-10a1 1 0 1 1-1 1 1 1 0 0 1 1-1ZM5 13a1 1 0 1 1 1-1 1 1 0 0 1-1 1Zm13 6a1 1 0 1 1 1-1 1 1 0 0 1-1 1Z" />
  </Icone>
);

/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/fill/svg/alert-circle-outline.svg */
export const AlertaIcon = (props: any) => (
  <Icone {...props}>
    <Path d="M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2Zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8Z" />
    <Circle cx="12" cy="16" r="1" />
    <Path d="M12 7a1 1 0 0 0-1 1v5a1 1 0 0 0 2 0V8a1 1 0 0 0-1-1Z" />
  </Icone>
);

/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/outline/svg/heart.svg */
export const FavoritadaIcon = (props: any) => (
  <Icone {...props} fill="#ffa8b4">
    <Path d="M12 21a1 1 0 0 1-.7-.3L3.5 13a5.3 5.3 0 0 1 0-7.4 5.2 5.2 0 0 1 7.4 0l1.1 1 1-1a5.2 5.2 0 0 1 7.5 0 5.3 5.3 0 0 1 0 7.3l-7.8 7.8a1 1 0 0 1-.7.3Z" />
  </Icone>
);

/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/fill/svg/heart-outline.svg */
export const NaoFavoritadaIcon = (props: any) => (
  <Icone {...props}>
    <Path d="M12 21a1 1 0 0 1-.71-.29l-7.77-7.78a5.26 5.26 0 0 1 0-7.4 5.24 5.24 0 0 1 7.4 0L12 6.61l1.08-1.08a5.24 5.24 0 0 1 7.4 0 5.26 5.26 0 0 1 0 7.4l-7.77 7.78A1 1 0 0 1 12 21ZM7.22 6a3.2 3.2 0 0 0-2.28.94 3.24 3.24 0 0 0 0 4.57L12 18.58l7.06-7.07a3.24 3.24 0 0 0 0-4.57 3.32 3.32 0 0 0-4.56 0l-1.79 1.8a1 1 0 0 1-1.42 0L9.5 6.94A3.2 3.2 0 0 0 7.22 6Z" />
  </Icone>
);

/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/fill/svg/clock-outline.svg */
export const RecentesIcon = (props: any) => (
  <Icone {...props}>
    <Path d="M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2Zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8Z" />
    <Path d="M16 11h-3V8a1 1 0 0 0-2 0v4a1 1 0 0 0 1 1h4a1 1 0 0 0 0-2Z" />
  </Icone>
);
/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/fill/svg/download-outline.svg */
export const BaixarIcon = (props: any) => (
  <Icone {...props}>
    <Rect width="16" height="2" x="4" y="18" rx="1" ry="1" />
    <Rect width="4" height="2" x="3" y="17" rx="1" ry="1" transform="rotate(-90 5 18)" />
    <Rect width="4" height="2" x="17" y="17" rx="1" ry="1" transform="rotate(-90 19 18)" />
    <Path d="M12 15a1 1 0 0 1-.6-.2l-4-2.8a1 1 0 0 1-.2-1.4 1 1 0 0 1 1.4-.2l3.4 2.4 3.4-2.6a1 1 0 0 1 1.2 1.6l-4 3a1 1 0 0 1-.6.2Z" />
    <Path d="M12 13a1 1 0 0 1-1-1V4a1 1 0 0 1 2 0v8a1 1 0 0 1-1 1Z" />
  </Icone>
);
/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/fill/svg/arrow-circle-down-outline.svg */
export const AtualizarIcon = (props: any) => (
  <Icone {...props}>
    <Path d="M14.3 12.4 13 13.7V8a1 1 0 0 0-2 0v5.6l-1.3-1.3a1 1 0 0 0-1.4 1.4l3 3a1 1 0 0 0 .3.2 1 1 0 0 0 .8 0 .5.5 0 0 0 .1 0 .5.5 0 0 0 .2-.2l3-2.8a1 1 0 0 0-1.4-1.5Z" />
    <Path d="M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2Zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8Z" />
  </Icone>
);

/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/fill/svg/checkmark-circle-outline.svg */
export const AtualizadoIcon = (props: any) => (
  <Icone {...props}>
    <Path d="M9.7 11.3a1 1 0 0 0-1.4 1.4l3 3a1 1 0 0 0 .7.3 1 1 0 0 0 .7-.3l7-8a1 1 0 0 0-1.5-1.4L12 13.5Z" />
    <Path d="M21 11a1 1 0 0 0-1 1 8 8 0 0 1-8 8A8 8 0 0 1 6.3 6.4 8 8 0 0 1 12 4a8.8 8.8 0 0 1 1.9.2 1 1 0 1 0 .5-2A10.5 10.5 0 0 0 12 2a10 10 0 0 0-7 17 10 10 0 0 0 7 3 10 10 0 0 0 10-10 1 1 0 0 0-1-1Z" />
  </Icone>
);

/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/fill/svg/info-outline.svg */
export const InformacoesIcon = (props: any) => (
  <Icone {...props}>
    <Path d="M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2Zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8Z" />
    <Circle cx="12" cy="8" r="1" />
    <Path d="M12 10a1 1 0 0 0-1 1v5a1 1 0 0 0 2 0v-5a1 1 0 0 0-1-1Z" />
  </Icone>
);
/** https://github.com/akveo/eva-icons/blob/aa96e23/package/icons/fill/svg/trash-2-outline.svg */
export const ExcluirIcon = (props: any) => (
  <Icone {...props}>
    <Path d="M21 6h-5V4.33A2.42 2.42 0 0 0 13.5 2h-3A2.42 2.42 0 0 0 8 4.33V6H3a1 1 0 0 0 0 2h1v11a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V8h1a1 1 0 0 0 0-2ZM10 4.33c0-.16.21-.33.5-.33h3c.29 0 .5.17.5.33V6h-4ZM18 19a1 1 0 0 1-1 1H7a1 1 0 0 1-1-1V8h12Z" />
    <Path d="M9 17a1 1 0 0 0 1-1v-4a1 1 0 0 0-2 0v4a1 1 0 0 0 1 1Zm6 0a1 1 0 0 0 1-1v-4a1 1 0 0 0-2 0v4a1 1 0 0 0 1 1Z" />
  </Icone>
);
/** https://github.com/akveo/eva-icons/blob/aa96e23cdcea4e2197fb3cf1288184c7fc4cb94a/package/icons/outline/svg/book-open-outline.svg */
export const LivroAbertoIcon = (props: any) => (
  <Icone {...props}>
    <Path d="M20.6 4.2a1 1 0 0 0-.8-.2L12 5.8 4.2 4A1 1 0 0 0 3 5v12.2a1 1 0 0 0 .8 1l8 1.8h.4l8-1.8a1 1 0 0 0 .8-1V5a1 1 0 0 0-.4-.8ZM5 6.2l6 1.4v10.2l-6-1.4Zm14 10.2-6 1.4V7.6l6-1.4Z" />
  </Icone>
);
