import {Button, ButtonGroup} from '@ui-kitten/components';
import React from 'react';

export interface ItemGrupoBotoes<T extends string> {
  nome: string;
  valor: T;
}

interface GrupoBotoesProps<T extends string> {
  atual: T;
  itens: ItemGrupoBotoes<T>[];
  onChange: (valor: T) => void;
}

export const GrupoBotoes = <T extends string>({itens, onChange, atual}: GrupoBotoesProps<T>) => {
  const botoes = itens.map(it => (
    <Button key={it.valor} disabled={atual === it.valor} onPress={() => onChange(it.valor)}>
      {it.nome}
    </Button>
  ));

  return (
    <ButtonGroup status="basic" size="small">
      {botoes}
    </ButtonGroup>
  );
};
