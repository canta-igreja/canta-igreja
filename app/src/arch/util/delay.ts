export const delay = (action: () => any, milliseconds = 25) => setTimeout(() => action(), milliseconds);

export const promiseDelay = <T>(promise: Promise<T>, milliseconds = 25) =>
  new Promise(resolve => setTimeout(resolve, milliseconds)).then(() => promise);

export const wait = (milliseconds = 25) => new Promise(resolve => setTimeout(resolve, milliseconds));
