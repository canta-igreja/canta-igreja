import React from 'react';
import {DEVELOPMENT} from './development';

// if (DEVELOPMENT) {
if (false) {
  const whyDidYouRender = require('@welldone-software/why-did-you-render');
  const ReactRedux = require('react-redux');

  whyDidYouRender(React, {
    // trackAllPureComponents: false,
    trackAllPureComponents: true,
    trackExtraHooks: [[ReactRedux, 'useSelector']],
    onlyLogs: false,
  });
}
