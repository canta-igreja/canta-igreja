import SimpleToast from 'react-native-simple-toast';

export class Toast {
  private constructor() {}
  static show(message: string) {
    SimpleToast.show(message, SimpleToast.SHORT);
  }
}
