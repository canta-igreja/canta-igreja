import {exists, copyFileAssets} from '@dr.pogodin/react-native-fs';

export async function copyAssetFileIfNecessary(origin: string, target: string) {
  if (!(await exists(target))) {
    await copyFileAssets(origin, target);
  }
}
