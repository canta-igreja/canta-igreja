import {Linking, Alert} from 'react-native';

export async function openLink(url: string) {
  const supported = await Linking.canOpenURL(url);

  if (supported) {
    await Linking.openURL(url);
  } else {
    Alert.alert(`Não foi possível abrir o link: ${url}`);
  }
}

/**
 * https://stackoverflow.com/a/111545
 */
export function encodeQueryData(data: {[key: string]: string}) {
  const ret: string[] = [];
  for (let d in data) {
    ret.push(`${encodeURIComponent(d)}=${encodeURIComponent(data[d])}`);
  }

  return ret.join('&');
}
