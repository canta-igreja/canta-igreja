import {FadeIn} from 'react-native-reanimated';

export class Animation {
  private constructor() {}

  static readonly FadeIn = FadeIn.duration(125);
}
