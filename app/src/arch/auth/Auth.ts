import {authorize, refresh, revoke} from 'react-native-app-auth';
import {DEVELOPMENT} from '../util/development';
import {AuthRepository, Credenciais} from './AuthRepository';
import {Api} from 'src/util/Api';
import {UsuarioLogadoRepository} from 'src/repository/UsuarioLogadoRepository';
import * as Sentry from '@sentry/react-native';
import {Toast} from '../util/Toast';

const GOOGLE_OAUTH_APP_GUID = DEVELOPMENT
  ? '1065907842958-nsjjc70td56vaan40kr754qfcp066ui2'
  : '1065907842958-7fmr0qkqmsjn2g3me74fojno9ndob0gf';

const config = {
  issuer: 'https://accounts.google.com',
  clientId: `${GOOGLE_OAUTH_APP_GUID}.apps.googleusercontent.com`,
  redirectUrl: `com.googleusercontent.apps.${GOOGLE_OAUTH_APP_GUID}:/oauth2redirect/google`,
  scopes: ['openid', 'profile', 'email'],
};

export class Auth {
  private readonly repository: AuthRepository;

  constructor() {
    this.repository = new AuthRepository();
  }

  async isAutenticado() {
    return this.repository.hasCredenciais();
  }

  async autenticar() {
    // Descomente para simular login quando quiser
    // await this.deslogar();

    try {
      let {credenciais} = await this.repository.getCredenciais();

      if (credenciais === undefined) {
        const authState = await authorize(config);
        credenciais = await this.repository.asCredenciais(authState);

        const {data: conta, error} = await Api.auth(credenciais);
        if (error || conta === undefined) {
          throw new Error(JSON.stringify(error));
        }

        credenciais = await this.repository.setCredenciais(conta, authState);
        const usuarioLogado = UsuarioLogadoRepository.setUsuarioLogado(credenciais, conta);
      } else if (!this.isAtiva(credenciais)) {
        credenciais = await this.repository.setCredenciaisRefreshed(await this.refresh(credenciais));
      }

      return credenciais.idToken;
    } catch (e) {
      Toast.show('Não foi possível compartilhar repertório. Você está com internet?');
      Sentry.captureException(e);
      console.error(e);
    }
  }

  private isAtiva(credenciais: Credenciais) {
    const agora = new Date().getTime();
    const dataExpiracao = new Date(credenciais.accessTokenExpirationDate).getTime();
    const umMinuto = 60 * 1000;

    return dataExpiracao - umMinuto > agora;
  }

  private async refresh(credenciais: Credenciais) {
    const refreshedState = await refresh(config, {
      refreshToken: credenciais.refreshToken,
    });

    return refreshedState;
  }

  public async deslogar() {
    const {credenciais} = await this.repository.getCredenciais();

    if (credenciais) {
      try {
        await revoke(config, {
          tokenToRevoke: credenciais.refreshToken,
        });
      } catch {}
    }

    this.repository.logout();
    UsuarioLogadoRepository.logout();
  }
}
