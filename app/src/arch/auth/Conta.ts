export interface Conta {
  slug: string;
  email: string;
  nome: string;
  subject: string;
  provider: string;
}
