import {AuthorizeResult, RefreshResult} from 'react-native-app-auth';
import {getGenericPassword, resetGenericPassword, setGenericPassword} from 'react-native-keychain';
import {Conta} from './Conta';

export interface SlugCredenciais {
  slug?: string;
  credenciais?: Credenciais;
}

export interface Credenciais {
  accessToken: string;
  idToken: string;
  refreshToken: string;

  accessTokenExpirationDate: string;
}

export class AuthRepository {
  async hasCredenciais(): Promise<boolean> {
    return (await getGenericPassword()) !== false;
  }

  async getCredenciais(): Promise<SlugCredenciais> {
    const credenciais = await getGenericPassword();

    if (credenciais === false) {
      return {};
    }
    const {username, password} = credenciais;

    return {
      slug: username,
      credenciais: JSON.parse(password) as Credenciais,
    };
  }

  async setCredenciais(user: Conta, authState: AuthorizeResult) {
    return this.setGenericPassword(user.slug, authState);
  }

  async setCredenciaisRefreshed(authState: RefreshResult) {
    const {slug} = await this.getCredenciais();

    return this.setGenericPassword(slug!, authState);
  }

  private async setGenericPassword(slug: string, authState: RefreshResult | AuthorizeResult) {
    const credenciais = await this.asCredenciais(authState);

    await setGenericPassword(slug!, JSON.stringify(credenciais));

    return credenciais;
  }

  public async asCredenciais(authState: AuthorizeResult | RefreshResult) {
    const {credenciais: credenciaisSalvas} = await this.getCredenciais();

    return {
      accessToken: authState.accessToken,
      refreshToken: authState.refreshToken || credenciaisSalvas?.refreshToken!,
      idToken: authState.idToken,
      accessTokenExpirationDate: authState.accessTokenExpirationDate,
    };
  }

  async logout() {
    await resetGenericPassword();
  }
}
