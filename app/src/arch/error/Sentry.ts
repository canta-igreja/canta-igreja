import * as Sentry from '@sentry/react-native';
import {DEVELOPMENT} from '../util/development';

let init = false;

export const routingInstrumentation = new Sentry.ReactNavigationInstrumentation({
  enableTimeToInitialDisplay: true,
});

export const inicializarSentry = () => {
  if (init) {
    return;
  }
  init = true;

  const sampleRate = DEVELOPMENT ? 1.0 : 0.01;

  Sentry.init({
    dsn: 'https://150b6c476d882df850e503813abc1054@o4507574049570816.ingest.us.sentry.io/4507612720726016',

    environment: DEVELOPMENT ? 'local' : 'production',
    integrations: [new Sentry.ReactNativeTracing({routingInstrumentation})],

    tracesSampleRate: sampleRate,
    _experiments: {
      // profilesSampleRate is relative to tracesSampleRate.
      // Here, we'll capture profiles for 100% of transactions.
      profilesSampleRate: sampleRate,
    },

    // uncomment the line below to enable Spotlight (https://spotlightjs.com)
    enableSpotlight: DEVELOPMENT,
  });
};
