export class QueryOperation {
  static optionalValue<T>(value: T): T | undefined {
    return value === null ? undefined : value;
  }
}
