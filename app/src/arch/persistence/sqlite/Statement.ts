import {PreparedStatementObj} from '@op-engineering/op-sqlite';
import {Banco} from '../Banco';

export class PreparedStatement {
  private instance: PreparedStatementObj | undefined;

  constructor(readonly query: string) {}

  private async statement() {
    if (this.instance === undefined) {
      this.instance = await Banco.prepareStatement(this.query);
    }

    return this.instance;
  }

  async bind(params: any[]) {
    (await this.statement()).bind(params);
  }

  async execute() {
    return (await this.statement()).execute();
  }
}
