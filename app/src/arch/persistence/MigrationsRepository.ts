import {Banco} from './Banco';

export class MigrationsRepository {
  private static readonly migrations: [number, {default: string; migration?: () => Promise<void>}][] = [
    //
    [8, require('./migration/v8_repertorio.sql.js')],
    [9, require('./migration/v9_musica_informacoes.sql.js')],
    [10, require('./migration/v10_slug_repertorio.sql.ts')],
  ];

  static async runMigrations() {
    let currentVersion = await MigrationsRepository.findCurrentVersion();

    for (const migration of this.migrations) {
      const [version, file] = migration;

      if (currentVersion < version) {
        currentVersion = await MigrationsRepository.migrate(version, file.default, file.migration);
      }
    }

    return currentVersion;
  }

  static async findCurrentVersion(): Promise<number> {
    const currentVersion = 'PRAGMA user_version;';
    const result = await Banco.executeAsync(currentVersion);
    return result.rows?._array[0].user_version ?? 0;
  }

  private static async migrate(version: number, fileContent: string, migration?: () => Promise<void>): Promise<number> {
    await Banco.executeAsync(fileContent);
    if (migration) {
      await migration();
    }
    await MigrationsRepository.updateVersion(version);

    return version;
  }

  private static async updateVersion(newVersion: number) {
    const currentVersion = `PRAGMA user_version = ${newVersion};`;
    return await Banco.executeAsync(currentVersion);
  }
}
