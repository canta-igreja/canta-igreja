export default /*sql*/ `

ALTER TABLE repertorio ADD COLUMN slug TEXT DEFAULT NULL;
ALTER TABLE repertorio ADD COLUMN data_edicao INTEGER;

UPDATE repertorio
   SET data_edicao=unixepoch()
 WHERE data_edicao is null;

`;

export async function migration() {}
