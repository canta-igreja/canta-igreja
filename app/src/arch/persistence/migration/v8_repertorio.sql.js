export default /*sql*/ `
-----------------
-- Repertório - Schema
-----------------
CREATE TABLE IF NOT exists tipo_repertorio (
    id_tipo_repertorio INTEGER PRIMARY KEY AUTOINCREMENT,
    tipo TEXT not null,
    descricao TEXT not null
);

CREATE TABLE IF NOT exists repertorio (
    id_repertorio INTEGER PRIMARY KEY AUTOINCREMENT,
    id_tipo_repertorio INTEGER not null CONSTRAINT id_tipo_repertorio REFERENCES tipo_repertorio(id_tipo_repertorio) default 2,
    titulo TEXT not null CHECK (length(titulo) <= 50),

    resumo TEXT not null default 'Nenhuma música adicionada',
    ordem_musicas TEXT not null default '[]',
    
    data_criacao TIMESTAMP not null default CURRENT_TIMESTAMP,
    data_ultima_visualizacao TIMESTAMP default null,
    
    data_exclusao TIMESTAMP default null  -- Repertórios ativas não possuem data de exclusão
);

CREATE TABLE IF NOT exists item_repertorio (
    id_item_repertorio INTEGER PRIMARY KEY AUTOINCREMENT,

    id_repertorio INTEGER not null CONSTRAINT id_repertorio REFERENCES repertorio(id_repertorio),
    id_musica INTEGER CONSTRAINT id_musica REFERENCES musica(id_musica),
    -- Possivelmente título de uma música não existente no catálogo ou algo como "Salmo"
    termo TEXT null CHECK (length(termo) <= 300),

    momento TEXT null CHECK (length(momento) <= 300),

    tonalidade TEXT default null,

    data_inclusao TIMESTAMP not null default CURRENT_TIMESTAMP
);


-----------------
-- Repertorios - Data
-----------------
INSERT INTO tipo_repertorio (id_tipo_repertorio, tipo, descricao) values
    (1, 'Padrão', 'Repertório padrão do sistema'),
    (2, 'Usuário', 'Repertório criada por um usuário')
;

INSERT INTO repertorio (id_repertorio, id_tipo_repertorio, titulo) values
    (1, 1, 'Músicas favoritas'),
    (2, 1, 'Visto recentemente')
;

-- Atualizar sequence
UPDATE SQLITE_SEQUENCE SET seq = 2 WHERE name = 'repertorio';
`;
