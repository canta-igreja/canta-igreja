import {
  BatchQueryResult,
  DB,
  IOS_LIBRARY_PATH,
  PreparedStatementObj,
  QueryResult,
  SQLBatchTuple,
  moveAssetsDatabase,
  open,
} from '@op-engineering/op-sqlite';

import {Platform} from 'react-native';
import {MigrationsRepository} from './MigrationsRepository';

export class Banco {
  private static databaseName = 'cantaIgreja.db';
  private static database: DB | undefined = undefined;

  private static async instance() {
    if (Banco.database === undefined) {
      Banco.database = await this.initialize();
    }

    return Banco.database;
  }

  private static async initialize() {
    // const path = Platform.OS === 'ios' ? IOS_DOCUMENT_PATH : undefined;
    const path = Platform.OS === 'ios' ? IOS_LIBRARY_PATH : undefined;

    await moveAssetsDatabase({
      filename: Banco.databaseName,
      path: path,
      overwrite: false,
    });

    const openDatabaseParams: any = {
      name: Banco.databaseName,
    };
    // opsqlite dá erro se eu incluir path como undefined no Android
    if (Platform.OS === 'ios') {
      openDatabaseParams.path = path;
    }
    Banco.database = open(openDatabaseParams);

    await MigrationsRepository.runMigrations();
    return Banco.database;
  }

  static async attach(dbFileNameToAttach: string, alias: string, path: string) {
    const instancia = await Banco.instance();
    instancia.attach(Banco.databaseName, dbFileNameToAttach, alias, path);
  }

  static async detach(alias: string) {
    const instancia = await Banco.instance();
    instancia.detach(Banco.databaseName, alias);
  }

  static async executeAsync(query: string, params?: any[]): Promise<QueryResult> {
    const instance = await Banco.instance();
    return await instance.executeAsync(query, params);
  }

  static async executeBatchAsync(commands: SQLBatchTuple[]): Promise<BatchQueryResult> {
    const instance = await Banco.instance();
    return await instance.executeBatchAsync(commands);
  }

  static async prepareStatement(query: string): Promise<PreparedStatementObj> {
    const instance = await Banco.instance();
    return instance.prepareStatement(query);
  }
}
