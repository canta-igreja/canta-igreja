/**
 * Eva colors
 * https://akveo.github.io/react-native-ui-kitten/docs/design-system/eva-light-theme#background-basic-color-1
 */
const general = {
  color: {
    basic: {
      // Light
      100: '#FFFFFF',
      200: '#F7F9FC',
      300: '#EDF1F7',
      400: '#E4E9F2',
      500: '#C5CEE0',
      // Dark
      600: '#8F9BB3',
      700: '#2E3A59',
      800: '#222B45',
      900: '#1A2138',
      1000: '#151A30',
      1100: '#101426',
    },
  },
};

export const lightTheme = {
  color: {
    typography: general.color.basic[800],

    /**
     * https://akveo.github.io/react-native-ui-kitten/docs/design-system/eva-light-theme#background-basic-color-1
     */
    background: {
      level_1: general.color.basic[100],
      level_2: general.color.basic[200],
      level_3: general.color.basic[300],
      level_4: general.color.basic[400],
    },
  },
  spacing: {
    s2: 2,
    s4: 4,
    s8: 8,
    s16: 16,
  },

  border: {
    radius: {
      sm: 8,
      md: 16,
    },
    color: general.color.basic[400],
    width: {
      sm: 1,
      md: 2,
    },
  },
} as const;
