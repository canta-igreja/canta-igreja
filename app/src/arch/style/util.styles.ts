import {UnistylesMiniRuntime} from 'react-native-unistyles/lib/typescript/src/core';

export const UtilStyles = {
  safeAreaViewInnerPadding: (
    runtime: UnistylesMiniRuntime,
    apply: {top?: boolean; bottom?: boolean; left?: boolean; right?: boolean} = {},
  ) => {
    return {
      paddingLeft: apply?.left === false ? 0 : runtime.insets.left,
      paddingRight: apply?.right === false ? 0 : runtime.insets.right,
      paddingTop: apply?.top === false ? 0 : runtime.insets.top,
      paddingBottom: apply?.bottom === false ? 0 : runtime.insets.bottom,
    };
  },
};
