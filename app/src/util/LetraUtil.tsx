import {TextProps} from '@ui-kitten/components';
import {Negrito} from 'src/view/base/Texto';

export class LetraUtil {
  static limparLetra(letra: string) {
    return letra
      .replace(/(_|\*|\\)/gi, '')
      .replace(/(\s+)/gi, ' ')
      .trim();
  }

  static marcarTermoBuscado(letra: string, termoBuscado?: string) {
    if (termoBuscado === undefined || termoBuscado.trim() === '') {
      return letra;
    }

    const parts = letra.split('||');
    return (props: TextProps | undefined) => {
      const content = parts.map((it, index) =>
        index % 2 === 1 ? <Negrito style={props?.style}>{it}</Negrito> : <>{it}</>,
      );

      return <>{content}</>;
    };
  }
}
