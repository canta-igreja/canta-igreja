import React, {useLayoutEffect} from 'react';
import {Keyboard} from 'react-native';

/**
 * https://github.com/akveo/react-native-ui-kitten/issues/1287#issuecomment-796173317
 */
export const useKeyboardSize = () => {
  const [keyboardSize, setKeyboardSize] = React.useState(0);

  useLayoutEffect(() => {
    const keyboardDidShowEventSubscription = Keyboard.addListener('keyboardDidShow', e => {
      setKeyboardSize(e.endCoordinates.height);
    });

    const keyboardDidHideEventSubscription = Keyboard.addListener('keyboardDidHide', e => {
      setKeyboardSize(e.endCoordinates.height);
    });

    return () => {
      keyboardDidShowEventSubscription.remove();
      keyboardDidHideEventSubscription.remove();
    };
  }, []);

  return keyboardSize;
};
