export class Timestamp {
  private constructor() {}

  /**
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/now
   *
   * @returns Número de segundos desde "midnight at the beginning of January 1, 1970, UTC."
   */
  static now() {
    return Math.floor(Date.now() / 1000);
  }

  static fromDateString(date: string): number {
    return Math.floor(new Date(date).getTime() / 1000);
  }
}
