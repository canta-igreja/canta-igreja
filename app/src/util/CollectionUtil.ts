/**
 * Removes "", 0, NaN, null, undefined, and false.
 *
 * https://stackoverflow.com/a/281335
 */
export function filterFalsy(list: any[]) {
  return list.filter(Boolean);
}
