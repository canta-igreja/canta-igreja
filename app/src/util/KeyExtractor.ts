import {AutoCompleteOption} from 'src/view/fragment/AutoCompleteFragment/AutoCompleteFragment.type';

export class KeyExtractor {
  static fromNumber(it: number, index: number) {
    return `${it}`;
  }

  static fromAutoCompleteOption(it: AutoCompleteOption, index: number) {
    return `${it.title}`;
  }
}
