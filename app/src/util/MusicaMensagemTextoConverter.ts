import {InformacoesDetalhadasMusica} from 'src/model/Musica';

export class MusicaMensagemTextoConverter {
  static convert(musica: InformacoesDetalhadasMusica): string {
    let letra = musica.letra.replaceAll('**', '*');

    return `https://canta.app/musicas/${musica.id_musica}

*${musica.titulo}*

${letra}

Acesse essa e outras músicas no Aplicativo Canta Igreja: https://canta.app`;
  }
}
