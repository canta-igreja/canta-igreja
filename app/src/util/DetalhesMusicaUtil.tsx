import {AutocompleteItem} from '@ui-kitten/components';

export class DetalhesMusicaUtil {
  static momentos = [
    'Refrão Meditativo',
    'Entrada',
    'Saudação a Trindade',
    'Ato Penintencial',
    'Aspersão',
    'Glória',
    'Salmo',
    'Aclamação',
    'Oferendas',
    'Partilha',
    'Santo',
    'Aleluia',
    'Paz',
    'Cordeiro',
    'Comunhão',
    'Pós comunhão',
    'Final',
    'Mantra',
  ];

  static momentosAsAutoCompleteItems(recomendacao?: string) {
    const opcoes = DetalhesMusicaUtil.momentos.map(it => ({label: it, recomendado: false}));

    return recomendacao === undefined //
      ? opcoes
      : [{label: recomendacao, recomendado: true}, ...opcoes.filter(it => it.label !== recomendacao)];
  }

  static tonalidades = [
    'C',
    'C#',
    'D',
    'Eb',
    'E',
    'Fb',
    'F',
    'F#',
    'Gb',
    'G',
    'G#',
    'Ab',
    'A',
    'Bb',
    'B',

    'Cm',
    'C#m',
    'Dm',
    'Ebm',
    'Em',
    'Fbm',
    'Fm',
    'F#m',
    'Gbm',
    'Gm',
    'G#m',
    'Abm',
    'Am',
    'Bbm',
    'Bm',
  ];

  static filter(query: string, all: string[]) {
    return all.filter(item => item.toLowerCase().includes(query.toLowerCase()));
  }

  static filterLabelled<T extends {title: string}>(query: string, all: T[]) {
    return all.filter(item => item.title.toLowerCase().includes(query.toLowerCase()));
  }

  static renderAsAutoCompleteItem(content: string) {
    return <AutocompleteItem key={content} title={content} />;
  }
}
