import notifee from '@notifee/react-native';
import {Platform} from 'react-native';
import {Toast} from 'src/arch/util/Toast';

export class Notification {
  static async startProgress(identifier: string, title: string, parts: number) {
    const progress = new NotificationProgress(identifier, parts);

    await progress.start(title);
    return progress;
  }
}

export class NotificationProgress {
  private current: number = 0;
  private readonly channelId: string = 'atualizar/obras';

  constructor(
    private identifier: string,
    private parts: number,
  ) {}

  async start(title: string) {
    // await notifee.deleteChannel(this.channelId);
    await notifee.createChannel({
      id: this.channelId,
      name: 'Atualização da letra das músicas',
      lights: false,
      sound: undefined,
      vibration: false,
      vibrationPattern: [],
    });

    if (Platform.OS !== 'ios') {
      await notifee.displayNotification({
        id: this.identifier,
        title: title,
        android: {
          autoCancel: false,
          channelId: this.channelId,
          // ongoing: true,
          progress: {
            max: this.parts,
            current: this.current,
            indeterminate: true,
          },
          onlyAlertOnce: true,
        },
      });
    }
  }

  async next(title: string) {
    this.current += 1;

    if (Platform.OS === 'ios') {
      Toast.show(title);
    } else {
      await notifee.displayNotification({
        id: this.identifier,
        title: title,
        android: {
          autoCancel: false,
          channelId: this.channelId,
          // ongoing: true,
          progress: {
            max: this.parts,
            current: this.current,
            // indeterminate: true,
          },
          onlyAlertOnce: true,
        },
      });
    }
  }

  async finish(title: string) {
    await notifee.cancelNotification(this.identifier);
    Toast.show(title);
  }
}
