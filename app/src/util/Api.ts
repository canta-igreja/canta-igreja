import {Credenciais} from 'src/arch/auth/AuthRepository';
import {Conta} from 'src/arch/auth/Conta';
import {DEVELOPMENT} from 'src/arch/util/development';
import {MusicaResponse} from 'src/model/api/MusicaResponse';
import {
  PersistirRepertorioRequest,
  RepertorioImportavelDTO,
  RepertorioPersistidoResponse,
} from 'src/model/api/RepertorioRest';
import {Obra} from 'src/model/obra/Obra';
import {ItemRepertorio, ReferenciaRepertorioSlug, Repertorio} from 'src/model/repertorio/Repertorio';
import {UsuarioLogado} from 'src/repository/UsuarioLogadoRepository';
import {BatchFetch} from './BatchFetch';
import {stripNulls} from './stripNulls';

interface ResponseError {
  status: number;
  headers: Headers;
  body: string;
}

export type Response<T> = {
  error?: ResponseError;
  data?: T;
};

export class Api {
  static readonly URL = {
    landingPage: 'https://canta.app',
    app: DEVELOPMENT ? 'https://api-preview.canta.app/v1' : 'https://api.canta.app/v1',
  };

  static readonly APP_MUSICA = (id: number) => `${Api.URL.app}/musicas/${id}`;

  static readonly APP_BIBLIOTECA = `${Api.URL.app}/biblioteca`;
  static readonly APP_BIBLIOTECA_OBRAS_SQLITE = (obra: Obra) =>
    `${Api.URL.app}/biblioteca/obras/${obra.id_obra}/sqlite`;

  private static readonly APP_AUTH = `${Api.URL.app}/auth`;
  private static readonly APP_REPERTORIOS = `${Api.URL.app}/repertorios`;
  private static readonly APP_REPERTORIOS_GET = (referencia: ReferenciaRepertorioSlug) =>
    `${Api.URL.app}/repertorios/${referencia.conta}/${referencia.repertorio}`;

  private static async request<T>(
    method: 'GET' | 'POST' | 'PUT',
    url: string,
    params: {token?: string; body?: object} = {},
    timeout: number = 20,
  ): Promise<Response<T>> {
    const headers: any = {};
    let body: any | undefined;

    if (params.token) {
      headers.Authorization = `Bearer ${params.token}`;
    }
    if (params.body) {
      headers['Content-Type'] = 'application/json';
      body = JSON.stringify(params.body);
    }

    let controller = new AbortController();
    const timeoutId = setTimeout(() => controller.abort(), timeout * 1000);

    const response = await fetch(url, {
      method,
      headers,
      body,
      signal: controller.signal,
    });

    clearTimeout(timeoutId);

    if (response.status >= 400) {
      const error: ResponseError = {
        status: response.status,
        headers: response.headers,
        body: await response.text(),
      };
      return {error};
    }

    return {data: stripNulls(await response.json()) as T};
  }

  static async auth(credenciais: Credenciais) {
    return await this.request<Conta>('POST', Api.APP_AUTH, {token: credenciais.idToken});
  }

  static async compartilharRepertorio(
    usuario: UsuarioLogado,
    repertorio: Repertorio,
    itens: ItemRepertorio[],
    token: string,
  ) {
    const repertorioParaCompartilhar: PersistirRepertorioRequest = {
      titulo: repertorio.titulo,
      itens: itens.map(it => ({
        id_musica: it.musica?.id_musica,
        termo: it.termo,
        momento: it.momento.valor,
        tonalidade: it.tonalidade.valor,
      })),
    };

    let endpoint = `${Api.APP_REPERTORIOS}/${usuario.slug}`;
    let method: 'POST' | 'PUT' = 'POST';

    if (repertorio.slug && repertorio.slug.corrente.conta === usuario.slug) {
      endpoint = `${endpoint}/${repertorio.slug.corrente.repertorio}`;
      method = 'PUT';
    }

    return await this.request<RepertorioPersistidoResponse>(method, endpoint, {
      token,
      body: repertorioParaCompartilhar,
    });
  }

  static async getRepertorio(referencia: ReferenciaRepertorioSlug): Promise<Response<RepertorioImportavelDTO>> {
    const {error, data: repertorioCompartilhado} = await this.request<RepertorioPersistidoResponse>(
      'GET',
      Api.APP_REPERTORIOS_GET(referencia),
    );

    if (error || repertorioCompartilhado === undefined) {
      return {error};
    }

    const repertorio: RepertorioImportavelDTO = {
      repertorio: repertorioCompartilhado,
      musicas: await Api.getMusicasRepertorio(repertorioCompartilhado),
    };

    return {data: repertorio};
  }

  private static async getMusicasRepertorio(repertorio: RepertorioPersistidoResponse): Promise<MusicaResponse[]> {
    const urls = repertorio.itens //
      .filter(it => it.id_musica)
      .map(item => Api.APP_MUSICA(item.id_musica!));

    return await BatchFetch.fetch(urls, 5);
  }
}
