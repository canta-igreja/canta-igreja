// https://github.com/ai/nanoid?tab=readme-ov-file#react-native
import 'react-native-get-random-values';
import {nanoid} from 'nanoid';

export class NanoId {
  static generate() {
    return nanoid(10);
  }
}
