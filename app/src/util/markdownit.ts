import {MarkdownIt} from 'react-native-markdown-display';

export class MarkdownItConfiguration {
  private static readonly parserConfiguration = {
    html: false,
    linkify: false,
    typographer: false,
  };

  private static readonly nonRelevant = [
    // Texto
    //'balance_pairs', 'emphasis', // Negrito e itálico
    //'list'
    'heading',
    'lheading',
    'table',
    'fence',
    'code',
    'html_block',
    'html_inline',
    'hr',
    'reference',
    'blockquote',
    'escape',
    'backticks',
    'link',
    'image',
    'autolink',
    'strikethrough',
    'text_collapse',
    'backticks',
    // 'hardbreak', 'softbreak'

    'normalize',
    'linkify',
    'replacements',
    'smartquotes',
  ];

  static readonly lyrics = MarkdownIt(MarkdownItConfiguration.parserConfiguration).disable([
    ...MarkdownItConfiguration.nonRelevant,
  ]);
}
