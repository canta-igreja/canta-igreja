import {MusicaItemListData} from 'src/view/component/list/data/MusicaItemListData';
import {LetraUtil} from './LetraUtil';

export class ListUtil {
  static reposicionarElemento<Elemento>(lista: Elemento[], posicaoAtual: number, posicaoNova: number) {
    const copia = [...lista];

    const elemento = copia.splice(posicaoAtual, 1)[0];
    copia.splice(posicaoNova, 0, elemento);

    return copia;
  }

  static removerElemento<Elemento>(lista: Elemento[], posicao: number): Elemento[] {
    const copia = [...lista];
    copia.splice(posicao, 1);

    return copia;
  }

  static getConteudoItemList(item: MusicaItemListData, termoBuscado: string | undefined) {
    const inicioLetraLimpa = LetraUtil.limparLetra(item.trecho_letra);

    const sigla = item.livro?.sigla ? ` ${item.livro.sigla}` : '';
    return {
      livro: item.livro?.titulo,
      indice: item.livro?.indice ? `${item.livro.indice}${sigla} - ` : '',
      titulo: LetraUtil.marcarTermoBuscado(item.titulo, termoBuscado),
      trechoLetra: LetraUtil.marcarTermoBuscado(inicioLetraLimpa, termoBuscado),
    };
  }

  static toMap<T, Value extends number>(itens: T[], keyMap: (item: T) => Value): {[key: number]: T} {
    return itens.reduce((obj, item) => ({...obj, [keyMap(item)]: item}), {});
  }
}
