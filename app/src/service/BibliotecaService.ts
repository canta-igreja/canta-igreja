import {openLink} from 'src/arch/util/linking';

export enum BibliotecaLink {
  LITURIA_DIARIA = 'https://liturgiadiaria.cnbb.org.br/',
  SUGESTOES_REPERTORIO = 'https://drive.google.com/drive/folders/1BrCJskeBmrjRb_XgHsShBwnvFYGap8Gs',
}

export class BibliotecaService {
  async abrirLink(link: BibliotecaLink) {
    await openLink(link);
  }
}
