import {RepertorioQuery, SimilaridadeRepertorios} from 'src/model/api/RepertorioRest';
import {ItemRepertorio, ReferenciaRepertorioSlug, Repertorio} from 'src/model/repertorio/Repertorio';
import {IndicesLivroByMusicaId, MusicasRepository} from 'src/repository/musicas/MusicasRepository';
import {RepertoriosRepository} from 'src/repository/RepertoriosRepository';
import {Api} from 'src/util/Api';

type HasRepertorioSimilarResponse =
  | {repertorio: undefined; similaridade: SimilaridadeRepertorios.NAO_EXISTE_CORRESPONDENTE}
  | {
      repertorio: Repertorio;
      similaridade:
        | SimilaridadeRepertorios.CORRESPONDENTE_IGUAL
        | SimilaridadeRepertorios.CORRESPONDENTE_COM_VERSAO_DISTINTA;
    };

export class RepertorioService {
  async gerarMensagemCompartilhamento(repertorio: Repertorio, itens: ItemRepertorio[]) {
    const indicesLivro = await MusicasRepository.getIndicesLivrosFromRepertorio(repertorio);
    return `*Repertorio:* ${repertorio.titulo}
${this.toUrl(repertorio)}

${itens.map(it => this.toMarkdown(it, indicesLivro)).join('\n')}`;
  }

  private toMarkdown(item: ItemRepertorio, indicesLivro: IndicesLivroByMusicaId): string {
    const tonalidade = item.tonalidade.valor //
      ? `\`\`\`${item.tonalidade.valor.trim()}\`\`\``
      : '';
    const momento = item.momento.valor //
      ? `*${item.momento.valor.trim()}*`
      : '';

    const separadorTonalidadeMomento =
      item.tonalidade.valor && item.momento.valor //
        ? ' '
        : '';
    const separador =
      item.tonalidade.valor || item.momento.valor //
        ? ': '
        : '';
    const titulo =
      item.musica?.titulo.trim() ?? //
      item.termo?.trim() ??
      '';

    const livros =
      item.musica && indicesLivro[item.musica.id_musica] !== undefined //
        ? `\n   ${indicesLivro[item.musica.id_musica]?.map(it => `${it.sigla ?? it.titulo} ${it.indice}`).join(', ')}`
        : '';

    return ` - ${tonalidade}${separadorTonalidadeMomento}${momento}${separador}${titulo}${livros}`;
  }

  private toUrl(repertorio: Repertorio): string {
    return `${Api.URL.landingPage}/repertorios/?${repertorio.slug?.corrente.conta}:${repertorio.slug?.corrente.repertorio}`;
  }

  async findRepertorioSimilar(referencia: ReferenciaRepertorioSlug): Promise<HasRepertorioSimilarResponse> {
    const repertorioBanco = (await RepertoriosRepository.findRepertorios({slug: referencia.repertorio})).shift();

    if (repertorioBanco === undefined) {
      return {repertorio: undefined, similaridade: SimilaridadeRepertorios.NAO_EXISTE_CORRESPONDENTE};
    }

    return {
      repertorio: repertorioBanco,
      similaridade: SimilaridadeRepertorios.CORRESPONDENTE_COM_VERSAO_DISTINTA,
    };
  }

  convertToQuery(repertorio: Repertorio, itens: ItemRepertorio[]): RepertorioQuery {
    return {
      titulo: repertorio.titulo,

      itens: itens
        .map(it => ({id: it.musica?.id_musica, tom: it.tonalidade.valor, momento: it.momento.valor}))
        .filter(it => it.id !== undefined),
    };
  }
}
