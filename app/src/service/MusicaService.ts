import {Share} from 'react-native';
import {InformacoesBasicasMusica, InformacoesDetalhadasMusica} from 'src/model/Musica';
import {encodeQueryData, openLink} from 'src/arch/util/linking';
import {MusicasFavoritasRepository} from '../repository/repertorioPadrao/MusicasFavoritasRepository';
import {MusicasRecemVisualizadasRepository} from 'src/repository/repertorioPadrao/MusicasRecemVisualizadasRepository';
import {MusicaMensagemTextoConverter} from 'src/util/MusicaMensagemTextoConverter';

export class MusicaService {
  static isFavorita(idMusica: number) {
    return new MusicasFavoritasRepository().isFavorita(idMusica);
  }

  static updateFavorita(idMusica: number, favorita: boolean) {
    return new MusicasFavoritasRepository().updateFavorita(idMusica, favorita);
  }

  static marcarAsVisualizada(musica: InformacoesBasicasMusica) {
    return new MusicasRecemVisualizadasRepository().marcarAsVisualizada(musica);
  }

  static async relatarErro(musica: InformacoesDetalhadasMusica) {
    const url = 'https://airtable.com/app9cwHxq1xPzFdBv/pagChDhWwrAF0NSPB/form';
    const params = encodeQueryData({
      prefill_id: `${musica.id_musica}`,
      'prefill_Nome da música': musica.titulo,
      'prefill_Início da música': musica.letra.slice(0, 50),
    });

    await openLink(`${url}?${params}`);
  }
}
