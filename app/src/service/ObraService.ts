import {CachesDirectoryPath, downloadFile} from '@dr.pogodin/react-native-fs';
import {Obra} from 'src/model/obra/Obra';
import {Api} from 'src/util/Api';

export class ObraService {
  private static downloadEmAndamento: boolean = false;

  async baixarObra(obra: Obra) {
    if (ObraService.downloadEmAndamento) {
      throw new Error('Já existe uma obra sendo baixada no momento. Aguarde até sua finalização');
    }

    ObraService.downloadEmAndamento = true;

    const path = `${CachesDirectoryPath}/${obra.id_obra}.db`;
    const response = downloadFile({
      fromUrl: Api.APP_BIBLIOTECA_OBRAS_SQLITE(obra),
      toFile: path,

      progressDivider: 1,

      progress(res) {
        console.log(res.bytesWritten / res.contentLength);
      },
    });

    try {
      await response.promise;
    } catch (e) {
      console.error(e);
      throw e;
    } finally {
      ObraService.downloadEmAndamento = false;
    }

    return path;
  }
}
