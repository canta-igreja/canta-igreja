import {Share} from 'react-native';
import {Auth} from 'src/arch/auth/Auth';
import {Toast} from 'src/arch/util/Toast';
import {RepertorioPersistidoResponse} from 'src/model/api/RepertorioRest';
import {InformacoesDetalhadasMusica} from 'src/model/Musica';
import {ItemRepertorio, ReferenciaRepertorioSlug, Repertorio} from 'src/model/repertorio/Repertorio';
import {UsuarioLogadoRepository} from 'src/repository/UsuarioLogadoRepository';
import {RepertorioService} from 'src/service/RepertorioService';
import {Api} from 'src/util/Api';
import {MusicaMensagemTextoConverter} from 'src/util/MusicaMensagemTextoConverter';

export class CompartilharService {
  private async getToken() {
    const auth = new Auth();
    if (!(await auth.isAutenticado())) {
      Toast.show('Para compartilhar um repertório, é necessário se autenticar em uma conta Google');
    }

    const token = await auth.autenticar();

    if (token === undefined) {
      Toast.show('Não foi possível realizar a autenticação');
    }

    return token;
  }

  async repertorio(repertorio: Repertorio, itens: ItemRepertorio[]): Promise<RepertorioPersistidoResponse | undefined> {
    const token = await this.getToken();
    const usuario = UsuarioLogadoRepository.getUsuarioLogado();
    if (token === undefined || usuario === undefined) {
      return;
    }

    const {error, data: repertorioCompartilhado} = await Api.compartilharRepertorio(usuario, repertorio, itens, token);
    if (error || repertorioCompartilhado === undefined) {
      Toast.show('Não foi possível compartilhar o repertório. O dispositivo está com internet?');
      return;
    }

    const corrente: ReferenciaRepertorioSlug = {
      conta: repertorioCompartilhado.conta.slug,
      repertorio: repertorioCompartilhado.slug,
    };
    const referencia: ReferenciaRepertorioSlug = repertorio.slug?.referencia ?? corrente;

    const repertorioAtualizado: Repertorio = {
      ...repertorio,
      slug: {referencia, corrente},
    };

    await Share.share(
      {
        title: repertorio.titulo,
        message: await new RepertorioService().gerarMensagemCompartilhamento(repertorioAtualizado, itens),
      },
      {
        dialogTitle: 'Compartilhar repertório',
      },
    );

    return repertorioCompartilhado;
  }

  async musica(musica: InformacoesDetalhadasMusica) {
    await Share.share(
      {
        title: musica.titulo,
        message: MusicaMensagemTextoConverter.convert(musica),
      },
      {
        dialogTitle: 'Compartilhar letra',
      },
    );
  }
}
