import * as eva from '@eva-design/eva';
import {NavigationContainer, NavigationContainerRef} from '@react-navigation/native';
import * as Sentry from '@sentry/react';
import {ApplicationProvider, Text} from '@ui-kitten/components';
import React, {FC, useEffect} from 'react';
import {Linking, Platform, StyleSheet} from 'react-native';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {SafeAreaView} from 'react-native-safe-area-context';
import SplashScreen from 'react-native-splash-screen';
import {Provider as ReduxProvider} from 'react-redux';
import {ReactNavigationRepository} from 'src/repository/ReactNavigationRepository';
import {inicializarSentry, routingInstrumentation} from './arch/error/Sentry';
import './arch/style/uni.styles';
import {AppStackNavigator} from './router/AppStackNavigator';
import {AppStackParams} from './router/AppStackNavigator.types';
import {linking} from './router/linking';
import {store} from './store/store';
import {ScreenFallback} from './view/component/screen/ScreenFallback';
import {SelecaoObrasBibliotecaModal} from './view/screen/biblioteca/fragment/SelecaoObrasBibliotecaModal';

inicializarSentry();

export const App = () => {
  // const isDarkMode = useColorScheme() === 'dark';
  const isDarkMode = false;

  const theme = isDarkMode //
    ? eva.dark
    : eva.light;

  useEffect(() => {
    SplashScreen.hide();
  }, []);

  const navigation = React.useRef<NavigationContainerRef<AppStackParams>>() as any;

  // const appState = useRef(AppState.currentState);

  // Considerar usar useSyncExternalStore - https://react.dev/learn/you-might-not-need-an-effect#subscribing-to-an-external-store
  // useEffect(() => {
  //   const subscription = AppState.addEventListener('change', nextAppState => {
  //     console.log(`Current - ${appState.current}`);
  //     console.log(`Next - ${nextAppState}`);

  //     if (appState.current.match(/inactive|background/) && nextAppState === 'active') {
  //       console.log('App has come to the foreground!');
  //     }

  //     appState.current = nextAppState;
  //   });

  //   return () => {
  //     subscription.remove();
  //   };
  // }, []);

  return (
    <React.StrictMode>
      <ReduxProvider store={store}>
        <ApplicationProvider {...eva} theme={theme}>
          <Sentry.ErrorBoundary
            fallback={
              <SafeAreaView>
                <Text style={styles.crashPage}>Ocorreu um erro inesperado, por favor reinicie o aplicativo</Text>
              </SafeAreaView>
            }>
            <NavigationContainer
              ref={navigation}
              linking={linking}
              onReady={() => {
                routingInstrumentation.registerNavigationContainer(navigation);
              }}>
              {/* <NavigationWithStatePersistenceContainer> */}
              {/* GestureHandlerRootView added when changed Stack to NativeStack */}
              <GestureHandlerRootView style={styles.gestureHadler}>
                <AppStackNavigator />
                <SelecaoObrasBibliotecaModal />
              </GestureHandlerRootView>
              {/* </NavigationWithStatePersistenceContainer> */}
            </NavigationContainer>
          </Sentry.ErrorBoundary>
        </ApplicationProvider>
      </ReduxProvider>
    </React.StrictMode>
  );
};

const styles = StyleSheet.create({
  crashPage: {
    margin: 24,
  },
  gestureHadler: {
    flex: 1,
  },
});

const NavigationWithStatePersistenceContainer: FC<{children: React.ReactElement}> = ({children}) => {
  const [isReady, setIsReady] = React.useState(Platform.OS === 'web'); // Don't persist state on web since it's based on URL
  const [initialState, setInitialState] = React.useState();

  React.useEffect(() => {
    const restoreState = async () => {
      try {
        const initialUrl = await Linking.getInitialURL();
        const hasDeepLink = initialUrl != null;

        if (!hasDeepLink) {
          const state = undefined; //await ReactNavigationRepository.getState();

          if (state !== undefined) {
            setInitialState(state);
          }
        }
      } finally {
        setIsReady(true);
      }
    };

    if (!isReady) {
      restoreState();
    }
  }, [isReady]);

  if (!isReady) {
    return <ScreenFallback />;
  }

  const updateState = (state: any) => ReactNavigationRepository.updateState(state);

  return (
    <NavigationContainer initialState={initialState} onStateChange={updateState}>
      {children}
    </NavigationContainer>
  );
};
