import {RepertorioImportavelDTO} from 'src/model/api/RepertorioRest';
import {RepertorioPadrao} from 'src/store/ItensRepertorio/ItensRepertorio.types';
import {MusicaDrawerStack, MusicaDrawerStackParams} from './MusicaDrawerNavigator.types';

export enum AppStack {
  Inicio = 'Inicio',
  Musica = 'Musica',

  IncluirMusicaEmRepertorio = 'IncluirMusicaEmRepertorio',
  Repertorios = 'Repertorios',
  ImportarRepertorioModal = 'ImportarRepertorioModal',

  MusicasRepertorioPadrao = 'MusicasRepertorioPadrao',
  MusicasRepertorioUsuario = 'MusicasRepertorioUsuario',

  Livro = 'Livro',
  LivroDetalhes = 'LivroDetalhes',

  MomentoModal = 'MomentoModal',
  TonalidadeModal = 'TonalidadeModal',

  BuscaMusicaParaRepertorio = 'BuscaMusicaParaRepertorio',
  PreviaMusicaModal = 'PreviaMusicaModal',
}

export type AppStackParams = {
  [AppStack.Inicio]: {};
  [AppStack.Musica]: {
    // screen: keyof MusicaDrawerStackParamList,
    // params: MusicaDrawerStackParamList[keyof MusicaDrawerStackParamList]
    screen: MusicaDrawerStack.MusicaDrawer;
    params: MusicaDrawerStackParams[MusicaDrawerStack.MusicaDrawer];
  };
  [AppStack.IncluirMusicaEmRepertorio]: {idMusica: number};

  [AppStack.Repertorios]: {conta?: string; repertorio?: string};
  [AppStack.ImportarRepertorioModal]: {repertorio: RepertorioImportavelDTO};

  [AppStack.MusicasRepertorioPadrao]: {repertorio: RepertorioPadrao};
  [AppStack.MusicasRepertorioUsuario]: {
    idRepertorio: number;
  };

  [AppStack.Livro]: {idEdicaoLivro: number};
  [AppStack.LivroDetalhes]: {idEdicaoLivro: number};

  [AppStack.MomentoModal]: {idItemRepertorio: number};
  [AppStack.TonalidadeModal]: {idItemRepertorio: number};
  [AppStack.BuscaMusicaParaRepertorio]: {idRepertorio: number};
  [AppStack.PreviaMusicaModal]: {idMusica: number; titulo?: string};
};
