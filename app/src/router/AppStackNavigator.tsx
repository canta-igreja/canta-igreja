import {RouteProp} from '@react-navigation/native';
import {NativeStackNavigationOptions, createNativeStackNavigator} from '@react-navigation/native-stack';
import React, {FC, useCallback} from 'react';
import {AppStack, AppStackParams} from 'src/router/AppStackNavigator.types';
import {HomeBottomNavigator} from 'src/router/HomeBottomNavigator';
import {BuscaMusicaParaRepertorioScreenLazy} from 'src/view/screen/BuscaMusicaParaRepertorioScreen.lazy';
import {IncluirMusicaEmRepertorioScreenLazy} from 'src/view/screen/IncluirMusicaEmRepertorioScreen.lazy';
import {LivroDetalhesScreenLazy} from 'src/view/screen/LivroDetalhesScreen.lazy';
import {MusicasRepertorioPadraoScreenLazy} from 'src/view/screen/MusicasRepertorioPadraoScreen.lazy';
import {MusicasRepertorioUsuarioScreenLazy} from 'src/view/screen/MusicasRepertorioUsuarioScreen.lazy';
import {ImportarRepertorioModalScreenLazy} from 'src/view/screen/modal/importarRepertorio/ImportarRepertorioModalScreen.lazy';
import {MomentoModalScreenLazy} from 'src/view/screen/modal/MomentoModalScreen.lazy';
import {PreviaMusicaModalScreenLazy} from 'src/view/screen/modal/PreviaMusicaModalScreen.lazy';
import {TonalidadeModalScreenLazy} from 'src/view/screen/modal/TonalidadeModalScreen.lazy';
import {MusicaScreenHeaderButtons} from 'src/view/screen/musica/MusicaScreenHeaderButtons';

const NativeStackNavigator = createNativeStackNavigator<AppStackParams>();
const MusicaScreenHeaderButtonsFunction = () => <MusicaScreenHeaderButtons />;

export const AppStackNavigator: FC = () => {
  const musicaOptions: (props: any) => NativeStackNavigationOptions = useCallback(
    ({route}: {route: RouteProp<AppStackParams, AppStack.Musica>}) => {
      const hasLivro = route.params.params.livro !== undefined && route.params.params.indice !== undefined;
      const title = hasLivro ? `${route.params.params.indice} - ${route.params.params.livro?.titulo}` : '';

      return {
        headerShown: true,
        headerShadowVisible: hasLivro,
        title: title,
        headerRight: MusicaScreenHeaderButtonsFunction,
        drawerPosition: 'right',
      };
    },
    [],
  );

  return (
    <NativeStackNavigator.Navigator initialRouteName={AppStack.Inicio}>
      {/* Abas referente a área principal */}
      <NativeStackNavigator.Group screenOptions={homeOptions}>
        <NativeStackNavigator.Screen name={AppStack.Inicio} component={HomeBottomNavigator} />
      </NativeStackNavigator.Group>

      {/* Fluxo de busca */}
      <NativeStackNavigator.Group>
        <NativeStackNavigator.Screen
          name={AppStack.Musica}
          getComponent={() => require('./MusicaDrawerNavigator').MusicaDrawerNavigator}
          options={musicaOptions}
        />
        <NativeStackNavigator.Screen
          name={AppStack.IncluirMusicaEmRepertorio}
          component={IncluirMusicaEmRepertorioScreenLazy}
          options={defaultOptions}
        />
      </NativeStackNavigator.Group>

      {/* Fluxo de livros */}
      <NativeStackNavigator.Group screenOptions={defaultOptions}>
        {/*<NativeStackNavigator.Screen
          name={AppStack.Livro}
          component={LivroScreenLazy}
        />*/}
        <NativeStackNavigator.Screen name={AppStack.LivroDetalhes} component={LivroDetalhesScreenLazy} />
      </NativeStackNavigator.Group>

      {/* Fluxo de Repertórios */}
      <NativeStackNavigator.Group screenOptions={defaultOptions}>
        <NativeStackNavigator.Screen
          name={AppStack.MusicasRepertorioPadrao}
          component={MusicasRepertorioPadraoScreenLazy}
        />
        <NativeStackNavigator.Screen
          name={AppStack.MusicasRepertorioUsuario}
          component={MusicasRepertorioUsuarioScreenLazy}
        />
        <NativeStackNavigator.Screen
          name={AppStack.BuscaMusicaParaRepertorio}
          component={BuscaMusicaParaRepertorioScreenLazy}
        />
      </NativeStackNavigator.Group>

      <NativeStackNavigator.Group screenOptions={modalPresentation}>
        <NativeStackNavigator.Screen
          name={AppStack.ImportarRepertorioModal}
          component={ImportarRepertorioModalScreenLazy}
          options={defaultOptions}
        />
        <NativeStackNavigator.Screen
          name={AppStack.MomentoModal}
          component={MomentoModalScreenLazy}
          options={defaultOptions}
        />
        <NativeStackNavigator.Screen
          name={AppStack.TonalidadeModal}
          component={TonalidadeModalScreenLazy}
          options={defaultOptions}
        />
        <NativeStackNavigator.Screen
          name={AppStack.PreviaMusicaModal}
          component={PreviaMusicaModalScreenLazy}
          options={defaultOptions}
        />
      </NativeStackNavigator.Group>
    </NativeStackNavigator.Navigator>
  );
};

const homeOptions: NativeStackNavigationOptions = {
  headerShown: false,
};
const modalPresentation: NativeStackNavigationOptions = {presentation: 'modal'};
const defaultOptions = () => {
  return {
    headerShadowVisible: false,
    title: '',
  };
};
