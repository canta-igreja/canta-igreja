import {BottomTabBarProps, BottomTabNavigationOptions, createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {BottomNavigation, BottomNavigationTab} from '@ui-kitten/components';
import React, {FC, useCallback} from 'react';
import {Platform} from 'react-native';
import {UnistylesRuntime, createStyleSheet, useStyles} from 'react-native-unistyles';
import {BuscarIcon, LivroIcon, RepertoriosIcon} from 'src/view/base/icones/Icones';
import {RepertoriosScreenLazy} from 'src/view/screen/RepertoriosScreen.lazy';
import {BibliotecaScreenLazy} from 'src/view/screen/biblioteca/BibliotecaScreen.lazy';
import {useOnboardingApplication} from 'src/view/screen/biblioteca/fragment/useOnboardingApplication';
import {HomeStack} from './HomeBottomNavigator.types';

const {Navigator, Screen} = createBottomTabNavigator();

const MusicasScreen = require('src/view/screen/MusicasScreen').MusicasScreen;

export const HomeBottomNavigator = () => {
  const tabBar = useCallback((props: BottomTabBarProps) => <NavigationTab {...props} />, []);
  const options: BottomTabNavigationOptions = {
    headerShown: false,

    tabBarLabel: 'Home',
    tabBarIcon: RepertoriosIcon,
  };

  useOnboardingApplication();

  return (
    <Navigator tabBar={tabBar} initialRouteName="Busca">
      <Screen name={HomeStack.Biblioteca} component={BibliotecaScreenLazy} options={options} />
      {/* {preview && <Screen name="Livros" component={LivrosScreen} options={options} />} */}
      <Screen name={HomeStack.Busca} component={MusicasScreen} options={options} />
      <Screen name={HomeStack.Repertorios} component={RepertoriosScreenLazy} options={options} />
    </Navigator>
  );
};

const NavigationTab: FC<BottomTabBarProps> = ({navigation, state}) => {
  const {styles} = useStyles(stylesheet);

  // FIXME - Pensar como fazer isso sem depender de state.index
  const onSelect = useCallback(
    (index: number) => {
      if (index === state.index) {
        return;
      }

      navigation.navigate(state.routeNames[index]);
    },
    [navigation, state.index, state.routeNames],
  );

  return (
    <BottomNavigation selectedIndex={state.index} onSelect={onSelect} style={styles.bottomNavigation}>
      <BottomNavigationTab key="biblioteca" icon={LivroIcon} title="Biblioteca" />
      {/* {preview ? <BottomNavigationTab key="livros" icon={LivroIcon} title="Livros" /> : <></>} */}
      <BottomNavigationTab key="busca" icon={BuscarIcon} title="Busca" />
      <BottomNavigationTab key="repertorios" icon={RepertoriosIcon} title="Repertórios" />
    </BottomNavigation>
  );
};

const stylesheet = createStyleSheet(theme => ({
  bottomNavigation: {
    paddingBottom: UnistylesRuntime.insets.bottom,
  },
}));
