export enum HomeStack {
  Biblioteca = 'Biblioteca',
  Busca = 'Busca',
  Repertorios = 'Repertorios',
}

export type HomeStackParams = {
  [HomeStack.Biblioteca]: {};
  [HomeStack.Busca]: {};
  [HomeStack.Repertorios]: {};
};
