import {createDrawerNavigator} from '@react-navigation/drawer';
import React from 'react';
import {ConfiguracoesMusicaDrawerLazy} from 'src/view/fragment/Musica/drawer/ConfiguracoesMusicaDrawer.lazy';
import {MusicaScreenLazy} from 'src/view/screen/musica/MusicaScreen.lazy';
import {MusicaDrawerStack, MusicaDrawerStackParams} from './MusicaDrawerNavigator.types';

const Drawer = createDrawerNavigator<MusicaDrawerStackParams>();

export const MusicaDrawerNavigator = () => {
  return (
    <Drawer.Navigator initialRouteName={MusicaDrawerStack.MusicaDrawer} drawerContent={ConfiguracoesMusicaDrawerLazy}>
      <Drawer.Screen
        name={MusicaDrawerStack.MusicaDrawer}
        component={MusicaScreenLazy}
        options={{
          drawerPosition: 'right',
          headerShown: false,
          drawerType: 'front',
        }}
      />
    </Drawer.Navigator>
  );
};
