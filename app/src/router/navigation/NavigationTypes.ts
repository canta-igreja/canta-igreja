import {ParamListBase, RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {AppStackParams} from 'src/router/AppStackNavigator.types';
import {MusicaDrawerStackParams} from 'src/router/MusicaDrawerNavigator.types';
import {HomeStackParams} from 'src/router/HomeBottomNavigator.types';

export type NavigationProps<ParamList extends ParamListBase, Key extends keyof ParamList> = {
  navigation: StackNavigationProp<ParamList, Key>;
  route: RouteProp<ParamList, Key>;
};

export type HomeNavigationProps<Key extends keyof HomeStackParams> = NavigationProps<HomeStackParams, Key>;
export type AppStackNavigationProps<Key extends keyof AppStackParams> = NavigationProps<AppStackParams, Key>;
export type MusicaDrawerNavigationProps<Key extends keyof MusicaDrawerStackParams> = NavigationProps<
  MusicaDrawerStackParams,
  Key
>;
