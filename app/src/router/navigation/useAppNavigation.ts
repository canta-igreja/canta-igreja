import {RouteProp, useNavigation, useRoute} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {AppStack, AppStackParams} from '../AppStackNavigator.types';
import {useMemo} from 'react';
import {ItemRepertorio, Repertorio} from 'src/model/repertorio/Repertorio';
import {IndiceMusica} from 'src/model/IndiceMusica';
import {RepertorioImportavelDTO} from 'src/model/api/RepertorioRest';

export const useMusicasStackNavigation = () => useNavigation<StackNavigationProp<AppStackParams, AppStack.Musica>>();

export const useStackRoute = <Key extends keyof AppStackParams>() => useRoute<RouteProp<AppStackParams, Key>>();

export const useNavStack = () => {
  const navigation = useMusicasStackNavigation();

  return useMemo(() => new NavStack(navigation), [navigation]);
};

export class NavStack {
  constructor(private navigation: StackNavigationProp<AppStackParams, AppStack.Musica>) {}

  goBack() {
    this.navigation.goBack();
  }

  navigateToImportarRepertorioModal(repertorio: RepertorioImportavelDTO) {
    this.navigation.navigate(AppStack.ImportarRepertorioModal, {repertorio});
  }

  navigateToMomentoModal(itemRepertorio: ItemRepertorio) {
    this.navigation.navigate(AppStack.MomentoModal, {idItemRepertorio: itemRepertorio.id_item_repertorio});
  }
  navigateToTonalidadeModal(itemRepertorio: ItemRepertorio) {
    this.navigation.navigate(AppStack.TonalidadeModal, {idItemRepertorio: itemRepertorio.id_item_repertorio});
  }

  navigateDetalhesEdicaoLivro(indiceMusica: IndiceMusica) {
    this.navigation.navigate(AppStack.LivroDetalhes, {idEdicaoLivro: indiceMusica.id_edicao_livro});
  }

  navigateToRepertorio(repertorio: Repertorio) {
    this.navigation.navigate(AppStack.MusicasRepertorioUsuario, {idRepertorio: repertorio.id_repertorio});
  }
}
