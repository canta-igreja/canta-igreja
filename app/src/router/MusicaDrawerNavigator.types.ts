import {EdicaoLivro} from 'src/model/Livro';
import {MusicaItemListData} from 'src/view/component/list/data/MusicaItemListData';

export enum MusicaDrawerStack {
  MusicaDrawer = 'MusicaDrawer',
}

export type MusicaDrawerStackParams = {
  [MusicaDrawerStack.MusicaDrawer]: {
    livro?: EdicaoLivro;
    /**
     * FIXME: descobrir se isso está em uso/se é necessário
     * @deprecated
     */
    indice?: MusicaItemListData;
    idMusica: number;
    titulo?: string;

    idItemRepertorio?: number;
  };
};
