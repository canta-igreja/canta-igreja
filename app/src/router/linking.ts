import {LinkingOptions, getStateFromPath} from '@react-navigation/native';
import {AppStackParams, AppStack} from './AppStackNavigator.types';
import {HomeStack} from './HomeBottomNavigator.types';

export const linking: LinkingOptions<AppStackParams> = {
  prefixes: ['canta://canta.app', 'https://canta.app', 'https://preview.canta.app'],
  config: {
    screens: {
      [AppStack.Inicio]: {
        screens: {
          [HomeStack.Repertorios]: 'repertorios/importar/:conta/:repertorio',
          [HomeStack.Busca]: '*',
        },
      },
    },
  },
  getStateFromPath(path, config) {
    const pattern = /^\/repertorios\/?\?([a-zA-Z-0-9_\-]+):([a-zA-Z-0-9_\-]+)/;

    if (path.startsWith('/repertorios') && path.match(pattern)?.length === 3) {
      const [_, conta, repertorio] = path.match(pattern) ?? [];
      return getStateFromPath(`/repertorios/importar/${conta}/${repertorio}`, config);
    } else {
      return getStateFromPath(path, config);
    }
  },
};
