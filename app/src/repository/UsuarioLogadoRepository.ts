import {jwtDecode} from 'jwt-decode';
import {MMKV} from 'react-native-mmkv';
import {Credenciais} from 'src/arch/auth/AuthRepository';
import {Conta} from 'src/arch/auth/Conta';

export interface UsuarioLogado {
  slug: string;
  nome: string;

  imagem?: string;
}

export class UsuarioLogadoRepository {
  private static readonly KEY = '@cantaIgreja/usuario';

  private static readonly storage = new MMKV({
    id: UsuarioLogadoRepository.KEY,
  });

  static getUsuarioLogado(): UsuarioLogado | undefined {
    const response = UsuarioLogadoRepository.storage.getString(UsuarioLogadoRepository.KEY);

    if (response) {
      return JSON.parse(response) as UsuarioLogado;
    }
  }

  static setUsuarioLogado(credenciais: Credenciais, conta: Conta): UsuarioLogado {
    const usuarioLogado = {
      slug: conta.slug,
      nome: conta.nome,

      imagem: (jwtDecode(credenciais.idToken) as any).picture,
    };

    UsuarioLogadoRepository.storage.set(UsuarioLogadoRepository.KEY, JSON.stringify(usuarioLogado));

    return usuarioLogado;
  }

  static logout() {
    UsuarioLogadoRepository.storage.delete(UsuarioLogadoRepository.KEY);
  }
}
