import {MMKV} from 'react-native-mmkv';
import {MusicaItemListData} from 'src/view/component/list/data/MusicaItemListData';
import {Banco} from '../../arch/persistence/Banco';

export class RepertorioPadraoRepository {
  private static readonly ORDEM_KEY = 'ordem';

  constructor(
    private storage: MMKV,
    private permitirDuplicacoes: boolean,
    private limite: number | undefined,
  ) {}

  public isPresent(idMusica: number): boolean {
    return this.storage.contains(`${idMusica}`);
  }

  public ordem(): number[] {
    let ordemAsStr = this.storage.getString(RepertorioPadraoRepository.ORDEM_KEY);
    const ordem = ordemAsStr ? JSON.parse(ordemAsStr) : this.ordemInicial();

    return ordem;
  }

  public setOrdem(novaOrdemMusicas: number[]) {
    this.storage.set(RepertorioPadraoRepository.ORDEM_KEY, JSON.stringify(novaOrdemMusicas));
  }

  public ordemInicial() {
    const keys = this.storage.getAllKeys();
    return keys.map(it => parseInt(it, 10)).filter(it => !isNaN(it));
  }

  public add(idMusica: number) {
    let ordem = this.ordem();

    if (!this.permitirDuplicacoes) {
      ordem = ordem.filter(it => it !== idMusica);
    }
    if (this.limite !== undefined) {
      ordem = ordem.slice(0, this.limite);
    }

    this.setOrdem([idMusica, ...ordem]);

    return this.storage.set(`${idMusica}`, true);
  }

  public remove(idMusica: number) {
    const novaOrdemMusicas = this.ordem().filter(it => it !== idMusica);
    this.setOrdem(novaOrdemMusicas);

    return this.storage.delete(`${idMusica}`);
  }

  public async findAll(): Promise<MusicaItemListData[]> {
    const ids = this.ordem();

    const query = `
      SELECT id_musica, null, titulo, substr(letra, 0, 70) as trecho_letra
        FROM musica
       WHERE id_musica in (${`${ids.join(', ')}`})
    `;
    const response = await Banco.executeAsync(query);
    const musicas: MusicaItemListData[] = response.rows?._array ?? [];

    const musicasById: {[key: number]: MusicaItemListData} = {};
    musicas.map(it => (musicasById[it.id_musica] = it));

    return ids.map(it => musicasById[it]);
  }
}
