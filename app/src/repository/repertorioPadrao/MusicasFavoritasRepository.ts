import {MMKV} from 'react-native-mmkv';
import {RepertorioPadraoRepository} from './RepertorioPadraoRepository';

export class MusicasFavoritasRepository {
  private static readonly KEY = '@cantaIgreja/musicasFavoritas';

  private static readonly storage = new MMKV({
    id: MusicasFavoritasRepository.KEY,
  });

  private repertorioPadrao: RepertorioPadraoRepository;

  constructor() {
    this.repertorioPadrao = new RepertorioPadraoRepository(MusicasFavoritasRepository.storage, false, undefined);
  }

  public isFavorita(idMusica: number): boolean {
    return this.repertorioPadrao.isPresent(idMusica);
  }

  public updateFavorita(idMusica: number, favorita: boolean) {
    return favorita ? this.updateAsFavorita(idMusica) : this.updateAsNaoFavorita(idMusica);
  }

  public updateAsFavorita(idMusica: number) {
    return this.repertorioPadrao.add(idMusica);
  }

  public updateAsNaoFavorita(idMusica: number) {
    return this.repertorioPadrao.remove(idMusica);
  }

  public async findAll() {
    return await this.repertorioPadrao.findAll();
  }
}
