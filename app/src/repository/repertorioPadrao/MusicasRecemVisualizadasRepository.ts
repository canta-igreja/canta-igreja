import {MMKV} from 'react-native-mmkv';
import {RepertorioPadraoRepository} from './RepertorioPadraoRepository';
import {InformacoesBasicasMusica} from 'src/model/Musica';

export class MusicasRecemVisualizadasRepository {
  private static readonly KEY = '@cantaIgreja/musicasRecemVisualizadas';

  private static readonly storage = new MMKV({
    id: MusicasRecemVisualizadasRepository.KEY,
  });

  private repertorioPadrao: RepertorioPadraoRepository;

  constructor() {
    this.repertorioPadrao = new RepertorioPadraoRepository(MusicasRecemVisualizadasRepository.storage, false, 25);
  }

  marcarAsVisualizada(musica: InformacoesBasicasMusica) {
    this.repertorioPadrao.add(musica.id_musica);
  }

  public async findAll() {
    return await this.repertorioPadrao.findAll();
  }
}
