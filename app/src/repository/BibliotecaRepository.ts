import {Platform} from 'react-native';
import {MusicaResponse} from 'src/model/api/MusicaResponse';
import {ExemplarBiblioteca, Obra} from 'src/model/obra/Obra';
import {Banco} from '../arch/persistence/Banco';
import {PreparedStatement} from '../arch/persistence/sqlite/Statement';

export class BibliotecaRepository {
  private static readonly findAllExemplaresAtivos = new PreparedStatement(`
    select id_obra, id_exemplar, titulo, descricao, crc32, ativo
      from exemplar_biblioteca
     where ativo
    `);

  private static readonly alias: string = 'toImport';
  private static readonly SELECT_DIFFERENT_DATA = `
   SELECT ${this.alias}.musica.id_musica,
          ${this.alias}.musica.titulo,
          ${this.alias}.musica.letra,
          ${this.alias}.musica.contribuidores,
          ${this.alias}.musica.midias,
          ${this.alias}.musica.crc32
    FROM ${this.alias}.musica
    LEFT JOIN main.musica
      ON main.musica.id_musica = ${this.alias}.musica.id_musica
   WHERE TRUE
      -- is musica nova
     AND main.musica.id_musica is null
      -- is musica atualizada
      OR (main.musica.id_musica is not null
          AND main.musica.crc32 != ${this.alias}.musica.crc32
      )
  `;
  private static readonly upsertMusicas = new PreparedStatement(`
    INSERT INTO musica (id_musica, titulo, letra, contribuidores, midias, crc32)
      ${this.SELECT_DIFFERENT_DATA}
    ON CONFLICT(id_musica) DO UPDATE SET
       titulo = excluded.titulo,
       letra = excluded.letra,
       contribuidores = excluded.contribuidores,
       midias = excluded.midias,
       crc32 = excluded.crc32;
    `);

  private static readonly disableExemplarObra = new PreparedStatement(`
    UPDATE exemplar_biblioteca
       SET ativo=false
     WHERE id_obra=? and ativo 
    `);

  private static readonly createExemplarObra = new PreparedStatement(`
    INSERT INTO exemplar_biblioteca (id_obra, titulo, descricao, crc32)
    VALUES (?, ?, ?, ?)
    `);

  private static readonly upsertLivroEditoraEdicaoLivro = new PreparedStatement(`
    INSERT INTO main.livro (id_livro, titulo)
      SELECT * FROM ${this.alias}.livro WHERE true
    ON CONFLICT(id_livro) DO UPDATE SET
       id_livro = excluded.id_livro,
       titulo = excluded.titulo;

    INSERT INTO main.editora (id_editora, nome, site)
      SELECT * FROM ${this.alias}.editora WHERE true
    ON CONFLICT(id_editora) DO UPDATE SET
       id_editora = excluded.id_editora,
       nome = excluded.nome,
       site = excluded.site;
    
    INSERT INTO main.edicao_livro
      SELECT * FROM ${this.alias}.edicao_livro WHERE true
    ON CONFLICT(id_edicao_livro) DO UPDATE SET
        id_edicao_livro = excluded.id_edicao_livro,
        id_livro = excluded.id_livro,
        edicao = excluded.edicao,
        id_editora = excluded.id_editora,
        ano = excluded.ano,
        descricao = excluded.descricao,
        site = excluded.site,
        detalhes = excluded.detalhes;
      `);

  private static readonly upsertCanticoLivro = new PreparedStatement(`
    INSERT INTO main.cantico_livro (id_musica, id_edicao_livro, indice, ordem)

    SELECT ${this.alias}.cantico_livro.id_musica,
           ${this.alias}.cantico_livro.id_edicao_livro,
           ${this.alias}.cantico_livro.indice,
           ${this.alias}.cantico_livro.ordem
      FROM ${this.alias}.cantico_livro
     WHERE true
    
    ON CONFLICT(id_musica, id_edicao_livro) DO UPDATE SET
      id_musica = excluded.id_musica,
      id_edicao_livro = excluded.id_edicao_livro,
      indice = excluded.indice,
      ordem = excluded.ordem;

    DELETE FROM main.cantico_livro
     WHERE id_musica || id_edicao_livro IN (

     SELECT main.cantico_livro.id_musica || main.cantico_livro.id_edicao_livro
       FROM main.cantico_livro
       LEFT JOIN ${this.alias}.cantico_livro on (
            main.cantico_livro.id_musica = ${this.alias}.cantico_livro.id_musica
        AND main.cantico_livro.id_edicao_livro = ${this.alias}.cantico_livro.id_edicao_livro
       )
      WHERE main.cantico_livro.id_musica IS NULL
    
    );
    `);

  private static readonly deleteCreateSecaoLivroCanticoSecao = new PreparedStatement(`
      DELETE FROM main.cantico_secao
      WHERE id_edicao_livro IN (

        SELECT DISTINCT ${this.alias}.secao_edicao_livro.id_edicao_livro
          FROM ${this.alias}.secao_edicao_livro

      );

      INSERT INTO main.cantico_secao (id_secao_edicao_livro, id_musica, id_edicao_livro)
      SELECT ${this.alias}.cantico_secao.id_secao_edicao_livro,
             ${this.alias}.cantico_secao.id_musica,
             ${this.alias}.cantico_secao.id_edicao_livro
        FROM ${this.alias}.cantico_secao;
      

      DELETE FROM main.secao_edicao_livro
      WHERE id_edicao_livro IN (

        SELECT DISTINCT ${this.alias}.secao_edicao_livro.id_edicao_livro
          FROM ${this.alias}.secao_edicao_livro

      );

      INSERT INTO main.secao_edicao_livro (id_edicao_livro, titulo, ordem)
      SELECT ${this.alias}.secao_edicao_livro.id_secao_edicao_livro,
             ${this.alias}.secao_edicao_livro.titulo,
             ${this.alias}.secao_edicao_livro.ordem
        FROM ${this.alias}.secao_edicao_livro;
  `);

  static async findExemplaresAtivos(): Promise<{[key: number]: ExemplarBiblioteca}> {
    const result = await Banco.executeAsync(this.findAllExemplaresAtivos.query, []);

    const itens = result.rows?._array ?? [];
    const response: {[key: number]: ExemplarBiblioteca} = {};

    for (const exemplar of itens) {
      response[exemplar.id_obra] = {
        id_obra: exemplar.id_obra,
        id_exemplar: exemplar.id_exemplar,

        titulo: exemplar.titulo,
        descricao: exemplar.descricao,

        crc32: exemplar.crc32,
        ativo: exemplar.ativo,
      };
    }

    return response;
  }

  static async importarObra(obra: Obra, path: string) {
    // UPSERT
    // https://sqldocs.org/sqlite/sqlite-upsert-insert-on-conflict/
    let cacheDir = Platform.OS === 'ios' ? 'Caches' : '../cache/';
    await Banco.attach(`${obra.id_obra}.db`, BibliotecaRepository.alias, cacheDir);

    try {
      await this.atualizarMusicas();
      await this.atualizarLivros();
      await this.registrarObra(obra);
    } finally {
      await Banco.detach(BibliotecaRepository.alias);
    }
  }

  private static async atualizarMusicas() {
    await Banco.executeAsync(this.upsertMusicas.query, []);
  }

  private static async atualizarLivros() {
    await Promise.all([
      Banco.executeAsync(this.upsertLivroEditoraEdicaoLivro.query, []),
      Banco.executeAsync(this.upsertCanticoLivro.query, []),
      Banco.executeAsync(this.deleteCreateSecaoLivroCanticoSecao.query, []),
    ]);
  }

  private static async registrarObra(obra: Obra) {
    await Banco.executeAsync(this.disableExemplarObra.query, []);
    await Banco.executeAsync(this.createExemplarObra.query, [obra.id_obra, obra.titulo, obra.descricao, obra.crc32]);
  }

  public static async atualizarMusicasSeNecessario(musicasResponse: MusicaResponse[]) {
    const upsert = /*sql*/ `
      INSERT INTO musica (id_musica, titulo, letra, contribuidores, midias, crc32)
      VALUES (?, ?, ?, ?, ?, ?)
      ON CONFLICT(id_musica) DO UPDATE SET
         titulo = excluded.titulo,
         letra = excluded.letra,
         contribuidores = excluded.contribuidores,
         midias = excluded.midias,
         crc32 = excluded.crc32;
      `;

    const params = musicasResponse.map(it => [
      it.id_musica,
      it.titulo,
      it.letra,
      JSON.stringify(it.colaboradores),
      JSON.stringify(it.midias),
      it.crc32,
    ]);
    await Banco.executeBatchAsync([[upsert, params]]);
  }
}
