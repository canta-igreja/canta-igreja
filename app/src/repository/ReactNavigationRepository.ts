import {MMKV} from 'react-native-mmkv';

export class ReactNavigationRepository {
  private static readonly KEY = '@cantaIgreja/reactNavigation';

  private static readonly CONTENT_KEY = 'state';

  private static readonly storage = new MMKV({
    id: ReactNavigationRepository.KEY,
  });

  static async getState(): Promise<any | undefined> {
    const state = ReactNavigationRepository.storage.getString(ReactNavigationRepository.CONTENT_KEY);

    return state ? JSON.parse(state) : undefined;
  }

  static async updateState(state: any) {
    ReactNavigationRepository.storage.set(ReactNavigationRepository.CONTENT_KEY, JSON.stringify(state));
  }
}
