import {QueryResult} from '@op-engineering/op-sqlite';
import {ReferenciaMusica} from 'src/model/Musica';
import {ItemRepertorio, Repertorio, RepertorioSlug} from 'src/model/repertorio/Repertorio';
import {LetraUtil} from 'src/util/LetraUtil';
import {ListUtil} from 'src/util/ListUtil';
import {MusicaItemListData} from 'src/view/component/list/data/MusicaItemListData';
import {Banco} from '../arch/persistence/Banco';
import {PreparedStatement} from '../arch/persistence/sqlite/Statement';
import {QueryOperation} from 'src/arch/persistence/sqlite/QueryOperation';
import {filterFalsy} from 'src/util/CollectionUtil';
import {Timestamp} from 'src/util/Timestamp';

export type ItemRepertorioAtualizado = {
  item: ItemRepertorio;
  repertorio: Repertorio;
};

type RepertoriosFilter = {id?: number; slug?: string};

export class RepertoriosRepository {
  private static readonly createRepertorioStatement = new PreparedStatement(/*sql*/ `
    INSERT INTO repertorio (titulo, slug, data_edicao) 
         VALUES (?, ?, ?)
    `);

  private static readonly findRepertoriosQuery = (filter?: RepertoriosFilter) => {
    // FIXME slug
    const idFilter = filter?.id ? 'AND id_repertorio = ?' : '';
    const slugFilter = filter?.slug
      ? /*sql*/ `AND (repertorio.slug ->> '$.referencia.repertorio' = ? OR repertorio.slug ->> '$.corrente.repertorio' = ?)`
      : '';

    return /*sql*/ `
    SELECT id_repertorio,
           slug,
           titulo,
           resumo,
           ordem_musicas,
           data_edicao
      FROM repertorio
     WHERE id_tipo_repertorio = 2
           ${idFilter}
           ${slugFilter}
       AND data_exclusao IS NULL
     ORDER BY data_criacao DESC
    `;
  };

  private static readonly mensagens = {
    repertorioSemMusica: 'Nenhuma música adicionada',
  };

  static async createRepertorio(titulo: string, slug?: RepertorioSlug): Promise<Repertorio> {
    const now = Timestamp.now();

    const response = await Banco.executeAsync(this.createRepertorioStatement.query, [
      titulo,
      JSON.stringify(slug),
      now,
    ]);

    return {
      id_repertorio: response.insertId!!,
      slug: slug,

      data: {
        edicao: now,
      },

      titulo: titulo,
      resumo: RepertoriosRepository.mensagens.repertorioSemMusica,
      ordem_musicas: [],
    };
  }

  static async deactivateRepertorio(repertorio: Repertorio) {
    const now = Timestamp.now();

    const query = /*sql*/ `
    UPDATE repertorio
       SET data_exclusao = ?,
           data_edicao = ?
     WHERE id_repertorio = ?
    `;
    await Banco.executeAsync(query, [Date.now(), now, repertorio.id_repertorio]);
  }

  static async renameRepertorio(repertorio: Repertorio, novoTitulo: string): Promise<Repertorio> {
    const now = Timestamp.now();

    const query = /*sql*/ `
    UPDATE repertorio
       SET titulo = ?,
           data_edicao = ?
     WHERE id_repertorio = ?
    `;
    await Banco.executeAsync(query, [novoTitulo, now, repertorio.id_repertorio]);

    return {
      ...repertorio,
      titulo: novoTitulo,
      data: {
        ...repertorio.data,
        edicao: now,
      },
    };
  }

  static async findRepertorios(filter?: RepertoriosFilter): Promise<Repertorio[]> {
    const params = [
      filter?.id,
      // Slug tem que ser passado duas vezes
      filter?.slug,
      filter?.slug,
    ].filter(it => it !== undefined);

    const response = await Banco.executeAsync(this.findRepertoriosQuery(filter), params);

    return this.findRepertoriosResponse(response);
  }

  private static findRepertoriosResponse(result: QueryResult): Repertorio[] {
    return (
      result.rows?._array?.map(it => ({
        id_repertorio: it.id_repertorio,
        slug: JSON.parse(it.slug),
        titulo: it.titulo,
        resumo: it.resumo,
        ordem_musicas: JSON.parse(it.ordem_musicas),

        data: {
          edicao: it.data_edicao,
        },
      })) ?? []
    );
  }

  static async addMusica(
    repertorio: Repertorio,
    referenciaMusica: ReferenciaMusica,
  ): Promise<ItemRepertorioAtualizado> {
    // Incluir item no repertorio
    const createItemRepertorio = `
      INSERT INTO item_repertorio (id_repertorio, id_musica)
           VALUES (?, ?)
    `;

    const insertResponse = await Banco.executeAsync(createItemRepertorio, [
      repertorio.id_repertorio,
      referenciaMusica.id_musica,
    ]);
    const idItemRepertorio = insertResponse.insertId!!;

    // Atualizar repertorio
    const novaOrdemMusicas = [...repertorio.ordem_musicas, idItemRepertorio];
    const repertorioAtualizado = {...repertorio, ordem_musicas: novaOrdemMusicas};

    const [repertorioComResumoAtualizado, musica] = await Promise.all([
      await RepertoriosRepository.updateResumoOrdemMusicas(repertorioAtualizado),
      await RepertoriosRepository.findMusicaParaRepertorio(referenciaMusica.id_musica),
    ]);

    return {
      repertorio: repertorioComResumoAtualizado,
      item: {
        id_item_repertorio: idItemRepertorio,
        id_repertorio: repertorio.id_repertorio,

        musica: musica,

        tonalidade: {
          valor: undefined,
          sugestoes: musica.tonalidades ?? [],
        },
        momento: {
          valor: undefined,
          sugestoes: musica.momentos ?? [],
        },
      },
    };
  }

  static async addTermo(repertorio: Repertorio, termo: string): Promise<ItemRepertorioAtualizado> {
    // Incluir item no repertorio
    const createItemRepertorio = `
      INSERT INTO item_repertorio (id_repertorio, termo)
           VALUES (?, ?)
    `;

    const insertResponse = await Banco.executeAsync(createItemRepertorio, [repertorio.id_repertorio, termo]);
    const idItemRepertorio = insertResponse.insertId!!;

    // Atualizar repertorio
    const novaOrdemMusicas = [...repertorio.ordem_musicas, idItemRepertorio];
    const repertorioAtualizado = {...repertorio, ordem_musicas: novaOrdemMusicas};

    const repertorioComResumoAtualizado = await RepertoriosRepository.updateResumoOrdemMusicas(repertorioAtualizado);

    return {
      repertorio: repertorioComResumoAtualizado,
      item: {
        id_item_repertorio: idItemRepertorio,
        id_repertorio: repertorio.id_repertorio,

        musica: undefined,
        termo: termo,

        tonalidade: {
          valor: undefined,
          sugestoes: [],
        },
        momento: {
          valor: undefined,
          sugestoes: [],
        },
      },
    };
  }

  private static async updateResumoOrdemMusicas(repertorio: Repertorio): Promise<Repertorio> {
    const now = Timestamp.now();

    const updateRepertorio = /*sql*/ `
      UPDATE repertorio
         SET resumo=?,
             ordem_musicas=?,
             data_edicao=?
       WHERE id_repertorio=?
    `;

    const resumoAtualizado = await RepertoriosRepository.gerarResumo(repertorio);

    await Banco.executeAsync(updateRepertorio, [
      resumoAtualizado,
      JSON.stringify(repertorio.ordem_musicas),
      now,
      repertorio.id_repertorio,
    ]);

    return {
      ...{
        ...repertorio,
        data: {
          ...repertorio.data,
          edicao: now,
        },
      },
      resumo: resumoAtualizado,
    };
  }

  private static async gerarResumo(repertorio: Repertorio) {
    const numeroMusicasNoResumo = 3;
    const musicasNoResumo = repertorio.ordem_musicas.slice(0, numeroMusicasNoResumo);

    const selectMusicas = /*sql*/ `
      SELECT id_item_repertorio, coalesce(termo, musica.titulo) as titulo
        FROM item_repertorio
        LEFT JOIN musica using (id_musica)
       WHERE id_item_repertorio IN (${`${musicasNoResumo.join(', ')}`})
    `;

    const response = await Banco.executeAsync(selectMusicas);
    const rows = response.rows?._array ?? [];

    const musicas: {[key: number]: string} = {};
    rows.forEach(it => (musicas[it.id_item_repertorio] = it.titulo));

    if (repertorio.ordem_musicas.length === 0) {
      return RepertoriosRepository.mensagens.repertorioSemMusica;
    }

    const primeirasMusicas = musicasNoResumo.map(it => musicas[it]).join('; ');
    const sufixo =
      repertorio.ordem_musicas.length > numeroMusicasNoResumo
        ? ` e mais ${repertorio.ordem_musicas.length - numeroMusicasNoResumo}`
        : '';

    return `${primeirasMusicas}${sufixo}`;
  }

  private static async findMusicaParaRepertorio(idMusica: number): Promise<MusicaItemListData> {
    const query = /*sql*/ `
      SELECT musica.id_musica as id_musica, musica.titulo,
             substr(letra, 0, 70) as trecho_letra,
             musica.informacoes ->> 'tonalidades' as tonalidades,
             musica.informacoes ->> 'momentos' as momentos
        FROM musica
       WHERE id_musica = ?
    `;

    const response = await Banco.executeAsync(query, [idMusica]);
    const rows = response.rows?._array ?? [];

    if (rows.length !== 1) {
      throw 'Música não localizda';
    }

    const item = rows[0];

    return {
      id_musica: item.id_musica,
      titulo: item.titulo,
      trecho_letra: LetraUtil.limparLetra(item.trecho_letra),
      tonalidades: filterFalsy(JSON.parse(item.tonalidades) ?? []),
      momentos: filterFalsy(JSON.parse(item.momentos) ?? []),
    };
  }

  static async findItensRepertorio(repertorio: Repertorio): Promise<ItemRepertorio[]> {
    const query = /*sql*/ `
      SELECT id_item_repertorio, id_repertorio,
             termo, momento, tonalidade,
             musica.id_musica as musica_id_musica, musica.titulo as musica_titulo,
             substr(letra, 0, 70) as musica_trecho_letra,
             musica.informacoes ->> 'tonalidades' as musica_tonalidades,
             musica.informacoes ->> 'momentos' as musica_momentos
        FROM item_repertorio
        LEFT JOIN musica using (id_musica)
       WHERE id_repertorio = ?
    `;

    const response = await Banco.executeAsync(query, [repertorio.id_repertorio]);
    const rows = response.rows?._array ?? [];

    const rowById: {[key: number]: ItemRepertorio} = {};

    rows.forEach(it => {
      rowById[it.id_item_repertorio] = {
        id_item_repertorio: it.id_item_repertorio,
        id_repertorio: it.id_repertorio,

        musica: it.musica_id_musica && {
          id_musica: it.musica_id_musica,
          titulo: it.musica_titulo,
          trecho_letra: it.musica_trecho_letra && LetraUtil.limparLetra(it.musica_trecho_letra),
        },

        termo: it.termo,

        momento: {
          valor: QueryOperation.optionalValue(it.momento),
          // filterFalsy: https://gitlab.com/canta-igreja/canta-igreja/-/issues/26
          sugestoes: filterFalsy(JSON.parse(it.musica_momentos) ?? []),
        },
        tonalidade: {
          valor: QueryOperation.optionalValue(it.tonalidade),
          // filterFalsy: https://gitlab.com/canta-igreja/canta-igreja/-/issues/26
          sugestoes: filterFalsy(JSON.parse(it.musica_tonalidades) ?? []),
        },
      };
    });

    return repertorio.ordem_musicas.map(it => rowById[it]);
  }

  static async removerMusica(repertorio: Repertorio, item: ItemRepertorio): Promise<Repertorio> {
    const selectMusicas = /*sql*/ `
      DELETE FROM item_repertorio
       WHERE id_item_repertorio = ?
    `;

    const posicao: number = repertorio.ordem_musicas.indexOf(item.id_item_repertorio);
    const novaOrdem = ListUtil.removerElemento(repertorio.ordem_musicas, posicao);

    const repertorioAtualizado: Repertorio = {
      ...repertorio,
      ordem_musicas: novaOrdem,
    };

    const [repertorioPersistido, _] = await Promise.all([
      //
      this.updateResumoOrdemMusicas(repertorioAtualizado),
      Banco.executeAsync(selectMusicas, [item.id_item_repertorio]),
    ]);

    return repertorioPersistido;
  }

  static async reposicionarMusica(repertorio: Repertorio, posicaoAtual: number, posicaoNova: number) {
    const repertorioAtualizado = {
      ...repertorio,
      ordem_musicas: ListUtil.reposicionarElemento(repertorio.ordem_musicas, posicaoAtual, posicaoNova),
    };

    return await RepertoriosRepository.updateResumoOrdemMusicas(repertorioAtualizado);
  }

  static async updateItem(repertorio: Repertorio, item: ItemRepertorio): Promise<ItemRepertorioAtualizado> {
    let repertorioAtualizado = repertorio;

    const updateRepertorio = /*sql*/ `
      UPDATE item_repertorio
         SET momento = ?,
             tonalidade = ?,
             termo = ?
       WHERE id_item_repertorio = ?
    `;

    const undefinedIfBlank = (text: string | undefined) => {
      if (text === undefined) {
        return text;
      }
      text = text.trim();
      if (text === '') {
        return undefined;
      } else {
        return text;
      }
    };

    item.momento.valor = undefinedIfBlank(item.momento.valor);
    item.tonalidade.valor = undefinedIfBlank(item.tonalidade.valor);

    const queries = [
      Banco.executeAsync(updateRepertorio, [
        item.momento.valor,
        item.tonalidade.valor,
        item.termo,
        item.id_item_repertorio,
      ]),
      this.updateInformacoesRecomendacoesMusica(item),
    ];

    await Promise.all(queries);

    if (item.musica === undefined || item.musica === null) {
      // NOTE: Executar somente depois de atualizar o item
      // Item pode ter sido renomeado
      repertorioAtualizado = await RepertoriosRepository.updateResumoOrdemMusicas(repertorio);
    } else {
      repertorioAtualizado = await RepertoriosRepository.updateDataEdicao(repertorio);
    }

    return {item, repertorio: repertorioAtualizado};
  }

  private static async updateInformacoesRecomendacoesMusica(item: ItemRepertorio) {
    if (item.musica === undefined || item.musica === null) {
      return;
    }

    const updateInformacoesMusica = /*sql*/ `
        UPDATE musica
           SET informacoes = json_patch(informacoes, ?)
         WHERE id_musica = ?
      `;

    const update = JSON.stringify({
      tonalidades: item.tonalidade.valor
        ? filterFalsy([
            item.tonalidade.valor.trim(),
            ...item.tonalidade.sugestoes.filter(it => it !== item.tonalidade.valor).map(it => it.trim()),
          ]).slice(0, 3)
        : item.tonalidade.sugestoes,
      momentos: item.momento.valor
        ? filterFalsy([
            item.momento.valor.trim(), //
            ...item.momento.sugestoes.filter(it => it !== item.momento.valor).map(it => it.trim()),
          ]).slice(0, 3)
        : item.momento.sugestoes,
    });

    return await Banco.executeAsync(updateInformacoesMusica, [update, item.musica?.id_musica]);
  }

  public static async updateDataEdicao(repertorio: Repertorio, dataEdicao?: number): Promise<Repertorio> {
    const now = Timestamp.now();
    const data = dataEdicao ?? now;

    const updateRepertorio = /*sql*/ `
     UPDATE repertorio
        SET data_edicao = ?
      WHERE id_repertorio=?
    `;

    await Banco.executeAsync(updateRepertorio, [data, repertorio.id_repertorio]);

    return {
      ...repertorio,
      data: {
        ...repertorio.data,
        edicao: data,
      },
    };
  }

  static async updateRepertorioSlug(repertorio: Repertorio, slug: RepertorioSlug): Promise<Repertorio> {
    const data = Timestamp.now();

    const updateRepertorio = /*sql*/ `
     UPDATE repertorio
        SET slug = ?,
            data_edicao = ?
      WHERE id_repertorio=?
    `;

    await Banco.executeAsync(updateRepertorio, [JSON.stringify(slug), data, repertorio.id_repertorio]);

    return {
      ...repertorio,
      slug: slug,
      data: {
        ...repertorio.data,
        edicao: data,
      },
    };
  }
}
