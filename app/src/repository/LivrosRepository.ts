import {EdicaoLivro} from 'src/model/Livro';
import {Banco} from '../arch/persistence/Banco';

export class LivrosRepository {
  static async findById(idEdicaoLivro: number): Promise<EdicaoLivro | undefined> {
    const livros = await this.findAllWithWhere('id_edicao_livro = ?', [idEdicaoLivro]);

    if (livros.length === 1) {
      return livros[0];
    }
    return undefined;
  }

  static async findAll(): Promise<EdicaoLivro[]> {
    return this.findAllWithWhere('1 = 1');
  }

  private static async findAllWithWhere(where: string, params?: any[]): Promise<EdicaoLivro[]> {
    const query = /*sql*/ `
      select id_livro, id_edicao_livro, edicao, editora.nome as editora, livro.titulo,
             edicao_livro.ano,
             edicao_livro.descricao,
             edicao_livro.site
        from livro
        join edicao_livro using (id_livro)
        join editora using (id_editora)
       where ${where}
       order by livro.titulo, edicao
    `;

    const result = await Banco.executeAsync(query, params);
    return result.rows?._array ?? [];
  }
}
