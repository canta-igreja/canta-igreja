import {
  ConfiguracoesMusica,
  EstiloRefrao,
  FonteMusica,
  TamanhoFonte,
} from '../store/ui/Configuracoes/Configuracoes.types';
import {MMKV} from 'react-native-mmkv';

enum ConfiguracoesUsuarioKeys {
  estiloRefrao = 'estiloRefrao',
  tamanho = 'tamanho',
  fonte = 'fonte',
}

export class ConfiguracoesUsuarioRepository {
  private static readonly KEY = '@cantaIgreja/configuracoes';

  private static readonly storage = new MMKV({
    id: ConfiguracoesUsuarioRepository.KEY,
  });

  private static readonly configuracoesPadrao: ConfiguracoesMusica = {
    [ConfiguracoesUsuarioKeys.estiloRefrao]: EstiloRefrao.NEGRITO,
    [ConfiguracoesUsuarioKeys.tamanho]: TamanhoFonte.NORMAL,
    [ConfiguracoesUsuarioKeys.fonte]: FonteMusica.PADRAO,
  };

  static async getConfiguracoes(): Promise<ConfiguracoesMusica> {
    const configuracoes: any = {};

    for (const key in ConfiguracoesUsuarioKeys) {
      const stateKey = key as keyof ConfiguracoesMusica;

      configuracoes[stateKey] =
        ConfiguracoesUsuarioRepository.storage.getString(stateKey) ?? this.configuracoesPadrao[stateKey];
    }

    return configuracoes;
  }

  static async persistir(configuracoes: ConfiguracoesMusica) {
    for (const [key, value] of Object.entries(configuracoes)) {
      ConfiguracoesUsuarioRepository.storage.set(key, value);
    }
  }
}
