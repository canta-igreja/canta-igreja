export interface MusicasQueryParams {
  idLivro?: number;
  termoBusca?: string;
}