import {IndiceMusica} from 'src/model/IndiceMusica';
import {InformacoesBasicasMusica, InformacoesDetalhadasMusica} from 'src/model/Musica';
import {MusicaItemListData} from 'src/view/component/list/data/MusicaItemListData';
import {RotuloMusica} from 'src/model/RotuloMusica';
import {Pagina} from 'src/model/paginacao/Pagina';
import {Banco} from '../../arch/persistence/Banco';
import {PreparedStatement} from '../../arch/persistence/sqlite/Statement';
import {Repertorio} from 'src/model/repertorio/Repertorio';
import {QueryOperation} from 'src/arch/persistence/sqlite/QueryOperation';

export type IndicesLivroByMusicaId = {[key: number]: {sigla?: string; indice: string; titulo: string}[] | undefined};

export class MusicasRepository {
  static findByIdStatement = new PreparedStatement(`
    select id_musica, titulo, letra, contribuidores, midias
      from musica
     where id_musica = ?
  `);

  static async findComDetalhesById(idMusica: number): Promise<InformacoesDetalhadasMusica | undefined> {
    const [musica, rotulos, indices] = await Promise.all([
      this.findById(idMusica),
      this.getRotulosMusica(idMusica),
      this.getIndicesLivrosMusica(idMusica),
    ]);

    if (musica === undefined) {
      return undefined;
    }

    return {
      ...musica,

      rotulos: rotulos,
      indices: indices,
    };
  }

  private static async getRotulosMusica(idMusica: number): Promise<RotuloMusica[]> {
    const query = `
      select id_rotulo_musica, id_rotulo, id_categoria_rotulo, rotulo.nome
        from rotulo_musica
        join rotulo using (id_rotulo)
        join categoria_rotulo using (id_categoria_rotulo)
       where id_musica = ?
    `;

    const result = await Banco.executeAsync(query, [idMusica]);

    return result.rows?._array ?? [];
  }

  public static async getIndicesLivrosFromRepertorio(repertorio: Repertorio): Promise<IndicesLivroByMusicaId> {
    const query = /*sql*/ `
      select item_repertorio.id_musica,
             cantico_livro.indice,
             edicao_livro.detalhes ->> 'sigla' as sigla,
             livro.titulo as titulo
        from cantico_livro
        join edicao_livro using (id_edicao_livro)
        join livro using (id_livro)
        join editora using (id_editora)
        join item_repertorio using (id_musica)
      where id_repertorio = ?
      order by livro.titulo
    `;

    const result = await Banco.executeAsync(query, [repertorio.id_repertorio]);

    const response = result.rows?._array ?? [];

    const indicesById: IndicesLivroByMusicaId = {};

    response.forEach(it => {
      if (indicesById[it.id_musica] === undefined) {
        indicesById[it.id_musica] = [];
      }

      indicesById[it.id_musica]?.push({
        indice: it.indice,
        sigla: it.sigla,
        titulo: it.titulo,
      });
    });

    return indicesById;
  }

  private static async getIndicesLivrosMusica(idMusica: number): Promise<IndiceMusica[]> {
    const query = /*sql*/ `
      select cantico_livro.id_musica, cantico_livro.id_edicao_livro, cantico_livro.indice,
             livro.titulo as titulo_livro,
             edicao_livro.edicao,
             editora.nome as editora
        from cantico_livro
        join edicao_livro using (id_edicao_livro)
        join livro using (id_livro)
        join editora using (id_editora)
      where id_musica = ?
      order by livro.titulo
    `;

    const result = await Banco.executeAsync(query, [idMusica]);

    const response = result.rows?._array ?? [];

    return response.map(it => ({
      id_edicao_livro: it.id_edicao_livro,
      id_musica: it.musica,

      indice: it.indice,
      edicao: it.edicao,
      editora: it.editora,
      titulo_livro: it.titulo_livro,
    }));
  }

  static async findById(idMusica: number): Promise<InformacoesBasicasMusica | undefined> {
    // https://github.com/OP-Engineering/op-sqlite/issues/37
    // await MusicasRepository.findByIdStatement.bind([idMusica]);
    // const result = await MusicasRepository.findByIdStatement.execute();

    const result = await Banco.executeAsync(this.findByIdStatement.query, [idMusica]);

    const itens = result.rows?._array ?? [];

    if (itens.length === 1) {
      const musica = itens[0];

      return {
        id_musica: musica.id_musica,
        letra: musica.letra,
        titulo: musica.titulo,

        contribuidores: JSON.parse(itens[0].contribuidores),
        midias: JSON.parse(itens[0].midias),
      };
    } else {
      return undefined;
    }
  }

  private getDetalhesTermoBusca(termoBusca: string | undefined): DetalhesTermoBusca {
    const termoBuscaTrimmed = termoBusca?.trim();
    const hasTermo = !(termoBusca === undefined || termoBuscaTrimmed?.length === 0);

    const isIndice = hasTermo && !isNaN(termoBuscaTrimmed as any);
    const isTexto = hasTermo && !isIndice;

    const indice = isIndice ? parseInt(termoBuscaTrimmed as string, 10) : undefined;
    const termo = isTexto
      ? (
          termoBuscaTrimmed
            ?.normalize('NFKD') // Ignorar acentuações
            .replace(/[^\w ]/g, '') // Retirar o que não é caractere ou número
            .replace(/\s+/gi, '* ') // Incluir "* " dentre as palavras
            .trim() + '*'
        ) // Retirar espaços iniciais e finais
          // Incluir asterisco no fim
          .replace(/\*\*/g, '*') // Retirar asteriscos duplicados por conta da inclusão do asterisco final
      : undefined;

    return {
      hasTermo: hasTermo,
      indice: indice,
      termo: termo,
    };
  }

  async findAllBy(
    termoBusca: string | undefined,
    idEdicaoLivro: number | undefined,
    paginaAtual: number,
    tamanho = 10,
  ): Promise<Pagina<MusicaItemListData>> {
    const detalhes = this.getDetalhesTermoBusca(termoBusca);

    let strategy: BuscaStrategy = BuscaStrategies.semTermo;

    if (!detalhes.hasTermo) {
      strategy = BuscaStrategies.semTermo;
    } else if (detalhes.indice) {
      strategy = BuscaStrategies.porIndice;
    } else if (detalhes.termo) {
      strategy = BuscaStrategies.porTermo;
    }

    // FIXME: Alterar paginação e ordenação por cantico_musica.ordem
    //        quando for baseada em livro
    //        e não houver uma busca no momento
    // FIXME: Corrigir buscas por livro quando se usa índice
    const query = `
        ${strategy.select(strategy.from_where(idEdicaoLivro), idEdicaoLivro)}
        LIMIT ${tamanho + 1} OFFSET ${paginaAtual * tamanho}
      `;
    const parametros: any[] = strategy.parametros(detalhes, idEdicaoLivro);

    const response = await Banco.executeAsync(query, parametros);

    const musicas = response.rows;

    const pagina: Pagina<MusicaItemListData> = {
      current: paginaAtual,
      size: tamanho,
      elements: (musicas?._array.slice(0, tamanho) ?? []).map(it => {
        const livro =
          it.indice && it.titulo_livro
            ? {
                indice: it.indice,
                sigla: QueryOperation.optionalValue(it.sigla),
                titulo: it.titulo_livro,
              }
            : undefined;

        return {
          id_musica: it.id_musica,
          titulo: it.titulo,
          trecho_letra: it.trecho_letra,
          livro: livro,
        };
      }),
      hasNextPage: (musicas?.length ?? 0) > tamanho,
    };

    return pagina;
  }
}

interface DetalhesTermoBusca {
  hasTermo: boolean;
  indice?: number;
  termo?: string;
}

interface BuscaStrategy {
  select: (from_where: string, idEdicaoLivro: number | undefined) => string;
  from_where: (idEdicaoLivro: number | undefined) => string;
  parametros: (detalhes: DetalhesTermoBusca, idEdicaoLivro: number | undefined) => any[];
}

class BuscaStrategies {
  static readonly semTermo: BuscaStrategy = {
    select: (from_where, idEdicaoLivro) => {
      const indice = idEdicaoLivro ? 'indice' : 'null';
      const order = idEdicaoLivro ? 'ordem, cast(indice as integer)' : 'titulo, id_musica'; // COLLATE NOCASE desc
      return `
          SELECT id_musica, ${indice}, titulo, substr(letra, 0, 70) as trecho_letra
          ${from_where}
          ORDER BY ${order}
      `;
    },
    from_where: idEdicaoLivro => {
      if (idEdicaoLivro) {
        return `
         from musica
         join cantico_livro using (id_musica)
        where id_edicao_livro = ?`;
      } else {
        return 'from musica';
      }
    },
    parametros: (_, idEdicaoLivro) => {
      return idEdicaoLivro ? [idEdicaoLivro] : [];
    },
  };

  static readonly porIndice: BuscaStrategy = {
    select: from_where => `
      SELECT id_musica, indice, livro.titulo as titulo_livro, edicao_livro.detalhes ->> 'sigla' as sigla, musica.titulo as titulo, substr(letra, 0, 70) as trecho_letra
       ${from_where}
      ORDER BY livro.titulo, indice
    `,
    from_where: id_edicao_livro => {
      const whereLivro = id_edicao_livro ? 'and id_edicao_livro = ?' : '';

      return `
        from musica
        join cantico_livro using (id_musica)
        join edicao_livro using (id_edicao_livro)
        join livro using (id_livro)
        where indice = ?
              ${whereLivro}
      `;
    },
    parametros: (detalhes, idEdicaoLivro) => [detalhes.indice, ...(idEdicaoLivro ? [idEdicaoLivro] : [])],
  };

  static readonly porTermo: BuscaStrategy = {
    select: (from_where, idEdicaoLivro) => {
      const indice = idEdicaoLivro ? 'indice' : 'null';

      return `
        SELECT musica_busca.rowid as id_musica,
               ${indice} as indice,
               snippet(musica_busca, 0, '||', '||', '', 12) as titulo,
               snippet(musica_busca, 1, '||', '||', '', 12) as trecho_letra
         ${from_where}
        --ORDER BY rank
         ORDER BY bm25(musica_busca, 50, 5)
      `;
    },
    from_where: idEdicaoLivro => {
      if (idEdicaoLivro) {
        return `
         FROM musica_busca
         JOIN cantico_livro on (musica_busca.rowid = cantico_livro.id_musica)
        WHERE musica_busca MATCH ?
          AND id_edicao_livro = ?
        `;
      } else {
        return `
         FROM musica_busca
        WHERE musica_busca MATCH ?
        `;
      }
    },
    parametros: (detalhes, idEdicaoLivro) => (idEdicaoLivro ? [detalhes.termo, idEdicaoLivro] : [detalhes.termo]),
  };
}
