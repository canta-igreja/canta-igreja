import {MMKV} from 'react-native-mmkv';

export class ConfiguracaoAplicacao<T extends string | number | boolean | Uint8Array> {
  private constructor(
    public readonly chave: string,
    public readonly valorPadrao: T,
  ) {}

  public static readonly ONBOARDING_MODAL = new ConfiguracaoAplicacao('onboardingModal', false);
}

export class ConfiguracoesAplicacaoRepository {
  private static readonly KEY = '@cantaIgreja/configuracoes/aplicacao';

  private static readonly storage = new MMKV({
    id: ConfiguracoesAplicacaoRepository.KEY,
  });

  static getBoolean(configuracao: ConfiguracaoAplicacao<boolean>): boolean {
    return this.storage.getBoolean(configuracao.chave) ?? configuracao.valorPadrao;
  }

  static async persistir<T extends string | number | boolean | Uint8Array>(
    configuracao: ConfiguracaoAplicacao<T>,
    valor: T,
  ) {
    this.storage.set(configuracao.chave, valor);
  }
}
