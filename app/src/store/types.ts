export enum EstadoRequisicao {
  /** Aguardando a requisição */
  OCIOSO = 'ocioso',
  /** Requisição em andamento */
  CARREGANDO = 'carregando',
  /** Requisição terminou com êxito */
  BEM_SUCEDIDO = 'bem_sucedido',
  /** Requisição terminou com falha*/
  FALHOU = 'falhou',
}
