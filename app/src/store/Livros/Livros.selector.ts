import {RootState} from 'src/store/store.types';
import {EstadoRequisicao} from '../types';
import {EdicaoLivro} from 'src/model/Livro';
import {livrosAdapter} from './Livros.reducer';

const {selectById} = livrosAdapter.getSelectors<RootState>(state => state.livros);

export class LivrosSelect {
  static readonly isCarregando = (it: RootState) =>
    it.livros.estado === EstadoRequisicao.OCIOSO || it.livros.estado === EstadoRequisicao.CARREGANDO;

  static readonly allIds = (it: RootState) => it.repertorios.ids;
  static readonly byId = (idLivro: number) => (it: RootState) => selectById(it, idLivro) as EdicaoLivro | undefined;
}
