import {createEntityAdapter, createSlice} from '@reduxjs/toolkit';
import {EdicaoLivro} from 'src/model/Livro';
import {EstadoRequisicao} from '../types';
import {LivrosAction} from './Livros.action';
import {LivrosState} from './Livros.types';

export const livrosAdapter = createEntityAdapter({
  selectId: (livro: EdicaoLivro) => livro.id_livro,
});

const initialState = livrosAdapter.getInitialState<LivrosState>({
  estado: EstadoRequisicao.OCIOSO,
});

export const livrosSlice = createSlice({
  name: 'Livros',
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(LivrosAction.carregar.pending, state => {
        state.estado = EstadoRequisicao.CARREGANDO;
      })
      .addCase(LivrosAction.carregar.fulfilled, (state, action) => {
        state.estado = EstadoRequisicao.BEM_SUCEDIDO;
        livrosAdapter.setAll(state, action.payload);
      })
      .addCase(LivrosAction.carregar.rejected, state => {
        state.estado = EstadoRequisicao.FALHOU;
      })
      .addCase(LivrosAction.carregarLivro.fulfilled, livrosAdapter.addOne);
  },
});
