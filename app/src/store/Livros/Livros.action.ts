import {createAsyncThunk} from '@reduxjs/toolkit';
import {LivrosRepository} from 'src/repository/LivrosRepository';
import {LivrosSelect} from './Livros.selector';
import {createAppAsyncThunk} from '../store.types';

export class LivrosAction {
  static readonly carregar = createAsyncThunk('Livros/carregar', async (it, {rejectWithValue}) => {
    try {
      return await LivrosRepository.findAll();
    } catch (e) {
      console.error(e);
      return rejectWithValue(e);
    }
  });

  static readonly carregarLivro = createAppAsyncThunk(
    'Livros/carregarLivro',
    async (idLivro: number, {rejectWithValue}) => {
      try {
        const response = await LivrosRepository.findById(idLivro);
        if (response === undefined) {
          return rejectWithValue('not found');
        } else {
          return response;
        }
      } catch (e) {
        return rejectWithValue(e);
      }
    },
    {
      condition: (idLivro: number, {getState}) => {
        const livro = LivrosSelect.byId(idLivro)(getState());
        return livro === undefined;
      },
    },
  );
}
