import {createEntityAdapter, createSlice} from '@reduxjs/toolkit';
import {EstadoRequisicao} from '../types';
import {RepertoriosState} from './Repertorios.types';
import {RepertoriosAction} from './Repertorios.action';
import {Repertorio} from 'src/model/repertorio/Repertorio';
import {ItensRepertorioAction} from '../ItensRepertorio/ItensRepertorio.action';
import {ListUtil} from 'src/util/ListUtil';

export const repertoriosAdapter = createEntityAdapter({
  selectId: (repertorio: Repertorio) => repertorio.id_repertorio,
  // NOTE: Maiores ids indicam repertórios mais antigos
  // FIXME: Como tratar outros casos de ordenação? Ex: últimos acessados?
  sortComparer: (a, b) => (a.id_repertorio > b.id_repertorio ? -1 : 1),
});

const initialState = repertoriosAdapter.getInitialState<RepertoriosState>({
  estado: EstadoRequisicao.OCIOSO,
});

export const repertoriosSlice = createSlice({
  name: 'Repertorios',
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(RepertoriosAction.carregar.pending, state => {
        state.estado = EstadoRequisicao.CARREGANDO;
      })
      .addCase(RepertoriosAction.carregar.fulfilled, (state, action) => {
        state.estado = EstadoRequisicao.BEM_SUCEDIDO;
        repertoriosAdapter.setAll(state, action.payload);
      })
      .addCase(RepertoriosAction.carregar.rejected, state => {
        state.estado = EstadoRequisicao.FALHOU;
      })

      .addCase(RepertoriosAction.criar.fulfilled, (state, action) => {
        repertoriosAdapter.addOne(state, action.payload);
      })
      .addCase(RepertoriosAction.renomear.fulfilled, (state, action) => {
        state.entities[action.payload.id_repertorio].titulo = action.payload.titulo;
      })
      .addCase(RepertoriosAction.excluirRepertorio.fulfilled, (state, action) => {
        repertoriosAdapter.removeOne(state, action.meta.arg.id_repertorio);
      })

      .addCase(RepertoriosAction.compartilhar.fulfilled, (state, action) => {
        if (action.payload) {
          repertoriosAdapter.setOne(state, action.payload);
        }
      })
      .addCase(RepertoriosAction.importar.fulfilled, (state, action) => {
        repertoriosAdapter.setOne(state, action.payload);
      })

      // Itens do repertório
      .addCase(RepertoriosAction.incluirItem.fulfilled, (state, action) => {
        state.entities[action.payload.repertorio.id_repertorio] = {
          ...action.payload.repertorio,
        };
      })
      .addCase(RepertoriosAction.removerItem.fulfilled, (state, action) => {
        state.entities[action.payload.id_repertorio] = {
          ...action.payload,
        };
      })
      .addCase(ItensRepertorioAction.atualizarItem.fulfilled, (state, action) => {
        state.entities[action.payload.item.id_repertorio] = {
          ...action.payload.repertorio,
        };
      })
      .addCase(ItensRepertorioAction.reposicionarItem.pending, (state, action) => {
        const repertorio = state.entities[action.meta.arg.repertorio.id_repertorio];

        repertorio.ordem_musicas = ListUtil.reposicionarElemento(
          repertorio.ordem_musicas,
          action.meta.arg.posicaoOrigem,
          action.meta.arg.posicaoDestino,
        );
      })
      .addCase(ItensRepertorioAction.reposicionarItem.fulfilled, (state, action) => {
        const repertorio = state.entities[action.meta.arg.repertorio.id_repertorio];
        repertorio.resumo = action.payload.resumo;
      });
  },
});
