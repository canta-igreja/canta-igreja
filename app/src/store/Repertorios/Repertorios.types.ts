import {EstadoRequisicao} from '../types';

export interface RepertoriosState {
  estado: EstadoRequisicao;
}
