import {Repertorio} from 'src/model/repertorio/Repertorio';
import {RootState} from 'src/store/store.types';
import {EstadoRequisicao} from '../types';
import {repertoriosAdapter} from './Repertorios.reducer';

const {selectById} = repertoriosAdapter.getSelectors<RootState>(state => state.repertorios);

export class RepertoriosSelect {
  static readonly isCarregando = (it: RootState) =>
    it.repertorios.estado === EstadoRequisicao.OCIOSO || it.repertorios.estado === EstadoRequisicao.CARREGANDO;

  static readonly allIds = (it: RootState) => it.repertorios.ids;
  static readonly byId = (idRepertorio: number) => (it: RootState) =>
    selectById(it, idRepertorio) as Repertorio | undefined;

  static readonly itensById = (idRepertorio: number) => (it: RootState) =>
    selectById(it, idRepertorio)?.ordem_musicas ?? [];
}
