import {createAsyncThunk} from '@reduxjs/toolkit';
import * as Sentry from '@sentry/react-native';
import {openLink} from 'src/arch/util/linking';
import {RepertorioImportavelDTO} from 'src/model/api/RepertorioRest';
import {ReferenciaMusica} from 'src/model/Musica';
import {ItemRepertorio, ReferenciaRepertorioSlug, Repertorio, RepertorioSlug} from 'src/model/repertorio/Repertorio';
import {BibliotecaRepository} from 'src/repository/BibliotecaRepository';
import {ItemRepertorioAtualizado, RepertoriosRepository} from 'src/repository/RepertoriosRepository';
import {CompartilharService} from 'src/service/CompartilharService';
import {RepertorioService} from 'src/service/RepertorioService';
import {Base64Unicode} from 'src/util/Base64Unicode';
import {ListUtil} from 'src/util/ListUtil';
import {Timestamp} from 'src/util/Timestamp';
import {ItensRepertorioAction} from '../ItensRepertorio/ItensRepertorio.action';
import {ItensRepertorioSelect} from '../ItensRepertorio/ItensRepertorio.selector';
import {createAppAsyncThunk} from '../store.types';
import {RepertoriosSelect} from './Repertorios.selector';
import {Toast} from 'src/arch/util/Toast';

type IncluirItemType = {
  repertorio: Repertorio;
  termo?: string;
  musica?: ReferenciaMusica;
};
type RemoverMusicaType = {
  repertorio: Repertorio;
  item: ItemRepertorio;
};

type RenomearRepertorioParams = {repertorio: Repertorio; novoTitulo: string};

export class RepertoriosAction {
  static readonly carregar = createAsyncThunk('Repertorios/carregar', async (_, {rejectWithValue}) => {
    try {
      return await RepertoriosRepository.findRepertorios();
    } catch (e) {
      console.error(e);
      return rejectWithValue('Falhou');
    }
  });
  static readonly criar = createAsyncThunk(
    'Repertorios/criar',
    async ({nome, slug}: {nome: string; slug?: RepertorioSlug}, {rejectWithValue}) => {
      try {
        return await RepertoriosRepository.createRepertorio(nome, slug);
      } catch (e) {
        console.error(e);
        return rejectWithValue('Falhou');
      }
    },
  );
  static readonly excluirRepertorio = createAsyncThunk(
    'Repertorios/excluir',
    async (repertorio: Repertorio, {rejectWithValue}) => {
      try {
        return await RepertoriosRepository.deactivateRepertorio(repertorio);
      } catch (e) {
        console.error(e);
        return rejectWithValue(e);
      }
    },
  );

  static readonly renomear = createAsyncThunk(
    'Repertorios/renomear',
    async ({repertorio, novoTitulo}: RenomearRepertorioParams, {rejectWithValue}) => {
      try {
        return await RepertoriosRepository.renameRepertorio(repertorio, novoTitulo);
      } catch (e) {
        console.error(e);
        return rejectWithValue(e);
      }
    },
  );

  // Itens
  static readonly incluirItem = createAsyncThunk(
    'Repertorios/incluirItem',
    async ({repertorio, musica, termo}: IncluirItemType, {rejectWithValue}) => {
      try {
        if (musica) {
          return await RepertoriosRepository.addMusica(repertorio, musica);
        } else if (termo) {
          return await RepertoriosRepository.addTermo(repertorio, termo);
        } else {
          return rejectWithValue('Item desconhecido');
        }
      } catch (e) {
        console.error(e);
        return rejectWithValue('Falhou');
      }
    },
  );
  static readonly removerItem = createAsyncThunk(
    'Repertorios/removerItem',
    async ({repertorio, item}: RemoverMusicaType, {rejectWithValue}) => {
      try {
        return await RepertoriosRepository.removerMusica(repertorio, item);
      } catch (e) {
        console.error(e);
        return rejectWithValue('Falhou');
      }
    },
  );

  static readonly compartilhar = createAppAsyncThunk(
    'Repertorio/compartilhar',
    async (repertorio: Repertorio, {getState, rejectWithValue}) => {
      try {
        const idItens = RepertoriosSelect.itensById(repertorio.id_repertorio)(getState());

        const itens = idItens
          .map(it => ItensRepertorioSelect.byId(it)(getState()))
          .filter(it => it !== undefined) as ItemRepertorio[];

        const repertorioCompartilhado = await new CompartilharService().repertorio(repertorio, itens);

        if (repertorioCompartilhado && repertorio.slug?.corrente.repertorio !== repertorioCompartilhado.slug) {
          const corrente: ReferenciaRepertorioSlug = {
            conta: repertorioCompartilhado.conta.slug,
            repertorio: repertorioCompartilhado.slug,
          };
          const referencia: ReferenciaRepertorioSlug = repertorio.slug?.referencia ?? corrente;
          return await RepertoriosRepository.updateRepertorioSlug(repertorio, {referencia, corrente});
        }

        return undefined;
      } catch (e) {
        Toast.show('Não foi possível compartilhar repertório. Seu celular está com internet?');
        console.error(e);
        Sentry.captureException(e);
        return rejectWithValue('Falhou');
      }
    },
  );

  static readonly gerarFolheto = createAppAsyncThunk(
    'Repertorio/gerarFolheto',
    async (repertorio: Repertorio, {getState, rejectWithValue}) => {
      try {
        const idItens = RepertoriosSelect.itensById(repertorio.id_repertorio)(getState());

        const itens = idItens
          .map(it => ItensRepertorioSelect.byId(it)(getState()))
          .filter(it => it !== undefined) as ItemRepertorio[];

        const repertorioExportavel = new RepertorioService().convertToQuery(repertorio, itens);
        const query = Base64Unicode.encode(repertorioExportavel);

        openLink(`https://livreto.canta.app/?repertorio=${query}`);
      } catch (e) {
        console.error(e);
        return rejectWithValue('Falhou');
      }
    },
  );

  static readonly importar = createAppAsyncThunk(
    'Repertorio/importar',
    async (repertorioImportavel: RepertorioImportavelDTO, {dispatch, rejectWithValue}) => {
      try {
        const musicas = ListUtil.toMap(repertorioImportavel.musicas, it => it.id_musica);
        await BibliotecaRepository.atualizarMusicasSeNecessario(repertorioImportavel.musicas);

        const referencia: ReferenciaRepertorioSlug = {
          conta: repertorioImportavel.repertorio.conta.slug,
          repertorio: repertorioImportavel.repertorio.slug,
        };
        let repertorio = await dispatch(
          RepertoriosAction.criar({
            nome: repertorioImportavel.repertorio.titulo,
            slug: {referencia, corrente: referencia},
          }),
        ).unwrap();

        for (const item of repertorioImportavel.repertorio.itens) {
          const musica: ReferenciaMusica | undefined = item.id_musica ? musicas[item.id_musica] : undefined;

          let response = (await dispatch(RepertoriosAction.incluirItem({repertorio, musica, termo: item.termo})))
            .payload as ItemRepertorioAtualizado;

          const itemRepertorio = response.item;
          repertorio = response.repertorio;

          if (item.momento || item.tonalidade) {
            const itemAtualizado: ItemRepertorio = {
              ...itemRepertorio,
              momento: {
                ...itemRepertorio.momento,
                valor: item.momento,
              },
              tonalidade: {
                ...itemRepertorio.tonalidade,
                valor: item.tonalidade,
              },
            };
            response = (await dispatch(ItensRepertorioAction.atualizarItem(itemAtualizado)))
              .payload as ItemRepertorioAtualizado;
            repertorio = response.repertorio;
          }
        }

        const ultimaAtualizacao =
          repertorioImportavel.repertorio.atualizado_em ?? repertorioImportavel.repertorio.criado_em;
        return await RepertoriosRepository.updateDataEdicao(repertorio, Timestamp.fromDateString(ultimaAtualizacao));
      } catch (e) {
        console.error(e);
        return rejectWithValue('Falhou');
      }
    },
  );
}
