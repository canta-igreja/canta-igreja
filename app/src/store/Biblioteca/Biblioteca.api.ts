import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import {Obra} from 'src/model/obra/Obra';
import {BibliotecaRepository} from 'src/repository/BibliotecaRepository';
import {Api} from 'src/util/Api';
import {ObraItemListData, SituacaoItemObra} from 'src/view/component/list/data/ObraItemListData';

export const BibliotecaApi = createApi({
  baseQuery: fetchBaseQuery({baseUrl: Api.APP_BIBLIOTECA}),
  refetchOnReconnect: false,

  endpoints: builder => ({
    getObras: builder.query<Obra[], void>({
      query: () => ({
        url: '/obras',
        method: 'GET',
      }),
    }),

    getItensObras: builder.query<ObraItemListData[], void>({
      async queryFn(_arg, _queryApi, _extraOptions, fetchWithBQ) {
        const [obrasResponse, exemplares] = await Promise.all([
          fetchWithBQ('/obras'),
          await BibliotecaRepository.findExemplaresAtivos(),
        ]);

        if (obrasResponse.error) {
          return {error: obrasResponse.error};
        }

        const obras = obrasResponse.data as Obra[];
        const itens: ObraItemListData[] = obras.map(it => {
          let situacao = SituacaoItemObra.ATUALIZADA;
          if (!(it.id_obra in exemplares)) {
            situacao = SituacaoItemObra.NAO_BAIXADA;
          } else if (exemplares[it.id_obra]?.crc32 !== it.crc32) {
            situacao = SituacaoItemObra.NAO_ATUALIZADA;
          }

          return {
            obra: it,
            situacao: situacao,
          };
        });

        return {data: itens.filter(it => it.obra.disponivel)};
      },
    }),
  }),
});
