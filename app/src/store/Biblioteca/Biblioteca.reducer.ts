import {createSlice} from '@reduxjs/toolkit';
import {EstadoRequisicao} from '../types';
import {BibliotecaState} from './Biblioteca.types';
import {BibliotecaAction} from './Biblioteca.action';

const initialState: BibliotecaState = {
  estado: EstadoRequisicao.OCIOSO,
};

export const bibliotecaSlice = createSlice({
  name: 'obras',
  initialState,
  reducers: {},

  extraReducers(builder) {
    builder
      .addCase(BibliotecaAction.baixarObra.pending, (state, action) => {
        state.obraSendoBaixada = action.meta.arg;
        state.estado = EstadoRequisicao.CARREGANDO;
      })
      .addCase(BibliotecaAction.baixarObra.fulfilled, state => {
        state.obraSendoBaixada = undefined;
        state.estado = EstadoRequisicao.BEM_SUCEDIDO;
      })
      .addCase(BibliotecaAction.baixarObra.rejected, (state, action) => {
        state.obraSendoBaixada = undefined;
        state.estado = EstadoRequisicao.FALHOU;
        state.erros = action.error.message;
      });
  },
});
