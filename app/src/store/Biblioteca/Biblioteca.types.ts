import {Obra} from 'src/model/obra/Obra';
import {EstadoRequisicao} from '../types';

export interface BibliotecaState {
  estado: EstadoRequisicao;
  erros?: string;
  obraSendoBaixada?: Obra;
}
