import {createAsyncThunk} from '@reduxjs/toolkit';
import {Obra} from 'src/model/obra/Obra';
import {BibliotecaLink, BibliotecaService} from 'src/service/BibliotecaService';
import {Notification} from 'src/util/Notification';
import {BibliotecaRepository} from '../../repository/BibliotecaRepository';
import {ObraService} from '../../service/ObraService';
import {wait} from 'src/arch/util/delay';

export class BibliotecaAction {
  static readonly baixarObras = createAsyncThunk(
    'biblioteca/obras/download/all',
    async (obras: Obra[], {rejectWithValue, dispatch}) => {
      if (obras.length === 0) {
        return;
      }

      const notification = await Notification.startProgress('upload', 'Atualizando músicas', obras.length);

      let erro = false;
      try {
        for (const obra of obras) {
          notification.next(`Atualizando músicas - ${obra.titulo}`);
          const response = await dispatch(BibliotecaAction.baixarObra(obra));

          erro = erro || response.meta.requestStatus === 'rejected';

          await wait(2 * 1000);
        }
        if (!erro) {
          notification.finish('Músicas atualizadas');
        } else {
          const mensagem = 'Não foi possível atualizar as músicas no momento';
          notification.finish(mensagem);
          return rejectWithValue(mensagem);
        }
      } catch (e) {
        console.error(e);
        return rejectWithValue('Falhou');
      }
    },
  );

  static readonly baixarObra = createAsyncThunk('biblioteca/obras/download', async (obra: Obra, {rejectWithValue}) => {
    try {
      const path = await new ObraService().baixarObra(obra);
      await BibliotecaRepository.importarObra(obra, path);
      return path;
    } catch (e) {
      console.error(e);
      return rejectWithValue('Falhou');
    }
  });

  static readonly abrirLink = createAsyncThunk('biblioteca/cnbb', async (link: BibliotecaLink, {rejectWithValue}) => {
    try {
      await new BibliotecaService().abrirLink(link);
      return true;
    } catch (e) {
      console.error(e);
      return rejectWithValue('Falhou');
    }
  });
}
