import {RootState} from 'src/store/store.types';

export class BibliotecaSelector {
  static readonly obraSendoBaixada = (it: RootState) => it.biblioteca.obraSendoBaixada;
}
