import {configureStore} from '@reduxjs/toolkit';

import {BibliotecaApi} from './Biblioteca/Biblioteca.api';
import {bibliotecaSlice} from './Biblioteca/Biblioteca.reducer';
import {livrosSlice} from './Livros/Livros.reducer';
import {musicasSlice} from './Musicas/Musicas.reducer';
import {itensRepertorioSlice} from './ItensRepertorio/ItensRepertorio.reducer';
import {repertoriosSlice} from './Repertorios/Repertorios.reducer';
import {configuracoesSlice} from './ui/Configuracoes/Configuracoes.reducer';
import {listaMusicasSlice} from './ui/ListaMusicas/ListaMusicas.reducer';
import {modalObrasSlice} from './ui/ModalObras/ModalObras.reducer';
import {modalImportarRepertorioState} from './ui/ModalImportarRepertorio/ModalImportarRepertorio.reducer';

export const store = configureStore({
  reducer: {
    // Modelos
    livros: livrosSlice.reducer,
    musicas: musicasSlice.reducer,
    musicasRepertorio: itensRepertorioSlice.reducer,
    repertorios: repertoriosSlice.reducer,
    biblioteca: bibliotecaSlice.reducer,

    // APIs
    [BibliotecaApi.reducerPath]: BibliotecaApi.reducer,

    // UIs
    ui_configuracoes: configuracoesSlice.reducer,
    ui_listaMusicas: listaMusicasSlice.reducer,
    ui_modalObras: modalObrasSlice.reducer,
    ui_modalImportarRepertorio: modalImportarRepertorioState.reducer,
  },

  middleware: gDM => gDM().concat(BibliotecaApi.middleware),
});
