import {createSlice} from '@reduxjs/toolkit';
import {ModalObrasAction} from './ModalObras.action';
import {ModalObrasState} from './ModalObras.types';

const initialState: ModalObrasState = {
  aberto: false,
};

export const modalObrasSlice = createSlice({
  name: 'ui_modalObras',
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(ModalObrasAction.abrir, state => {
        state.aberto = true;
      })
      .addCase(ModalObrasAction.fechar, state => {
        state.aberto = false;
      });
  },
});
