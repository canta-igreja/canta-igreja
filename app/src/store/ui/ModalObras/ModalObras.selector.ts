import {RootState} from 'src/store/store.types';

export class ModalObrasSelect {
  static readonly isAberto = (it: RootState) => it.ui_modalObras.aberto;
}
