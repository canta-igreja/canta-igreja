import {createAction} from '@reduxjs/toolkit';

export class ModalObrasAction {
  static readonly abrir = createAction('Ui/ModalObras/abrir');
  static readonly fechar = createAction('Ui/ModalObras/fechar');
}
