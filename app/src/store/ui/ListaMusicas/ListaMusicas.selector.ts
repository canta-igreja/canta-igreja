import {RootState} from 'src/store/store.types';
import {EstadoRequisicao} from '../../types';
import {listaMusicasAdapter} from './ListaMusicas.reducer';
import {MusicaItemListData} from 'src/view/component/list/data/MusicaItemListData';
import {ListaMusicasWindow} from './ListaMusicas.types';

const {selectById} = listaMusicasAdapter.getSelectors<RootState>(state => state.ui_listaMusicas);

export class ListaMusicasSelect {
  static readonly window = (window: ListaMusicasWindow) => (it: RootState) => it.ui_listaMusicas.windows[window];

  static readonly allIds = (window: ListaMusicasWindow) => (it: RootState) => ListaMusicasSelect.window(window)(it).ids;
  static readonly byId = (idMusica: number) => (it: RootState) =>
    selectById(it, idMusica) as MusicaItemListData | undefined;

  static readonly estadoRequisicao = (window: ListaMusicasWindow) => (it: RootState) =>
    ListaMusicasSelect.window(window)(it).estado;
  static readonly termoBuscado = (window: ListaMusicasWindow) => (it: RootState) =>
    ListaMusicasSelect.window(window)(it).parametros.termoBusca;
  static readonly isFinalResultados = (window: ListaMusicasWindow) => (it: RootState) =>
    ListaMusicasSelect.window(window)(it).pagina.isUltima;

  // PaginacaoMusicasView
  static readonly hasAnyMusica = (window: ListaMusicasWindow) => (it: RootState) =>
    ListaMusicasSelect.allIds(window)(it).length > 0;
  static readonly isCarregando = (window: ListaMusicasWindow) => (it: RootState) =>
    ListaMusicasSelect.estadoRequisicao(window)(it) === EstadoRequisicao.CARREGANDO;
  static readonly isPaginaInicial = (window: ListaMusicasWindow) => (it: RootState) =>
    ListaMusicasSelect.window(window)(it).pagina.atual === 0;
  static readonly hasPaginasACarregar = (window: ListaMusicasWindow) => (it: RootState) =>
    !ListaMusicasSelect.isFinalResultados(window)(it);
}
