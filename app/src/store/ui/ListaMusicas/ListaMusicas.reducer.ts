import {createEntityAdapter, createSlice} from '@reduxjs/toolkit';
import {musicaAsItemListData} from 'src/model/Musica';
import {MusicasAction} from 'src/store/Musicas/Musicas.action';
import {MusicaItemListData} from 'src/view/component/list/data/MusicaItemListData';
import {EstadoRequisicao} from '../../types';
import {ListaMusicasAction} from './ListaMusicas.action';
import {
  ListaMusicasState,
  ListaMusicasWindow,
  criarListaMusicasWindows,
  getWindowByRepertorioPadrao,
} from './ListaMusicas.types';

export const listaMusicasAdapter = createEntityAdapter({
  selectId: (musica: MusicaItemListData) => musica.id_musica,
});

const initialState = listaMusicasAdapter.getInitialState<ListaMusicasState>({
  windows: criarListaMusicasWindows(),
});

export const listaMusicasSlice = createSlice({
  name: 'ui_listaMusicas',
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(ListaMusicasAction._pesquisar.pending, (state, action) => {
        const acao = action.meta.arg.acao;

        const isMudancaPagina = acao === 'paginaSeguinte';
        const window = state.windows[action.meta.arg.window];

        window.estado = EstadoRequisicao.CARREGANDO;
        window.erros = undefined;
        window.parametros = isMudancaPagina ? window.parametros : action.meta.arg.parametros!!;
        window.pagina.atual = isMudancaPagina ? window.pagina.atual : -1;
      })
      .addCase(ListaMusicasAction._pesquisar.fulfilled, (state, action) => {
        listaMusicasAdapter.upsertMany(state, action.payload.elements);

        const window = state.windows[action.meta.arg.window];
        window.ids =
          action.meta.arg.acao === 'buscar'
            ? action.payload.elements.map(it => it.id_musica)
            : [...window.ids, ...action.payload.elements.map(it => it.id_musica)];

        window.estado = EstadoRequisicao.BEM_SUCEDIDO;
        window.erros = undefined;
        window.pagina = {
          atual: action.payload.current,
          isUltima: !action.payload.hasNextPage,
        };
      })

      .addCase(ListaMusicasAction.carregarRepertorioPadrao.pending, (state, action) => {
        const window = state.windows[getWindowByRepertorioPadrao(action.meta.arg)];

        window.estado = EstadoRequisicao.CARREGANDO;
        window.erros = undefined;
        window.ids = [];
      })
      .addCase(ListaMusicasAction.carregarRepertorioPadrao.fulfilled, (state, action) => {
        const window = state.windows[getWindowByRepertorioPadrao(action.meta.arg)];

        window.estado = EstadoRequisicao.BEM_SUCEDIDO;
        listaMusicasAdapter.upsertMany(state, action.payload);
        window.ids = action.payload.map(it => it.id_musica);
      })

      .addCase(ListaMusicasAction.limpar, (state, action) => {
        const window = state.windows[action.payload];

        window.ids = [];
        window.estado = EstadoRequisicao.OCIOSO;
        window.erros = undefined;

        window.parametros = {};
        window.pagina = {
          atual: -1,
          isUltima: true,
        };
      })

      // Músicas favoritas
      .addCase(MusicasAction.setFavorita.fulfilled, (state, action) => {
        const idMusica = action.meta.arg.idMusica;
        const musica = action.payload.musica;

        // NOTE: É esperado que somente músicas já carregadas na base sejam definidas como favoritas
        if (musica) {
          const itemListData = musicaAsItemListData(musica);
          listaMusicasAdapter.upsertOne(state, itemListData);
        }

        const window = state.windows[ListaMusicasWindow.favoritas];

        window.ids = action.meta.arg.favorita
          ? [action.meta.arg.idMusica, ...window.ids.filter(it => it !== idMusica)]
          : window.ids.filter(it => it !== idMusica);
      });
  },
});
