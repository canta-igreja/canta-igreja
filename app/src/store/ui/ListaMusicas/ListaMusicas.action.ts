import {createAction} from '@reduxjs/toolkit';
import {MusicasRepository} from 'src/repository/musicas/MusicasRepository';
import {MusicasFavoritasRepository} from 'src/repository/repertorioPadrao/MusicasFavoritasRepository';
import {MusicasRecemVisualizadasRepository} from 'src/repository/repertorioPadrao/MusicasRecemVisualizadasRepository';
import {RepertorioPadrao} from 'src/store/ItensRepertorio/ItensRepertorio.types';
import {createAppAsyncThunk} from 'src/store/store.types';
import {MusicasQueryParams} from '../../../repository/musicas/MusicasQueryParams';
import {ListaMusicasSelect} from './ListaMusicas.selector';
import {ListaMusicasWindow} from './ListaMusicas.types';

type PesquisarType =
  | {
      window: ListaMusicasWindow;
      acao: 'buscar';
      parametros?: MusicasQueryParams;
      pagina: number;
    }
  | {
      window: ListaMusicasWindow;
      acao: 'paginaSeguinte';
      parametros?: MusicasQueryParams;
      pagina?: undefined;
    };

export class ListaMusicasAction {
  static readonly buscar = (window: ListaMusicasWindow, parametros: MusicasQueryParams) => {
    return ListaMusicasAction._pesquisar({
      window,
      acao: 'buscar',
      parametros,
      pagina: 0,
    });
  };

  static readonly paginaSeguinte = (window: ListaMusicasWindow) => {
    return ListaMusicasAction._pesquisar({window, acao: 'paginaSeguinte'});
  };

  static readonly _pesquisar = createAppAsyncThunk(
    'Ui/ListaMusicas/pesquisar',
    async ({window: windowName, parametros, acao, pagina}: PesquisarType, {rejectWithValue, getState}) => {
      const window = ListaMusicasSelect.window(windowName)(getState());

      const incremento = acao === 'paginaSeguinte' ? +1 : -1;
      const paginaBusca = pagina ?? window.pagina.atual + incremento;

      const {idLivro, termoBusca} = parametros ?? window.parametros;

      try {
        return idLivro
          ? await new MusicasRepository().findAllBy(termoBusca, idLivro, paginaBusca, 20)
          : await new MusicasRepository().findAllBy(termoBusca, undefined, paginaBusca, 20);
      } catch (e) {
        return rejectWithValue(e);
      }
    },
  );

  static readonly carregarRepertorioPadrao = createAppAsyncThunk(
    'Ui/ListaMusicas/carregarRepertorioPadrao',
    async (repertorioPadrao: RepertorioPadrao, {rejectWithValue}) => {
      const repository =
        repertorioPadrao === RepertorioPadrao.FAVORITOS
          ? new MusicasFavoritasRepository()
          : new MusicasRecemVisualizadasRepository();

      try {
        return await repository.findAll();
      } catch (e) {
        console.error(e);
        return rejectWithValue(e);
      }
    },
  );

  static readonly limpar = createAction<ListaMusicasWindow>('Ui/ListaMusicas/limpar');
}
