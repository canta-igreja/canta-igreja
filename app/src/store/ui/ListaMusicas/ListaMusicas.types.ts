import {RepertorioPadrao} from 'src/store/ItensRepertorio/ItensRepertorio.types';
import {MusicasQueryParams} from '../../../repository/musicas/MusicasQueryParams';
import {EstadoRequisicao} from '../../types';

// ------------------
// States
// ------------------

export enum ListaMusicasWindow {
  buscaMusicas = 'buscaMusicas',
  buscaMusicasParaRepertorio = 'buscaMusicasParaRepertorio',
  favoritas = 'favoritas',
  recemVisualizadas = 'recemVisualizadas',
}

interface Window {
  ids: number[];

  estado: EstadoRequisicao;
  erros?: string;

  /** Parâmetros da busca */
  parametros: MusicasQueryParams;
  pagina: {
    atual: number;
    isUltima: boolean;
  };
}

type WindowsType = {[key in ListaMusicasWindow]: Window};

export interface ListaMusicasState {
  windows: WindowsType;
}

export const criarListaMusicasWindows = () => {
  const windows: any = {};

  for (const key of Object.keys(ListaMusicasWindow)) {
    const window: Window = {
      ids: [],
      estado: EstadoRequisicao.OCIOSO,
      erros: undefined,

      parametros: {},
      pagina: {
        atual: -1,
        isUltima: true,
      },
    };

    windows[key] = window;
  }

  return windows as WindowsType;
};

export function getWindowByRepertorioPadrao(repertorioPadrao: RepertorioPadrao) {
  switch (repertorioPadrao) {
    case RepertorioPadrao.FAVORITOS:
      return ListaMusicasWindow.favoritas;

    case RepertorioPadrao.RECEM_VISUALIZADOS:
      return ListaMusicasWindow.recemVisualizadas;
  }
}
