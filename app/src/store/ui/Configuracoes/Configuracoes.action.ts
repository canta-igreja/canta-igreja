import {ConfiguracoesUsuarioRepository} from '../../../repository/ConfiguracoesUsuarioRepository';
import {createAppAsyncThunk} from 'src/store/store.types';

import {ConfiguracoesMusica, EstiloRefrao, FonteMusica, TamanhoFonte} from './Configuracoes.types';
import {ConfiguracoesSelect} from './Configuracoes.selector';

type ParteConfiguracao = {[key: string]: ConfiguracoesMusica[keyof ConfiguracoesMusica]};

export class ConfiguracoesAction {
  static readonly carregarConfiguracoes = createAppAsyncThunk(
    'Ui/Configuracoes/carregar',
    async (_, {rejectWithValue}) => {
      try {
        return await ConfiguracoesUsuarioRepository.getConfiguracoes();
      } catch (e) {
        return rejectWithValue(e);
      }
    },
    {
      condition: (_, {getState}) => {
        return !ConfiguracoesSelect.musica.iniciado(getState());
      },
    },
  );

  static readonly alterarFamiliaFonte = (it: FonteMusica) => ConfiguracoesAction._persistir({fonte: it});
  static readonly alterarTamanhoFonte = (it: TamanhoFonte) => ConfiguracoesAction._persistir({tamanho: it});
  static readonly alterarEstiloRefrao = (it: EstiloRefrao) => ConfiguracoesAction._persistir({estiloRefrao: it});

  static readonly _persistir = createAppAsyncThunk(
    'Ui/Configuracoes/persistir',
    async (it: ParteConfiguracao, {getState, rejectWithValue}) => {
      const {ui_configuracoes: configuracoes} = getState();

      const novasConfiguracoes: ConfiguracoesMusica = {
        ...configuracoes.musica,
        ...it,
      };

      try {
        await ConfiguracoesUsuarioRepository.persistir(novasConfiguracoes);
        return novasConfiguracoes;
      } catch (e) {
        return rejectWithValue(e);
      }
    },
  );
}
