import {EstadoRequisicao} from '../../types';

export enum TamanhoFonte {
  NORMAL = 'normal',
  MODERADO = 'moderado',
  GRANDE = 'grande',
}

export enum EstiloRefrao {
  NEGRITO = 'negrito',
  ITALICO = 'italico',
  CAIXA_ALTA = 'caixa alta',
}

export enum FonteMusica {
  PADRAO = 'normal',
  MONOESPACADA = 'monoespacada',
}

// ------------------
// States
// ------------------
export interface ConfiguracoesMusica {
  fonte: FonteMusica;
  tamanho: TamanhoFonte;
  estiloRefrao: EstiloRefrao;
}

export interface ConfiguracoesState {
  estadoRequisicaoCarregamentoInicial: EstadoRequisicao;
  musica: ConfiguracoesMusica;
}
