import {RootState} from 'src/store/store.types';
import {EstadoRequisicao} from '../../types';

class ConfiguracoesMusica {
  static readonly iniciado = (it: RootState) =>
    it.ui_configuracoes.estadoRequisicaoCarregamentoInicial !== EstadoRequisicao.OCIOSO;

  static readonly fonteMusica = (it: RootState) => it.ui_configuracoes.musica.fonte;
  static readonly tamanhoFonte = (it: RootState) => it.ui_configuracoes.musica.tamanho;
  static readonly estiloRefrao = (it: RootState) => it.ui_configuracoes.musica.estiloRefrao;
}

export class ConfiguracoesSelect {
  static readonly musica = ConfiguracoesMusica;
}
