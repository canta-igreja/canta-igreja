import {createSlice} from '@reduxjs/toolkit';
import {ConfiguracoesAction} from './Configuracoes.action';
import {ConfiguracoesState, EstiloRefrao, FonteMusica, TamanhoFonte} from './Configuracoes.types';
import {EstadoRequisicao} from '../../types';

const initialState: ConfiguracoesState = {
  estadoRequisicaoCarregamentoInicial: EstadoRequisicao.OCIOSO,

  musica: {
    fonte: FonteMusica.PADRAO,
    tamanho: TamanhoFonte.NORMAL,
    estiloRefrao: EstiloRefrao.NEGRITO,
  },
};

export const configuracoesSlice = createSlice({
  name: 'ui_configuracoes',
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(ConfiguracoesAction.carregarConfiguracoes.pending, state => {
        state.estadoRequisicaoCarregamentoInicial = EstadoRequisicao.CARREGANDO;
      })
      .addCase(ConfiguracoesAction.carregarConfiguracoes.fulfilled, (state, action) => {
        state.estadoRequisicaoCarregamentoInicial = EstadoRequisicao.BEM_SUCEDIDO;
        state.musica = action.payload;
      })

      .addCase(ConfiguracoesAction._persistir.fulfilled, (state, action) => {
        state.estadoRequisicaoCarregamentoInicial = EstadoRequisicao.BEM_SUCEDIDO;
        state.musica = action.payload;
      });
  },
});
