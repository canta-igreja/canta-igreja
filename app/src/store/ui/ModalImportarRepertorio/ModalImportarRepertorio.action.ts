import {createAction} from '@reduxjs/toolkit';
import {RepertorioImportavelDTO, SimilaridadeRepertorios} from 'src/model/api/RepertorioRest';
import {ReferenciaRepertorioSlug, Repertorio} from 'src/model/repertorio/Repertorio';
import {RepertorioService} from 'src/service/RepertorioService';
import {createAppAsyncThunk} from 'src/store/store.types';
import {Api} from 'src/util/Api';

export type ModalImportarRepertorioIniciarResponse = {
  repertorioImportavel?: RepertorioImportavelDTO;
  repertorioSimilar: {
    similaridade: SimilaridadeRepertorios;
    repertorio?: Repertorio;
  };
};

export class ModalImportarRepertorioAction {
  static readonly iniciar = createAppAsyncThunk(
    'ModalImportarRepertorio/iniciar',
    async (referenciaRepertorio: ReferenciaRepertorioSlug, {rejectWithValue}) => {
      try {
        const repertorioSimilar = await new RepertorioService().findRepertorioSimilar(referenciaRepertorio);

        if (repertorioSimilar.repertorio) {
          const response: ModalImportarRepertorioIniciarResponse = {
            repertorioSimilar: {
              similaridade: repertorioSimilar.similaridade,
              repertorio: repertorioSimilar.repertorio,
            },
          };

          return response;
        }

        const {error, data: repertorioImportavel} = await Api.getRepertorio(referenciaRepertorio);

        if (error || repertorioImportavel === undefined) {
          throw new Error(JSON.stringify(error));
        }

        const response: ModalImportarRepertorioIniciarResponse = {
          repertorioImportavel,
          repertorioSimilar: {
            similaridade: repertorioSimilar.similaridade,
            repertorio: repertorioSimilar.repertorio,
          },
        };

        return response;
      } catch (e) {
        console.error(e);
        return rejectWithValue('Falhou');
      }
    },
  );

  static readonly fechar = createAction('ModalImportarRepertorio/buscarMusicas');
}
