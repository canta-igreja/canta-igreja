import {RepertorioImportavelDTO, SimilaridadeRepertorios} from 'src/model/api/RepertorioRest';
import {EstadoRequisicao} from 'src/store/types';

export interface ModalImportarRepertorioState {
  estado: EstadoRequisicao;

  aberto: boolean;

  repertorioImportavel?: RepertorioImportavelDTO;
  repertorioSimilar?: {
    id?: number;
    similaridade: SimilaridadeRepertorios;
  };
}
