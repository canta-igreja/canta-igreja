import {RootState} from 'src/store/store.types';

export class ModalImportarRepertorioSelect {
  static readonly isAberto = (it: RootState) => it.ui_modalImportarRepertorio.aberto;
  static readonly repertorio = (it: RootState) => it.ui_modalImportarRepertorio.repertorioImportavel;
}
