import {createSlice} from '@reduxjs/toolkit';
import {EstadoRequisicao} from 'src/store/types';
import {ModalImportarRepertorioAction} from './ModalImportarRepertorio.action';
import {ModalImportarRepertorioState} from './ModalImportarRepertorio.types';

const initialState: ModalImportarRepertorioState = {
  estado: EstadoRequisicao.OCIOSO,
  aberto: false,
};

export const modalImportarRepertorioState = createSlice({
  name: 'ui_modalImportarRepertorio',
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(ModalImportarRepertorioAction.iniciar.pending, state => {
        state.aberto = true;
        state.estado = EstadoRequisicao.CARREGANDO;
      })
      .addCase(ModalImportarRepertorioAction.iniciar.fulfilled, (state, action) => {
        state.estado = EstadoRequisicao.BEM_SUCEDIDO;
        state.repertorioImportavel = action.payload.repertorioImportavel;
        state.repertorioSimilar = action.payload.repertorioSimilar;
      })
      .addCase(ModalImportarRepertorioAction.iniciar.rejected, state => {
        state.estado = EstadoRequisicao.FALHOU;
      })
      .addCase(ModalImportarRepertorioAction.fechar, state => {
        state.aberto = false;
        state.estado = EstadoRequisicao.OCIOSO;
        state.repertorioImportavel = undefined;
        state.repertorioSimilar = undefined;
      });
  },
});
