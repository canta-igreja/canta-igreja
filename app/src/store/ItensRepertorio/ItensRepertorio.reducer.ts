import {createEntityAdapter, createSlice} from '@reduxjs/toolkit';
import {ItemRepertorio} from 'src/model/repertorio/Repertorio';
import {EstadoRequisicao} from '../types';
import {ItensRepertorioAction} from './ItensRepertorio.action';
import {ItensRepertorioState} from './ItensRepertorio.types';
import {RepertoriosAction} from '../Repertorios/Repertorios.action';

export const itensRepertorioAdapter = createEntityAdapter({
  selectId: (repertorio: ItemRepertorio) => repertorio.id_item_repertorio,
});

const initialState = itensRepertorioAdapter.getInitialState<ItensRepertorioState>({
  estado: EstadoRequisicao.OCIOSO,
});

export const itensRepertorioSlice = createSlice({
  name: 'ItensRepertorio',
  initialState,
  reducers: {},

  extraReducers(builder) {
    builder
      .addCase(ItensRepertorioAction.carregar.pending, state => {
        state.estado = EstadoRequisicao.CARREGANDO;
      })
      .addCase(ItensRepertorioAction.carregar.fulfilled, (state, action) => {
        state.estado = EstadoRequisicao.BEM_SUCEDIDO;
        itensRepertorioAdapter.upsertMany(state, action.payload);
      })

      .addCase(ItensRepertorioAction.limpar, state => {
        state.estado = EstadoRequisicao.OCIOSO;
        itensRepertorioAdapter.removeAll(state);
      })

      .addCase(RepertoriosAction.incluirItem.fulfilled, (state, action) => {
        itensRepertorioAdapter.addOne(state, action.payload.item);
      })
      .addCase(ItensRepertorioAction.atualizarItem.fulfilled, (state, action) => {
        state.entities[action.payload.item.id_item_repertorio] = {
          ...action.payload.item,
        };
      });
  },
});
