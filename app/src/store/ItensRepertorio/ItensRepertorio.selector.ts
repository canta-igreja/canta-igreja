import {RootState} from 'src/store/store.types';
import {EstadoRequisicao} from '../types';
import {itensRepertorioAdapter} from './ItensRepertorio.reducer';
import {ItemRepertorio} from 'src/model/repertorio/Repertorio';

const {selectIds, selectById} = itensRepertorioAdapter.getSelectors<RootState>(state => state.musicasRepertorio);

export class ItensRepertorioSelect {
  static readonly isCarregando = (it: RootState) =>
    it.musicasRepertorio.estado === EstadoRequisicao.OCIOSO ||
    it.musicasRepertorio.estado === EstadoRequisicao.CARREGANDO;

  static readonly allIds = selectIds;
  static readonly byId = (id: number) => (it: RootState) => selectById(it, id) as ItemRepertorio | undefined;

  static readonly hasAnyMusica = (it: RootState) => ItensRepertorioSelect.allIds(it).length > 0;
}
