import {createAction, createAsyncThunk} from '@reduxjs/toolkit';
import {ItemRepertorio, Repertorio} from 'src/model/repertorio/Repertorio';
import {RepertoriosRepository} from 'src/repository/RepertoriosRepository';
import {RepertoriosSelect} from 'src/store/Repertorios/Repertorios.selector';
import {createAppAsyncThunk} from 'src/store/store.types';

type ReposicionarMusicaParams = {repertorio: Repertorio; posicaoOrigem: number; posicaoDestino: number};

export class ItensRepertorioAction {
  static readonly carregar = createAppAsyncThunk(
    'ItensRepertorio/carregar',
    async (idRepertorio: number, {rejectWithValue, getState}) => {
      const repertorio = RepertoriosSelect.byId(idRepertorio)(getState());

      // FIXME - Se repertório não for localizado, mandar um dispatch para tentar carregar ele em memória
      //       - Se ainda não existir, lançar erro
      if (repertorio === undefined) {
        return rejectWithValue('Repertório inexistente');
      }

      try {
        return await RepertoriosRepository.findItensRepertorio(repertorio);
      } catch (e) {
        console.error(e);
        return rejectWithValue(e);
      }
    },
  );

  static readonly limpar = createAction('ItensRepertorio/limpar');

  static readonly reposicionarItem = createAsyncThunk(
    'ItensRepertorio/reposicionar',
    async ({repertorio, posicaoOrigem, posicaoDestino}: ReposicionarMusicaParams, {rejectWithValue}) => {
      try {
        return await RepertoriosRepository.reposicionarMusica(repertorio, posicaoOrigem, posicaoDestino);
      } catch (e) {
        console.error(e);
        return rejectWithValue(e);
      }
    },
  );

  static readonly atualizarItem = createAppAsyncThunk(
    'ItensRepertorio/atualizarItem',
    async (item: ItemRepertorio, {rejectWithValue, getState}) => {
      const repertorio = RepertoriosSelect.byId(item.id_repertorio)(getState());

      // FIXME - Se repertório não for localizado, mandar um dispatch para tentar carregar ele em memória
      //       - Se ainda não existir, lançar erro
      if (repertorio === undefined) {
        return rejectWithValue('Repertório inexistente');
      }

      try {
        return await RepertoriosRepository.updateItem(repertorio, item);
      } catch (e) {
        console.error(e);
        return rejectWithValue(e);
      }
    },
  );
}
