import {EstadoRequisicao} from '../types';

export enum RepertorioPadrao {
  FAVORITOS = 'Músicas favoritas',
  RECEM_VISUALIZADOS = 'Recém visualizadas',
}

export interface ItensRepertorioState {
  estado: EstadoRequisicao;
}
