import {createEntityAdapter, createSlice} from '@reduxjs/toolkit';
import {MusicasAction} from './Musicas.action';
import {InformacoesDetalhadasMusica} from 'src/model/Musica';

export const musicasAdapter = createEntityAdapter({
  selectId: (musica: InformacoesDetalhadasMusica) => musica.id_musica,
});
const initialState = musicasAdapter.getInitialState();

export const musicasSlice = createSlice({
  name: 'musicas',
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder.addCase(MusicasAction.carregarMusica.fulfilled, musicasAdapter.addOne);
  },
});
