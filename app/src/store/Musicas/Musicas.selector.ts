import {InformacoesDetalhadasMusica} from 'src/model/Musica';
import {RootState} from '../store.types';
import {musicasAdapter} from './Musicas.reducer';

const {selectById} = musicasAdapter.getSelectors<RootState>(state => state.musicas);

export class MusicasSelect {
  static readonly byId = (idMusica: number) => (it: RootState) =>
    selectById(it, idMusica) as InformacoesDetalhadasMusica | undefined;
}
