import {createAction} from '@reduxjs/toolkit';
import {InformacoesDetalhadasMusica} from 'src/model/Musica';
import {MusicasRepository} from 'src/repository/musicas/MusicasRepository';
import {MusicaService} from 'src/service/MusicaService';
import {createAppAsyncThunk} from '../store.types';
import {MusicasSelect} from './Musicas.selector';

export class MusicasAction {
  static readonly carregarMusica = createAppAsyncThunk(
    'Musicas/carregarMusica',
    async (idMusica: number, {rejectWithValue}) => {
      try {
        const response = await MusicasRepository.findComDetalhesById(idMusica);
        if (response === undefined) {
          return rejectWithValue('not found');
        } else {
          return response;
        }
      } catch (e) {
        return rejectWithValue(e);
      }
    },
    {
      condition: (idMusica: number, {getState}) => {
        const musica = MusicasSelect.byId(idMusica)(getState());
        return musica === undefined;
      },
    },
  );

  static readonly marcarMusicaComoVisualizada = createAction(
    'Musicas/marcarMusicaComoVisualizada',
    (musica: InformacoesDetalhadasMusica) => {
      MusicaService.marcarAsVisualizada(musica);

      return {
        payload: {
          id_musica: musica.id_musica,
        },
      };
    },
  );

  static readonly setFavorita = createAppAsyncThunk(
    'Musicas/setFavoritada',
    (props: {idMusica: number; favorita: boolean}, {getState}) => {
      const musica = MusicasSelect.byId(props.idMusica)(getState());

      MusicaService.updateFavorita(props.idMusica, props.favorita);

      return {
        musica: musica,
        favorita: props.favorita,
      };
    },
  );
}
