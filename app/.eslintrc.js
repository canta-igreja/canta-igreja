module.exports = {
  extends: '@react-native',

  "plugins": [
    "react-native-unistyles"
  ],

  rules: {
    'react/react-in-jsx-scope': 'off',
    'react-native/no-unused-styles': 2,
    'react-native/split-platform-components': 2,
    'react-native/no-inline-styles': 2,
    // 'react-native/no-color-literals': 1,
    'react-native/no-single-element-style-arrays': 2,
    "react-native-unistyles/no-unused-styles": "warn",
    // "react-native-unistyles/sort-styles": [
    //   "warn",
    //   "asc",
    //   { "ignoreClassNames": false, "ignoreStyleProperties": false }
    // ],
  },
};
