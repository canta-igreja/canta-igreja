/**
 * Metro configuration
 * https://reactnative.dev/docs/metro
 */
const { getDefaultConfig, mergeConfig } = require('@react-native/metro-config');
const { withSentryConfig } = require("@sentry/react-native/metro");

// https://akveo.github.io/react-native-ui-kitten/docs/guides/improving-performance#metro-bundler
const MetroConfig = require('@ui-kitten/metro-config');

/**
 * Metro configuration
 * https://facebook.github.io/metro/docs/configuration
 *
 * @type {import('metro-config').MetroConfig}
 */
const evaConfig = {
  evaPackage: '@eva-design/eva',
  // Optional, but may be useful when using mapping customization feature.
  // customMappingPath: './custom-mapping.json',
  transformer: {
    getTransformOptions: async () => ({
      transform: {
        experimentalImportSupport: false,
        inlineRequires: true,
      },
    }),
  },
};

// module.exports = mergeConfig(getDefaultConfig(__dirname), evaConfig);
module.exports = async () => {
  let defaultConfig = await getDefaultConfig(__dirname);
  defaultConfig = withSentryConfig(defaultConfig, {
    annotateReactComponents: true,
  });

  return MetroConfig.create(evaConfig, defaultConfig);
};
