import madge from 'madge';

function build(output, circular) {
  madge(['index.js', 'src/'], {
    fileExtensions: ['js', 'jsx', 'ts', 'tsx'],
    tsConfig: '../tsconfig.json',
  })
    .then(res => res.image(output, circular))
    .then(result => {
      console.log(result.toString());
    });
}

build('output/files.svg', false);
build('output/circular.svg', true);
