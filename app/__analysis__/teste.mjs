/*
const {builtinModules} = require('module');
const ts = require('typescript');

const tsHost = ts.createCompilerHost(
  {
    allowJs: true,
    noEmit: true,
    isolatedModules: true,
    resolveJsonModule: false,
    moduleResolution: ts.ModuleResolutionKind.Classic,
    incremental: true,
    noLib: true,
    noResolve: true,
  },
  true,
);

function getImports(fileName) {
  const sourceFile = tsHost.getSourceFile(fileName, ts.ScriptTarget.Latest, msg => {
    throw new Error(`Failed to parse ${fileName}: ${msg}`);
  });
  if (!sourceFile) throw ReferenceError(`Failed to find file ${fileName}`);
  const importing = [];
  delintNode(sourceFile);

  return {
    importing,
  };

  function delintNode(node) {
    // console.log(node);
    if (ts.isNamedImports(node)) {
    }
    if (ts.isImportDeclaration(node)) {
      // console.log(node);
      const moduleName = node.moduleSpecifier.getText().replace(/['"]/g, '');
      if (!moduleName.startsWith('node:') && !builtinModules.includes(moduleName)) importing.push(moduleName);
    } else ts.forEachChild(node, delintNode);
  }
}

// console.log(getImports('src/App.tsx'));
// console.log(getImports('src/view/fragment/Musica/drawer/ConfiguracoesMusicaDrawer.lazy.tsx'));
*/
import {cruise} from 'dependency-cruiser';
import extractDepcruiseConfig from 'dependency-cruiser/config-utl/extract-depcruise-config';
import extractTSConfig from 'dependency-cruiser/config-utl/extract-ts-config';
import extractBabelConfig from 'dependency-cruiser/config-utl/extract-babel-config';

async function test() {
  const tsConfig = extractTSConfig('./tsconfig.json');
  const babelConfig = await extractBabelConfig('./babel.config.js');
  const webpackResolveOptions = {
    exportsFields: ['exports'],
    conditionNames: ['require'],
  };

  const config = await extractDepcruiseConfig('./__analysis__/.dependency-cruiser.js');

  const ARRAY_OF_FILES_AND_DIRS_TO_CRUISE = ['./index.js'];
  try {
    const cruiseResult = await cruise(
      //
      ARRAY_OF_FILES_AND_DIRS_TO_CRUISE,
      config,
      // undefined,
      webpackResolveOptions,
      {
        tsConfig,
        // babelConfig,
      },
    );
    console.dir(cruiseResult.output, {depth: 1000});
  } catch (error) {
    console.log(error);
  }
}

test().then(() => {});
