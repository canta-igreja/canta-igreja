# Canta Igreja - Aplicativo de músicas católicas

O aplicativo Canta Igreja trás uma coleção de letras de músicas cantadas em missas, encontros, celebrações e grupos de oração.

- Play Store: https://play.google.com/store/apps/details?id=com.cantaigreja&hl=pt-BR

## Infraestrutura

| Título | Área | Link | Tecnologia | Repositório |
| --- | --- | --- | --- | --- |
| Api | Backend | https://api.canta.app | Flask | https://gitlab.com/SrMouraSilva/canta-igreja-api |
| Api - Banco de dados | Banco de dados | - | Postgres (Neon) | https://gitlab.com/SrMouraSilva/canta-igreja-api/-/tree/main/migration |
| Canta Igreja App - Banco de dados | Banco de dados | - | SQLite | https://gitlab.com/SrMouraSilva/canta-igreja-api/-/tree/main/dados |
| Canta Igreja App | Aplicativo | https://canta.app | React Native | https://gitlab.com/canta-igreja/canta-igreja |
| Landing Page (canta.app) | Frontend | https://canta.app | Astro | https://gitlab.com/SrMouraSilva/canta-igreja-landing-page |
| Livreto (livreto.canta.app) | Frontend | https://livreto.canta.app | React | https://gitlab.com/SrMouraSilva/livreto |
